IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'Clinica')
	CREATE DATABASE Clinica;

GO

USE Clinica;

GO

-- Tabla Paciente
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Paciente')
	CREATE TABLE Paciente
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Dui VARCHAR(10) UNIQUE NOT NULL,
		-- N�mero de Afiliaci�n
		NoAfiliacion VARCHAR(12) UNIQUE NOT NULL,
		Nombres VARCHAR(50) NOT NULL,
		Apellidos VARCHAR(50) NOT NULL,
		CentroAtencion VARCHAR(100) NOT NULL,
		-- Fecha de Expedici�n de Expediente
		Fecha DATE not null,
		Calidad VARCHAR(15) NOT NULL,
		Sexo VARCHAR(20) NOT NULL,
		EstadoCivil VARCHAR(20) NOT NULL,
		Edad INT NOT NULL,
		FechaNacimiento DATE NOT NULL,
		LugarNacimiento VARCHAR(100) NOT NULL,
		-- Direcci�n de Domicilio Actual
		DireccionDomicilio VARCHAR(100) NOT NULL,
		-- Tel�fono de Domicilio Actual
		TelDomicilio VARCHAR(15) NOT NULL,
		-- Direcci�n de Tarabajo Actual
		DireccionTrabajo VARCHAR(100) NOT NULL,
		-- Tel�fono de Trabajo Actual
		TelTrabajo VARCHAR(15) NOT NULL,
		-- Ocupaci�n del Trabajo Actual
		Ocupacion VARCHAR(100) NOT NULL,
		-- Patrono Actual del Asegurado
		Patrono VARCHAR(100) NOT NULL,
		ActividadEconomica VARCHAR(100) NOT NULL,
		NombrePadre VARCHAR(100) NOT NULL,
		NombreMadre VARCHAR(100) NOT NULL,
		NombreConyugue VARCHAR(100),
		-- Nombre de Persona a llamar en caso de Emergencia
		NombreEmergencia VARCHAR(100) NOT NULL,
		TelEmergencia VARCHAR(15) NOT NULL,
		-- Nombre de Persona que Proporcion� los Datos
		ProporcionadorDatos VARCHAR(100) NOT NULL,
		-- Nombre de Persona que Archiva el Expediente
		Archivista VARCHAR(100) NOT NULL,
		Estado INT NOT NULL,
		CONSTRAINT PK_Paciente PRIMARY KEY(id)
	);

Go

-- Procedimiento almacenado para agregar Pacientes
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAgregarPaciente')
	DROP PROC spAgregarPaciente;
GO
Create Proc spAgregarPaciente
	@Dui VARCHAR(10),
	-- N�mero de Afiliaci�n
	@NoAfiliacion VARCHAR(12),
	@Nombres VARCHAR(50),
	@Apellidos VARCHAR(50),
	@CentroAtencion VARCHAR(100),
	-- Fecha de Expedici�n de Expediente
	@Fecha VARCHAR(10),
	@Calidad VARCHAR(15),
	@Sexo VARCHAR(20),
	@EstadoCivil VARCHAR(20),
	@Edad INT,
	@FechaNacimiento VARCHAR(10),
	@LugarNacimiento VARCHAR(100),
	-- Direcci�n de Domicilio Actual
	@DireccionDomicilio VARCHAR(100),
	-- Tel�fono de Domicilio Actual
	@TelDomicilio VARCHAR(15),
	-- Direcci�n de Tarabajo Actual
	@DireccionTrabajo VARCHAR(100),
	-- Tel�fono de Trabajo Actual
	@TelTrabajo VARCHAR(15),
	-- Ocupaci�n del Trabajo Actual
	@Ocupacion VARCHAR(100),
	-- Patrono Actual del Asegurado
	@Patrono VARCHAR(100),
	@ActividadEconomica VARCHAR(100),
	@NombrePadre VARCHAR(100),
	@NombreMadre VARCHAR(100),
	@NombreConyugue VARCHAR(100),
	-- Nombre de Persona a llamar en caso de Emergencia
	@NombreEmergencia VARCHAR(100),
	@TelEmergencia VARCHAR(15),
	-- Nombre de Persona que Proporcion� los Datos
	@ProporcionadorDatos VARCHAR(100),
	-- Nombre de Persona que Archiva el Expediente
	@Archivista VARCHAR(100)
AS
	INSERT INTO Paciente
	VALUES
	(
		@Dui,
		@NoAfiliacion,
		@Nombres,
		@Apellidos,
		@CentroAtencion,
		Convert(DATE, @Fecha, 103),
		@Calidad,
		@Sexo,
		@EstadoCivil,
		@Edad,
		Convert(DATE, @FechaNacimiento, 103),
		@LugarNacimiento,
		@DireccionDomicilio,
		@TelDomicilio,
		@DireccionTrabajo,
		@TelTrabajo,
		@Ocupacion,
		@Patrono,
		@ActividadEconomica,
		@NombrePadre,
		@NombreMadre,
		@NombreConyugue,
		@NombreEmergencia,
		@TelEmergencia,
		@ProporcionadorDatos,
		@Archivista, 1
	);

GO

-- Procedimiento almacenado para editar Pacientes
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spModificarPaciente')
	DROP PROC spModificarPaciente;
GO
CREATE PROC spModificarPaciente
	@Id INT,
	@Dui VARCHAR(10),
	-- N�mero de Afiliaci�n
	@NoAfiliacion VARCHAR(12),
	@Nombres VARCHAR(50),
	@Apellidos VARCHAR(50),
	@CentroAtencion VARCHAR(100),
	-- Fecha de Expedici�n de Expediente
	@Fecha VARCHAR(10),
	@Calidad VARCHAR(15),
	@Sexo VARCHAR(20),
	@EstadoCivil VARCHAR(20),
	@Edad INT,
	@FechaNacimiento VARCHAR(10),
	@LugarNacimiento VARCHAR(100),
	-- Direcci�n de Domicilio Actual
	@DireccionDomicilio VARCHAR(100),
	-- Tel�fono de Domicilio Actual
	@TelDomicilio VARCHAR(15),
	-- Direcci�n de Tarabajo Actual
	@DireccionTrabajo VARCHAR(100),
	-- Tel�fono de Trabajo Actual
	@TelTrabajo VARCHAR(15),
	-- Ocupaci�n del Trabajo Actual
	@Ocupacion VARCHAR(100),
	-- Patrono Actual del Asegurado
	@Patrono VARCHAR(100),
	@ActividadEconomica VARCHAR(100),
	@NombrePadre VARCHAR(100),
	@NombreMadre VARCHAR(100),
	@NombreConyugue VARCHAR(100),
	-- Nombre de Persona a llamar en caso de Emergencia
	@NombreEmergencia VARCHAR(100),
	@TelEmergencia VARCHAR(15),
	-- Nombre de Persona que Proporcion� los Datos
	@ProporcionadorDatos VARCHAR(100),
	-- Nombre de Persona que Archiva el Expediente
	@Archivista VARCHAR(100)
AS
	UPDATE Paciente
	SET
	Dui = @Dui,
	NoAfiliacion = @NoAfiliacion,
	Nombres = @Nombres,
	Apellidos = @Apellidos,
	CentroAtencion = @CentroAtencion,
	Fecha = Convert(DATE, @Fecha, 103),
	Calidad = @Calidad,
	Sexo = @Sexo,
	EstadoCivil = @EstadoCivil,
	Edad = @Edad,
	FechaNacimiento = Convert(DATE, @FechaNacimiento, 103),
	LugarNacimiento = @LugarNacimiento,
	DireccionDomicilio = @DireccionDomicilio,
	TelDomicilio = @TelDomicilio,
	DireccionTrabajo = @DireccionTrabajo,
	TelTrabajo = @TelTrabajo,
	Ocupacion = @Ocupacion,
	Patrono = @Patrono,
	ActividadEconomica = @ActividadEconomica,
	NombrePadre = @NombrePadre,
	NombreMadre = @NombreMadre,
	NombreConyugue = @NombreConyugue,
	NombreEmergencia = @NombreEmergencia,
	TelEmergencia = @TelEmergencia,
	ProporcionadorDatos = @ProporcionadorDatos,
	Archivista = @Archivista
	WHERE Id = @Id;

Go

-- Procedimiento almacenado para anular Pacientes
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAnularPaciente')
	DROP PROC spAnularPaciente;
GO
CREATE PROC spAnularPaciente
	@Id Int
AS
	UPDATE Paciente SET Estado = 0 WHERE Id = @Id;

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReactivarPaciente')
	DROP PROC spReactivarPaciente;
GO
CREATE PROC spReactivarPaciente
	@Id INT
AS
	UPDATE Paciente SET Estado = 1 WHERE Id = @Id;

GO

-- Procedimiento almacenado para anular Pacientes
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spEliminarPaciente')
	DROP PROC spEliminarPaciente;
GO
Create Proc spEliminarPaciente
	@Id Int
As
	DELETE FROM Paciente WHERE Id = @Id;

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarPacientesActivos')
	DROP PROC spMostrarPacientesActivos;
GO
CREATE PROC spMostrarPacientesActivos
As
	SELECT TOP 50
		Id,
		Dui,
		NoAfiliacion,
		Nombres,
		Apellidos,
		CentroAtencion,
		Convert(VARCHAR(10), Fecha, 103) As Fecha,
		Calidad,
		Sexo,
		EstadoCivil,
		Edad,
		Convert(VARCHAR(10), FechaNacimiento, 103) As FechaNacimiento,
		LugarNacimiento,
		DireccionDomicilio,
		TelDomicilio,
		DireccionTrabajo,
		TelTrabajo,
		Ocupacion,
		Patrono,
		ActividadEconomica,
		NombrePadre,
		NombreMadre,
		NombreConyugue,
		NombreEmergencia,
		TelEmergencia,
		ProporcionadorDatos,
		Archivista,
		Estado
		FROM Paciente p
		WHERE Estado = 1 ORDER BY id DESC;

GO

EXEC spMostrarPacientesActivos;

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarPacientesInactivos')
	DROP PROC spMostrarPacientesInactivos;
GO
CREATE PROC spMostrarPacientesInactivos
AS
	SELECT TOP 50
		Id,
		Dui,
		NoAfiliacion,
		Nombres,
		Apellidos,
		CentroAtencion,
		Convert(VARCHAR(10), Fecha, 103) AS Fecha,
		Calidad,
		Sexo,
		EstadoCivil,
		Edad,
		Convert(VARCHAR(10), FechaNacimiento, 103) AS FechaNacimiento,
		LugarNacimiento,
		DireccionDomicilio,
		TelDomicilio,
		DireccionTrabajo,
		TelTrabajo,
		Ocupacion,
		Patrono,
		ActividadEconomica,
		NombrePadre,
		NombreMadre,
		NombreConyugue,
		NombreEmergencia,
		TelEmergencia,
		ProporcionadorDatos,
		Archivista,
		Estado
		FROM Paciente p
		WHERE Estado = 0 ORDER BY id DESC;

GO

EXEC spMostrarPacientesInactivos;

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarPacientesActivos')
	DROP PROC spBuscarPacientesActivos;
GO
CREATE PROC spBuscarPacientesActivos
	@Dui VARCHAR(100)
AS
	SELECT TOP 50
	Id,
	Dui,
	NoAfiliacion,
	Nombres,
	Apellidos,
	CentroAtencion,
	Convert(Varchar(10), Fecha, 103) AS Fecha,
	Calidad,
	Sexo,
	EstadoCivil,
	Edad,
	Convert(Varchar(10), FechaNacimiento, 103) AS FechaNacimiento,
	LugarNacimiento,
	DireccionDomicilio,
	TelDomicilio,
	DireccionTrabajo,
	TelTrabajo,
	Ocupacion,
	Patrono,
	ActividadEconomica,
	NombrePadre,
	NombreMadre,
	NombreConyugue,
	NombreEmergencia,
	TelEmergencia,
	ProporcionadorDatos,
	Archivista,
	Estado
	From Paciente p
	WHERE Estado = 1 AND Dui LIKE @Dui + '%'
	ORDER BY id DESC;
	
GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarPacientesInactivos')
	DROP PROC spBuscarPacientesInactivos;
GO
CREATE PROC spBuscarPacientesInactivos
	@Dui VARCHAR(100)
AS
	SELECT TOP 50
	Id,
	Dui,
	NoAfiliacion,
	Nombres,
	Apellidos,
	CentroAtencion,
	Convert(Varchar(10), Fecha, 103) As Fecha,
	Calidad,
	Sexo,
	EstadoCivil,
	Edad,
	Convert(VARCHAR(10), FechaNacimiento, 103) AS FechaNacimiento,
	LugarNacimiento,
	DireccionDomicilio,
	TelDomicilio,
	DireccionTrabajo,
	TelTrabajo,
	Ocupacion,
	Patrono,
	ActividadEconomica,
	NombrePadre,
	NombreMadre,
	NombreConyugue,
	NombreEmergencia,
	TelEmergencia,
	ProporcionadorDatos,
	Archivista,
	Estado
	FROM Paciente p
	WHERE Estado = 0 AND Dui LIKE @Dui + '%'
	ORDER BY id DESC;

GO

-- Tabla Tipo de Usuario
If Not Exists (SELECT * FROM sys.tables WHERE name = 'TU')
	CREATE TABLE TU
	(
		Id INT NOT NULL,
		-- Descripci�n del Tipo de Usuarios
		Descripcion VARCHAR(20) NOT NULL,
		CONSTRAINT PK_TU PRIMARY KEY(Id)
	);

-- Volcado de datos para la tabla Tipo de Usuario (TU)
IF NOT EXISTS (SELECT * FROM TU WHERE Descripcion = 'Administrador' OR Descripcion = 'Usuario')
	INSERT INTO TU VALUES(1, 'Administrador'), (2, 'Usuario');

-- Tabla Usuarios
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Usuarios')
	CREATE TABLE Usuarios
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Nombre VARCHAR(30) UNIQUE NOT NULL,
		Pass VARCHAR(200) NOT NULL,
		IdTU INT NOT NULL,
		Estado INT NOT NULL,
		CONSTRAINT PK_Usuario PRIMARY KEY(Id),
		CONSTRAINT FK_Usuario_TU FOREIGN KEY(IdTU) REFERENCES TU(Id)
	);

Go

INSERT INTO Usuarios VALUES('Admin', EncryptByPassPhrase('key', '12345'), 1, 1);
INSERT INTO Usuarios VALUES('User', EncryptByPassPhrase('key', '12345'), 2, 1);

Go

-- Procendimiento para verificar que el usuario exista
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spVerificarUsuario')
	DROP PROC spVerificarUsuario;
GO
CREATE PROCEDURE spVerificarUsuario
	@nombre VARCHAR(30),
	@pass VARCHAR(20)
AS
	SELECT TOP 1
	Id, Nombre, Convert(VARCHAR(100), DecryptByPassPhrase('key', Pass)) AS Pass, IdTU, Estado
	FROM Usuarios
	WHERE Nombre = @nombre AND Convert(VARCHAR(100), DecryptByPassPhrase('key', Pass)) = @pass;

GO

-- Procendimiento para verificar el tipo de usuario
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spVerificarTipoUsuario')
	DROP PROC spVerificarTipoUsuario;
GO
CREATE PROCEDURE spVerificarTipoUsuario
	@nombre VARCHAR(30),
	@pass VARCHAR(20)
AS
	SELECT TOP 1
	Id, Nombre, Convert(VARCHAR(100), DecryptByPassPhrase('key', Pass)) AS Pass, IdTU, Estado
	FROM Usuarios
	WHERE Nombre = @nombre And Convert(VARCHAR(100), DecryptByPassPhrase('key', Pass)) = @pass AND IdTU = 1;

GO

Exec spVerificarTipoUsuario 'Admin', '12345';

GO

-- Procendimiento para verificar el estado del usuario
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spVerificarEstadoUsuario')
	DROP PROC spVerificarEstadoUsuario;
GO
CREATE PROCEDURE spVerificarEstadoUsuario
	@nombre VARCHAR(30),
	@pass VARCHAR(20)
AS
	SELECT TOP 1
	Id, Nombre, Convert(VARCHAR(100), DecryptByPassPhrase('key', Pass)) AS Pass, IdTU, Estado
	FROM Usuarios
	WHERE Nombre = @nombre And Convert(VARCHAR(100), DecryptByPassPhrase('key', Pass)) = @pass AND Estado = 1;

GO

EXEC spVerificarEstadoUsuario 'Admin', '12345';

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarUsuarios')
	DROP PROC spMostrarUsuarios;
GO
CREATE PROC spMostrarUsuarios
AS
	SELECT TOP 50
	Id, Nombre, Pass, IdTU, Estado
	FROM Usuarios;

GO

Exec spMostrarUsuarios;

GO

/*
-- Tabla Cargo
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='Cargo')
	CREATE TABLE Cargo
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Descripcion VARCHAR(50) NOT NULL,
		CONSTRAINT PK_Cargo PRIMARY KEY(Id)
	);

GO

INSERT INTO Cargo VALUES ('Paramedico'), ('Medico'), ('Enfermero(a)'), ('Ayudante');

-- Tabla Empleado
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='Empleado')
	CREATE TABLE Empleado
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Nombres VARCHAR(50) NOT NULL,
		Apellidos VARCHAR(50) NOT NULL,
		Telefono VARCHAR(15) NOT NULL,
		Correo VARCHAR(50) NULL,
		IdCargo INT NOT NULL,
		CONSTRAINT PK_Empleado PRIMARY KEY(Id),
		CONSTRAINT FK_Empleado_Cargo FOREIGN KEY(IdCargo) REFERENCES Cargo(Id)
	);
*/

-- Tabla Recordatorio

IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Recordatorio')
	CREATE TABLE Recordatorio
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Titulo VARCHAR(50) NOT NULL,
		Descripcion VARCHAR(200) NOT NULL,
		Fecha VARCHAR(15) NOT NULL,
		Estado VARCHAR(25) NOT NULL,
		CONSTRAINT PK_Recordatorio PRIMARY KEY(Id)
	);

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAgregarRecordatorio')
	DROP PROC spAgregarRecordatorio;
GO
CREATE PROC spAgregarRecordatorio
	@Titulo VARCHAR(50),
	@Descripcion VARCHAR(200),
	@Fecha VARCHAR(15),
	@Estado VARCHAR(25)
AS
	INSERT INTO Recordatorio VALUES(@Titulo, @Descripcion, @Fecha, @Estado);

GO

EXEC spAgregarRecordatorio 'Limpieza', 'Sacar la basura', '19/06/2018', 'Retrasado';
EXEC spAgregarRecordatorio 'Organizar', 'Gestionar expedientes', '21/06/2018', 'Pendiente';

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spModificarRecordatorio')
	DROP PROC spModificarRecordatorio;
GO
CREATE PROC spModificarRecordatorio
	@Id Int,
	@Titulo VARCHAR(50),
	@Descripcion VARCHAR(200),
	@Fecha VARCHAR(15),
	@Estado VARCHAR(25)
AS
	UPDATE Recordatorio
	SET
	Titulo = @Titulo,
	Descripcion = @Descripcion,
	Fecha = @Fecha,
	Estado = @Estado
	WHERE
	Id = @Id;

GO

EXEC spModificarRecordatorio 1, 'Limpieza', 'Sacar la basura', '19/06/2018', 'Retrasado';
EXEC spModificarRecordatorio 2, 'Organizar', 'Gestionar expedientes', '21/06/2018', 'Pendiente';

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spEliminarRecordatorio')
	DROP PROC spEliminarRecordatorio;
GO
CREATE PROC spEliminarRecordatorio
	@Id Int
AS
	DELETE FROM Recordatorio
	WHERE
	Id = @Id;

GO

--EXECUTE spEliminarRecordatorio 4;
--EXECUTE spEliminarRecordatorio 5;

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarRecordatorios')
	DROP PROC spBuscarRecordatorios;
GO
CREATE PROC spBuscarRecordatorios
	@valor VARCHAR(100)
AS
	SELECT TOP 50 * FROM Recordatorio
	WHERE
	Titulo LIKE @valor + '%'
	OR
	Fecha LIKE @valor + '%'

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarRecordatorios')
	DROP PROC spMostrarRecordatorios;
GO
CREATE PROC spMostrarRecordatorios
AS
	SELECT TOP 50 * FROM Recordatorio;

GO

EXEC spMostrarRecordatorios;

GO

IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spGetFechas')
	DROP PROC spGetFechas;
GO
CREATE PROC spGetFechas
AS
SELECT Fecha FROM Recordatorio;

GO

EXEC spGetFechas;

-- Tabla Tipo Asegurado
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'TipoAsegurado')
	CREATE TABLE TipoAsegurado
	(
		Id INT UNIQUE NOT NULL,
		Descripcion VARCHAR(50) NOT NULL,
		CONSTRAINT PK_TipoAsegurado PRIMARY KEY(Id)
	);

-- Volcado de datos para tabla Tipo Asegurado
INSERT INTO
TipoAsegurado
VALUES(1, 'Cotizantes'),
		(2, 'Pensionados'),
		  (3, 'Conyugues o compa�eros de vida'),
		    (4, 'Hijos de empleados del ISSS'),
			  (5, 'Padres de empleados del ISSS'),
				(6, 'No asegurados'),
				  (7, 'Cesantes');

-- Tabla Tipo Consulta
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'TipoConsulta')
	CREATE TABLE TipoConsulta
	(
		Id INT UNIQUE NOT NULL,
		Descripcion VARCHAR(50) NOT NULL,
		CONSTRAINT PK_TipoConsulta PRIMARY KEY(Id)
	);

-- Volcado de datos para tabla Tipo Consulta
INSERT INTO
TipoConsulta
VALUES(1, 'Enfermedad com�n'),
		(2, 'Accidente com�n'),
		  (3, 'Accidente de trabajo'),
		    (4, 'Enfermeddad profesional'),
			  (5, 'Maternidad');

-- Tabla Resultado
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Resultado')
	CREATE TABLE Resultado
	(
		Id INT UNIQUE NOT NULL,
		Descripcion VARCHAR(50) NOT NULL,
		CONSTRAINT PK_Resultado PRIMARY KEY(Id)
	);

-- Volcado de datos para tabla Resultado
INSERT INTO
Resultado
VALUES(1, 'Alta'),
		(2, 'Referencia'),
		  (3, 'Retorno'),
		    (4, 'Cita'),
			  (5, 'Ingreso'),
			    (6, 'Observaci�n');

GO

-- Tabla Consuta
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'Consulta')
	CREATE TABLE Consulta
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Fecha VARCHAR(15) NOT NULL,
		IdPaciente INT NOT NULL,
		-- Tipo Consulta
		IdTC INT NOT NULL,
		-- Tipo Asegurado
		IdTA INT NOT NULL,
		Tipo VARCHAR(25) NOT NULL,
		Serie VARCHAR(25) NOT NULL,
		-- Para saber si genera o no genera
		Subsidio VARCHAR(25) NOT NULL,
		NoExamenLab VARCHAR(25) NOT NULL,
		ExamenGabinete VARCHAR(25) NOT NULL,
		MedicamentoPrescrito VARCHAR(25) NOT NULL,
		IdResultado INT NOT NULL,
		DiagnosticoPrincipal VARCHAR(200) NOT NULL,
		CodPrincipal VARCHAR(25) NOT NULL,
		DiagnosticoSecundario VARCHAR(200) NULL,
		CodSecundario VARCHAR(25) NULL,
		-- deleted == 0 ? Activo : Eliminado
		Deleted INT NOT NULL,
		CONSTRAINT PK_Consulta PRIMARY KEY(Id),
		CONSTRAINT FK_Consulta_TC FOREIGN KEY(IdTC) REFERENCES TipoConsulta(Id),
		CONSTRAINT FK_Consulta_TA FOREIGN KEY(IdTA) REFERENCES TipoAsegurado(Id),
		CONSTRAINT FK_Consulta_Resultado FOREIGN KEY(IdResultado) REFERENCES Resultado(Id),
		CONSTRAINT FK_Consulta_Paciente FOREIGN KEY(IdPaciente) REFERENCES Paciente(Id)
	);

GO

-- Procedimiento almacenado para mostrar consultas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarConsultas')
	DROP PROC spMostrarConsultas;
GO
CREATE PROCEDURE spMostrarConsultas
AS
	SELECT TOP 50
	C.Id,
	C.Fecha,
	P.NoAfiliacion,
	P.Edad,
	P.Sexo,
	P.CentroAtencion,
	-- Tipo Consulta
	C.IdTC AS 'Tipo consulta',
	-- Tipo Asegurado
	C.IdTA AS 'Tipo asegurado',
	C.Tipo,
	C.Serie AS 'Serie Incapacidad',
	-- Para saber si genera o no genera
	C.Subsidio,
	C.NoExamenLab AS 'N° Laboratorio',
	C.ExamenGabinete AS 'Examen gabinete',
	C.MedicamentoPrescrito AS Medicamento,
	C.IdResultado,
	C.DiagnosticoPrincipal,
	C.CodPrincipal,
	C.DiagnosticoSecundario,
	C.CodSecundario
	FROM
	Consulta C
	INNER JOIN
	Paciente P
	ON
	C.IdPaciente = P.Id
	WHERE
	Deleted != 0
	ORDER BY Id DESC;

GO

EXECUTE spMostrarConsultas;

GO

-- Procedimiento almacenado para agregar consulta
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAgregarConsulta')
	DROP PROC spAgregarConsulta;
GO
CREATE PROCEDURE spAgregarConsulta
	@Fecha VARCHAR(25),
	@IdPaciente INT,
	-- Tipo Consulta
	@IdTC INT,
	-- Tipo Asegurado
	@IdTA INT,
	@Tipo VARCHAR(25),
	@Serie VARCHAR(25),
	-- Para saber si genera o no genera
	@Subsidio VARCHAR(25),
	@NoExamenLab VARCHAR(25),
	@ExamenGabinete VARCHAR(25),
	@MedicamentoPrescrito VARCHAR(25),
	@IdResultado INT,
	@DiagnosticoPrincipal VARCHAR(100),
	@CodPrincipal VARCHAR(25),
	@DiagnosticoSecundario VARCHAR(100),
	@CodSecundario VARCHAR(25)
AS
	INSERT INTO
	Consulta
	VALUES
	(
		@Fecha,
		@IdPaciente,
		-- Tipo Consulta
		@IdTC,
		-- Tipo Asegurado
		@IdTA,
		@Tipo,
		@Serie,
		-- Para saber si genera o no genera
		@Subsidio,
		@NoExamenLab,
		@ExamenGabinete,
		@MedicamentoPrescrito,
		@IdResultado,
		@DiagnosticoPrincipal,
		@CodPrincipal,
		@DiagnosticoSecundario,
		@CodSecundario,
		1
	);

GO

-- Procedimiento almacenado para buscar consultas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarConsultas')
	DROP PROC spBuscarConsultas;
GO
CREATE PROCEDURE spBuscarConsultas
	@Valor VARCHAR(10)
AS
	SELECT
	C.Id,
	C.Fecha,
	P.NoAfiliacion,
	P.Edad,
	P.Sexo,
	P.CentroAtencion,
	-- Tipo Consulta
	C.IdTC AS 'Tipo consulta',
	-- Tipo Asegurado
	C.IdTA AS 'Tipo asegurado',
	C.Tipo,
	C.Serie AS 'Serie Incapacidad',
	-- Para saber si genera o no genera
	C.Subsidio,
	C.NoExamenLab AS 'N° Laboratorio',
	C.ExamenGabinete AS 'Examen gabinete',
	C.MedicamentoPrescrito AS Medicamento,
	C.IdResultado,
	C.DiagnosticoPrincipal,
	C.CodPrincipal,
	C.DiagnosticoSecundario,
	C.CodSecundario
	FROM
	Consulta C
	INNER JOIN
	Paciente P
	ON
	C.IdPaciente = P.Id
	WHERE
	(C.Fecha LIKE @Valor + '%' AND Deleted != 0)
	OR
	(P.NoAfiliacion LIKE @Valor + '%' AND Deleted != 0);

GO

-- Procedimiento almacenado para seleccionar los datos del paciente
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spSelectPacienteData')
	DROP PROC spSelectPacienteData;
GO
CREATE PROCEDURE spSelectPacienteData
	@numA VARCHAR(15)
AS
	SELECT
	Id, Edad, Sexo, CentroAtencion
	FROM
	Paciente
	WHERE
	NoAfiliacion
	LIKE
	@numA + '%'
	AND
	Paciente.Estado = 1;

GO

-- Procedimiento almacenado para modificar consulta
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spModificarConsulta')
	DROP PROC spModificarConsulta;
GO
CREATE PROCEDURE spModificarConsulta
	@Id INT,
	@Fecha VARCHAR(25),
	@IdPaciente INT,
	-- Tipo Consulta
	@IdTC INT,
	-- Tipo Asegurado
	@IdTA INT,
	@Tipo VARCHAR(25),
	@Serie VARCHAR(25),
	-- Para saber si genera o no genera
	@Subsidio VARCHAR(25),
	@NoExamenLab VARCHAR(25),
	@ExamenGabinete VARCHAR(25),
	@MedicamentoPrescrito VARCHAR(25),
	@IdResultado INT,
	@DiagnosticoPrincipal VARCHAR(100),
	@CodPrincipal VARCHAR(25),
	@DiagnosticoSecundario VARCHAR(100),
	@CodSecundario VARCHAR(25)
AS
	UPDATE
	Consulta
	SET
	Fecha = @Fecha,
	IdPaciente = @IdPaciente,
	-- Tipo Consulta
	IdTC = @IdTC,
	-- Tipo Asegurado
	IdTA = @IdTA,
	Tipo = @Tipo,
	Serie = @Serie,
	-- Para saber si genera o no genera
	Subsidio = @Subsidio,
	NoExamenLab = @NoExamenLab,
	ExamenGabinete = @ExamenGabinete,
	MedicamentoPrescrito = @MedicamentoPrescrito,
	IdResultado = @IdResultado,
	DiagnosticoPrincipal = @DiagnosticoPrincipal,
	CodPrincipal = @CodPrincipal,
	DiagnosticoSecundario = @DiagnosticoSecundario,
	CodSecundario = @CodSecundario
	WHERE
	Id = @Id;

GO

-- Procedimiento almacenado para eliminar consulta
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spEliminarConsulta')
	DROP PROC spEliminarConsulta;
GO
CREATE PROCEDURE spEliminarConsulta
	@Id INT
AS
	UPDATE
	Consulta
	SET
	Deleted = 0
	WHERE
	Id = @Id;

GO

SELECT * FROM TipoConsulta;

GO

-- Procedimiento almacenado para mostrar usuarios
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarUsuarios')
	DROP PROC spMostrarUsuarios;
GO
CREATE PROCEDURE spMostrarUsuarios
	@act VARCHAR(50)
AS
	SELECT TOP 50
	U.Id, U.Nombre, '*******' AS 'Contraseña',
	TU.Descripcion AS 'Tipo de usuario', CONVERT(VARCHAR(2), U.Estado) AS Estado,
	(SELECT COUNT(Id) FROM Usuarios) AS TOTAL
	FROM
	Usuarios U
	INNER JOIN
	TU
	ON
	U.IdTU = TU.Id
	WHERE
	U.Nombre != @act
	GROUP BY U.Id, U.Nombre, TU.Descripcion, U.Estado
	ORDER BY Id DESC;
	
GO

EXECUTE spMostrarUsuarios 'Juan Pablo';

GO

-- Procedimiento almacenado para buscar usuarios
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarUsuarios')
	DROP PROC spBuscarUsuarios;
GO
CREATE PROCEDURE spBuscarUsuarios
	@val VARCHAR(50),
	@act VARCHAR(50)
AS
	SELECT TOP 50
	U.Id, U.Nombre, '*******' AS 'Contraseña',
	TU.Descripcion AS 'Tipo de usuario', CONVERT(VARCHAR(2), U.Estado) AS Estado,
	(SELECT COUNT(Id) FROM Usuarios) AS TOTAL
	FROM
	Usuarios U
	INNER JOIN
	TU
	ON
	U.IdTU = TU.Id
	WHERE
	U.Nombre != @act
	AND
	U.Nombre LIKE @val + '%'
	ORDER BY Id DESC;

GO

EXECUTE spBuscarUsuarios 'Admin', 'Juan Pablo';

GO

-- Procedimiento almacenado para agregar usuario
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAgregarUsuario')
	DROP PROC spAgregarUsuario;
GO
CREATE PROCEDURE spAgregarUsuario
	@name VARCHAR(50),
	@pass VARCHAR(50),
	@tu INT,
	@state INT
AS
	INSERT INTO
	Usuarios
	VALUES
	(@name, EncryptByPassPhrase('key', @pass), @tu, @state);

GO

-- Procedimiento almacenado para modificar usuario
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spModificarUsuario')
	DROP PROC spModificarUsuario;
GO
CREATE PROCEDURE spModificarUsuario
	@id INT,
	@name VARCHAR(50),
	@pass VARCHAR(50),
	@tu INT,
	@state INT
AS
	UPDATE
	Usuarios
	SET
	Nombre = @name, Pass = EncryptByPassPhrase('key', @pass), IdTU = @tu, Estado = @state
	WHERE
	Id = @id;

GO

-- Procedimiento almacenado para eliminar usuario
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spEliminarUsuario')
	DROP PROC spEliminarUsuario;
GO
CREATE PROCEDURE spEliminarUsuario
	@id INT
AS
	DELETE FROM
	Usuarios
	WHERE
	Id = @id;

GO

-- Tabla Enfermedad cie10.org/Cie10_Descargas.html
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name='CIE')
	CREATE TABLE CIE
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Codigo VARCHAR(10) NOT NULL,
		Descripcion VARCHAR(500) NOT NULL,
		Grupo VARCHAR(10) NULL,
		CONSTRAINT PK_Enfermedad PRIMARY KEY(Id)
	);

GO

-- Procedimiento almacenado para autocompletar nombres y codigos de enfermedades
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAutoComplete')
	DROP PROC spAutoComplete;
GO
CREATE PROCEDURE spAutoComplete
	@val VARCHAR(100)
AS
	SELECT Codigo FROM CIE
	WHERE
	Descripcion = @val;

GO

-- Procedimiento almacenado para llenar DataSource de autocommpletado
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spDataEnfermedades')
	DROP PROC spDataEnfermedades;
GO
CREATE PROCEDURE spDataEnfermedades
AS
	SELECT Descripcion FROM CIE;


EXECUTE spDataEnfermedades;

GO

-- Procedimiento almacenado para reporte general de consultas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReporteConsultas')
	DROP PROC spReporteConsultas;
GO
CREATE PROCEDURE spReporteConsultas
AS
	SELECT TOP 50
	C.Id,
	C.Fecha,
	P.NoAfiliacion,
	P.Edad,
	P.Sexo,
	-- Tipo Consulta
	C.IdTC,
	-- Tipo Asegurado
	C.IdTA,
	C.Tipo,
	C.Serie,
	-- Para saber si genera o no genera
	C.Subsidio,
	C.NoExamenLab,
	C.ExamenGabinete,
	C.MedicamentoPrescrito,
	C.IdResultado,
	C.DiagnosticoPrincipal,
	C.CodPrincipal,
	C.Fecha
	FROM
	Consulta C
	INNER JOIN
	Paciente P
	ON
	C.IdPaciente = P.Id
	WHERE
	Deleted != 0
	ORDER BY Id DESC;

GO

-- Procedimiento almacenado para reporte mensual de consultas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReporteConsultasMes')
	DROP PROC spReporteConsultasMes;
GO
CREATE PROCEDURE spReporteConsultasMes
AS
	SELECT
	C.Id,
	C.Fecha,
	P.NoAfiliacion,
	P.Edad,
	P.Sexo,
	-- Tipo Consulta
	C.IdTC,
	-- Tipo Asegurado
	C.IdTA,
	C.Tipo,
	C.Serie,
	-- Para saber si genera o no genera
	C.Subsidio,
	C.NoExamenLab,
	C.ExamenGabinete,
	C.MedicamentoPrescrito,
	C.IdResultado,
	C.DiagnosticoPrincipal,
	C.CodPrincipal
	FROM
	Consulta C
	INNER JOIN
	Paciente P
	ON
	C.IdPaciente = P.Id
	WHERE
	Deleted != 0
	AND
	MONTH(CONVERT(DATE, C.Fecha, 103)) = MONTH(GETDATE())
	AND
	YEAR(CONVERT(DATE, C.Fecha, 103)) = YEAR(GETDATE())
	ORDER BY Id DESC;

GO

EXEC spReporteConsultasMes;
EXEC spReporteConsultas;

GO

-- Procedimiento almacenado para reporte de paciente (Expediente individual)
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReportePaciente')
	DROP PROC spReportePaciente;
GO
CREATE PROCEDURE spReportePaciente
	@Id INT
AS
	SELECT
	Id,
	Dui,
	NoAfiliacion,
	Nombres,
	Apellidos,
	CentroAtencion,
	Convert(VARCHAR(10), Fecha, 103) As Fecha,
	Calidad,
	Sexo,
	EstadoCivil,
	Edad,
	Convert(VARCHAR(10), FechaNacimiento, 103) As FechaNacimiento,
	LugarNacimiento,
	DireccionDomicilio,
	TelDomicilio,
	DireccionTrabajo,
	TelTrabajo,
	Ocupacion,
	Patrono,
	ActividadEconomica,
	NombrePadre,
	NombreMadre,
	NombreConyugue,
	NombreEmergencia,
	TelEmergencia,
	ProporcionadorDatos,
	Archivista,
	Estado
	FROM
	Paciente
	WHERE
	Estado = 1
	AND
	Id = @Id
GO

EXEC spReportePaciente 1;

GO

-- Procedimiento almacenado para reporte de consultas reciiendo mes y año especificos
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReporteConsultasMesEspecifico')
	DROP PROC spReporteConsultasMesEspecifico;
GO
CREATE PROCEDURE spReporteConsultasMesEspecifico
	@Mes VARCHAR(2),
	@Year VARCHAR(4)
AS
	SELECT
	C.Id,
	C.Fecha,
	P.NoAfiliacion,
	P.Edad,
	P.Sexo,
	-- Tipo Consulta
	C.IdTC,
	-- Tipo Asegurado
	C.IdTA,
	C.Tipo,
	C.Serie,
	-- Para saber si genera o no genera
	C.Subsidio,
	C.NoExamenLab,
	C.ExamenGabinete,
	C.MedicamentoPrescrito,
	C.IdResultado,
	C.DiagnosticoPrincipal,
	C.CodPrincipal
	FROM
	Consulta C
	INNER JOIN
	Paciente P
	ON
	C.IdPaciente = P.Id
	WHERE
	Deleted != 0
	AND
	MONTH(CONVERT(DATE, C.Fecha, 103)) = @Mes
	AND
	YEAR(CONVERT(DATE, C.Fecha, 103)) = @Year
	ORDER BY Id DESC;

GO

EXEC spReporteConsultasMesEspecifico '7', '2018';

GO

-- Tabla Entrega de anticonceptivos planificacion familiar C
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'EntregaAnticonceptivos')
	CREATE TABLE EntregaAnticonceptivos
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Fecha VARCHAR(15) NOT NULL,
		IdPaciente INT NOT NULL,
		TipoConsulta VARCHAR(25) NOT NULL,
		Tipo VARCHAR(25) NOT NULL,
		Metodo VARCHAR(25) NOT NULL,
		Responsable VARCHAR(50) NOT NULL,
		--- 0 = Deleted & 1 = Active
		Deleted INT DEFAULT(1) NOT NULL,
		CONSTRAINT PK_EntregaAnticonceptivos PRIMARY KEY(Id),
		CONSTRAINT FK_EntregaAnticonceptivos_Paciente FOREIGN KEY(IdPaciente) REFERENCES Paciente(Id)
	);

GO

-- Procedimiento almacenado para obtener datos de paciente
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spSelectPacienteDataEntrega')
	DROP PROC spSelectPacienteDataEntrega;
GO
CREATE PROCEDURE spSelectPacienteDataEntrega
	@numA VARCHAR(9)
AS
	SELECT TOP 1
	Id, Nombres, Apellidos
	FROM
	Paciente
	WHERE
	NoAfiliacion LIKE @numA + '%'
	AND
	Estado != 0;

GO

EXEC spSelectPacienteDataEntrega '22';

GO

-- Procedimiento almacenado para mostrar entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarEntregas')
	DROP PROC spMostrarEntregas;
GO
CREATE PROCEDURE spMostrarEntregas
AS
	SELECT TOP 50
	E.Id, E.Fecha, P.NoAfiliacion,
	P.Nombres, P.Apellidos,
	E.TipoConsulta, E.Tipo, E.Metodo,
	E.Responsable
	FROM
	EntregaAnticonceptivos E
	INNER JOIN
	Paciente P
	ON
	E.IdPaciente = P.Id
	WHERE
	E.Deleted = 1;

GO

EXECUTE spMostrarEntregas;

GO

-- Procedimiento almacenado para buscar entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarEntregas')
	DROP PROC spBuscarEntregas;
GO
CREATE PROCEDURE spBuscarEntregas
	@numA VARCHAR(9)
AS
	SELECT TOP 50
	E.Id, E.Fecha, P.NoAfiliacion,
	P.Nombres, P.Apellidos,
	E.TipoConsulta, E.Tipo, E.Metodo,
	E.Responsable
	FROM
	EntregaAnticonceptivos E
	INNER JOIN
	Paciente P
	ON
	E.IdPaciente = P.Id
	WHERE
	E.Deleted = 1 AND P.NoAfiliacion LIKE @numA + '%';

GO

-- Procedimiento almacenado para agregar entrega
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAgregarEntrega')
	DROP PROC spAgregarEntrega;
GO
CREATE PROCEDURE spAgregarEntrega
	@Fecha VARCHAR(10),
	@IdPaciente INT,
	@TipoConsulta VARCHAR(25),
	@Tipo VARCHAR(25),
	@Metodo VARCHAR(25),
	@Responsable VARCHAR(50)
AS
	INSERT INTO
	EntregaAnticonceptivos
	VALUES
	(@Fecha, @IdPaciente, @TipoConsulta,
		@Tipo, @Metodo, @Responsable, 1);

GO

-- Procedimiento almacenado para modificar entrega
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spModificarEntrega')
	DROP PROC spModificarEntrega;
GO
CREATE PROCEDURE spModificarEntrega
	@Id INT,
	@Fecha VARCHAR(10),
	@IdPaciente INT,
	@TipoConsulta VARCHAR(25),
	@Tipo VARCHAR(25),
	@Metodo VARCHAR(25),
	@Responsable VARCHAR(50)
AS
	UPDATE
	EntregaAnticonceptivos
	SET
	Fecha = @Fecha, IdPaciente = @IdPaciente,
	TipoConsulta = @TipoConsulta,
	Tipo = @Tipo, Metodo = @Metodo,
	Responsable = @Responsable
	WHERE
	Id = @Id;

GO

-- Procedimiento almacenado para eliminar entrega
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spEliminarEntrega')
	DROP PROC spEliminarEntrega;
GO
CREATE PROCEDURE spEliminarEntrega
	@Id INT
AS
	DELETE FROM
	EntregaAnticonceptivos
	WHERE
	Id = @Id;

GO

-- Procedimiento almacenado para reporte entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReporteEntregas')
	DROP PROC spReporteEntregas;
GO
CREATE PROCEDURE spReporteEntregas
AS
	SELECT TOP 50
	E.Fecha, P.NoAfiliacion,
	P.Nombres, P.Apellidos,
	E.TipoConsulta, E.Tipo, E.Metodo,
	E.Responsable
	FROM
	EntregaAnticonceptivos E
	INNER JOIN
	Paciente P
	ON
	E.IdPaciente = P.Id
	WHERE
	E.Deleted = 1;

GO

EXECUTE spReporteEntregas;

-- Procedimiento almacenado para reporte entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReporteEntregasPlanCMes')
	DROP PROC spReporteEntregasPlanCMes;
GO
CREATE PROCEDURE spReporteEntregasPlanCMes
	@mes INT,
	@year INT
AS
	SELECT
	E.Id, E.Fecha, P.NoAfiliacion,
	P.Nombres, P.Apellidos,
	E.TipoConsulta, E.Tipo, E.Metodo,
	E.Responsable
	FROM
	EntregaAnticonceptivos E
	INNER JOIN
	Paciente P
	ON
	E.IdPaciente = P.Id
	WHERE
	E.Deleted = 1
	AND
	MONTH(CONVERT(DATE, E.Fecha, 103)) = @Mes
	AND
	YEAR(CONVERT(DATE, E.Fecha, 103)) = @Year
	ORDER BY Id DESC;

GO

EXECUTE spReporteEntregas;

GO

-- Tabla Entrega de anticonceptivos planificacion familiar A
IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'EntregaAnticonceptivosPlanA')
	CREATE TABLE EntregaAnticonceptivosPlanA
	(
		Id INT IDENTITY(1,1) NOT NULL,
		Fecha VARCHAR(15) NOT NULL,
		IdPaciente INT NOT NULL,
		Entrega VARCHAR(35) NOT NULL,
		Anticonceptivo VARCHAR(50) NOT NULL,
		--- 0 = Deleted & 1 = Active
		Deleted INT DEFAULT(1) NOT NULL,
		CONSTRAINT PK_EntregaAnticonceptivosPlanA PRIMARY KEY(Id),
		CONSTRAINT FK_EntregaAnticonceptivosPlanA_Paciente FOREIGN KEY(IdPaciente) REFERENCES Paciente(Id)
	);

GO

-- Procedimiento almacenado para mostrar entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spMostrarEntregasPlanA')
	DROP PROC spMostrarEntregasPlanA;
GO
CREATE PROCEDURE spMostrarEntregasPlanA
AS
	SELECT TOP 50
	E.Id, E.Fecha, P.NoAfiliacion,
	P.Nombres, P.Apellidos,
	E.Entrega, E.Anticonceptivo
	FROM
	EntregaAnticonceptivosPlanA E
	INNER JOIN
	Paciente P
	ON
	E.IdPaciente = P.Id
	WHERE
	E.Deleted = 1;

GO

EXECUTE spMostrarEntregasPlanA;

GO

-- Procedimiento almacenado para buscar entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spBuscarEntregasPlanA')
	DROP PROC spBuscarEntregasPlanA;
GO
CREATE PROCEDURE spBuscarEntregasPlanA
	@numA VARCHAR(9)
AS
	SELECT TOP 50
	E.Id, E.Fecha, P.NoAfiliacion,
	P.Nombres, P.Apellidos,
	E.Entrega, E.Anticonceptivo
	FROM
	EntregaAnticonceptivosPlanA E
	INNER JOIN
	Paciente P
	ON
	E.IdPaciente = P.Id
	WHERE
	E.Deleted = 1 AND P.NoAfiliacion LIKE @numA + '%';

GO

-- Procedimiento almacenado para agregar entrega
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spAgregarEntregaPlanA')
	DROP PROC spAgregarEntregaPlanA;
GO
CREATE PROCEDURE spAgregarEntregaPlanA
	@Fecha VARCHAR(10),
	@IdPaciente INT,
	@Entrega VARCHAR(35),
	@Anti VARCHAR(50)
AS
	INSERT INTO
	EntregaAnticonceptivosPlanA
	VALUES
	(@Fecha, @IdPaciente,
	@Entrega, @Anti, 1);

GO

-- Procedimiento almacenado para modificar entrega
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spModificarEntregaPlanA')
	DROP PROC spModificarEntregaPlanA;
GO
CREATE PROCEDURE spModificarEntregaPlanA
	@Id INT,
	@Fecha VARCHAR(10),
	@IdPaciente INT,
	@Entrega VARCHAR(35),
	@Anti VARCHAR(50)
AS
	UPDATE
	EntregaAnticonceptivosPlanA
	SET
	Fecha = @Fecha, IdPaciente = @IdPaciente,
	Entrega = @Entrega, Anticonceptivo = @Anti
	WHERE
	Id = @Id;

GO

-- Procedimiento almacenado para eliminar entrega
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spEliminarEntregaPlanA')
	DROP PROC spEliminarEntregaPlanA;
GO
CREATE PROCEDURE spEliminarEntregaPlanA
	@Id INT
AS
	DELETE FROM
	EntregaAnticonceptivosPlanA
	WHERE
	Id = @Id;

GO

-- Procedimiento almacenado para reporte entregas
IF EXISTS (SELECT * FROM sys.objects WHERE name = 'spReporteEntregasPlanAMes')
	DROP PROC spReporteEntregasPlanAMes;
GO
CREATE PROCEDURE spReporteEntregasPlanAMes
	@mes INT,
	@year INT
AS
	SELECT TOP 50
		E.Id, E.Fecha, P.NoAfiliacion,
		E.Entrega, E.Anticonceptivo,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'Normal'
		) 
		AS
			TotalNormal,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'Anovulatorios orales'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalOvulatorios,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'DIU T de cobre'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalDIU,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'Condones'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalCondones,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'Inyect. mensual'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalInyMen,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'Inyect. trimestral'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalInyTri,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Anticonceptivo = 'Implante'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalImplante,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Entrega = 'Primera'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalPrimera,
		(
			SELECT
				COUNT(*)
			FROM
				EntregaAnticonceptivosPlanA
			WHERE
				Deleted = 1
			AND
				Entrega = 'Primera de metodo'
			AND
				MONTH(CONVERT(DATE, Fecha, 103)) = @mes
			AND
				YEAR(CONVERT(DATE, Fecha, 103)) = @year
		) 
		AS
			TotalPrimeraMetodo
	FROM
		EntregaAnticonceptivosPlanA E
	INNER JOIN
		Paciente P
	ON
		E.IdPaciente = P.Id
	WHERE
		E.Deleted = 1
	AND
		MONTH(CONVERT(DATE, E.Fecha, 103)) = @mes
	AND
		YEAR(CONVERT(DATE, E.Fecha, 103)) = @year
	ORDER BY Id DESC;

GO

EXECUTE spReporteEntregasPlanAMes 7, 2018;