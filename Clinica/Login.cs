﻿using Clinica.Clases;
using Clinica.Vistas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica
{
    public partial class FrmLogin : Form
    {
        #region Instancias / Variales
        Usuario objUser = new Usuario();
        int move, x, y;
        #endregion

        #region Metodos del formulario
        public Boolean isValid()
        {
            Boolean estado = true;
            epError.Clear();
            if (String.IsNullOrWhiteSpace(txtUser.Text))
            {
                epError.SetError(pnlUser, "Ingrese su usuario.");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtPass.Text))
            {
                epError.SetError(pnlPass, "Ingrese su contraseña.");
                estado = false;
            }
            return estado;
        }

        void login()
        {
            if (isValid())
            {
                if (objUser.verificarUsuario(txtUser.Text.Replace("'", "''"), txtPass.Text.Replace("'", "''")))
                {
                    if (objUser.verificarEstadoUsuario(txtUser.Text.Replace("'", "''"), txtPass.Text.Replace("'", "''")))
                    {
                        if (objUser.verificarTipoUsuario(txtUser.Text.Replace("'", "''"), txtPass.Text.Replace("'", "''")))
                        {
                            // MessageBox.Show("Bienvenido/a, admin " + objUser.user, "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FrmMain objMain = new FrmMain(objUser.userActive, objUser.userTypeActive);
                            objMain.Show();
                            this.Close();
                        }
                        else
                        {
                            // MessageBox.Show("Bienvenido/a, usuario/a " + objUser.user, "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FrmMain objMain = new FrmMain(objUser.userActive, objUser.userTypeActive);
                            objMain.Show();
                            this.Close();
                        }
                    }
                    else
                    {
                        FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "Su usuario está deshabilitado.\nContacte con un administrador activo para solicitar la activación de su cuenta.", "Ok", "No", "info");
                        frmConf.ShowDialog();
                    }
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Usuario o contraseña incorrectos.\nVerifique los datos e intentelo de nuevo.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                }
            }
        }
        #endregion

        public FrmLogin()
        {
            InitializeComponent();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            Transition.run(pnlEntrar, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            Transition.run(lblEntrar, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            Transition.run(lblSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
        }

        private void FrmLogin_MouseDown(object sender, MouseEventArgs e)
        {
            move = 1;
            x = e.X;
            y = e.Y;
        }

        private void FrmLogin_MouseUp(object sender, MouseEventArgs e)
        {
            move = 0;
        }

        private void FrmLogin_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == 1)
            {
                SetDesktopLocation(MousePosition.X - x, MousePosition.Y - y);
            }
        }

        private void lblEntrar_Click(object sender, EventArgs e)
        {
            login();
        }

        private void lblSalir_Click(object sender, EventArgs e)
        {
            FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir del sistema?", "Si", "No", "question");
            if (frmConf.ShowDialog() == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void lblEntrar_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlEntrar, "BackColor", Color.FromArgb(1, 87, 155), new TransitionType_Linear(10));
            Transition.run(lblEntrar, "BackColor", Color.FromArgb(1, 87, 155), new TransitionType_Linear(10));
        }

        private void lblEntrar_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlEntrar, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(500));
            Transition.run(lblEntrar, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(500));
        }

        private void lblSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(10));
            Transition.run(lblSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(10));
        }

        private void lblSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(500));
            Transition.run(lblSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(500));
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

        private void txtUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
                login();
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
                login();
        }

    }
}
