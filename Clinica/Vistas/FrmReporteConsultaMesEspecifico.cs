﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Vistas
{
    public partial class FrmReporteConsultaMesEspecifico : Form
    {
        #region Variables
        String month, year;
        #endregion

        public FrmReporteConsultaMesEspecifico()
        {
            InitializeComponent();
        }

        public FrmReporteConsultaMesEspecifico(String mes, String anio)
        {
            InitializeComponent();
            this.month = mes;
            this.year = anio;
        }

        private void FrmReporteConsultaMesEspecifico_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dtsConsultas.spReporteConsultasMesEspecifico' Puede moverla o quitarla según sea necesario.
            this.spReporteConsultasMesEspecificoTableAdapter.Fill(this.dtsConsultas.spReporteConsultasMesEspecifico, month, year);
            this.reportViewer1.RefreshReport();
            this.Text = "Reporte de consultas realizadas el mes " + month + " del año " + year;
        }
    }
}
