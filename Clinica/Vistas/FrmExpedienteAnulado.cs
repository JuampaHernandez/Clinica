﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmExpedienteAnulado : Form
    {
        #region Instancias / Variables
        Paciente objPaciente = new Paciente();
        String userActive;
        Int32 userType;
        #endregion

        #region Métodos de la clase
        void all()
        {
            String sql = "Exec spMostrarPacientesInactivos";
            dgvPacientes.DataSource = objPaciente.all(sql);
            // Ocultando todas las columnas
            for (int i = 0; i <= 27; i++)
            {
                dgvPacientes.Columns[i].Visible = false;
            }
            dgvPacientes.Columns[1].Visible = true;
            dgvPacientes.Columns[2].Visible = true;
            dgvPacientes.Columns[3].Visible = true;
            dgvPacientes.Columns[4].Visible = true;
            dgvPacientes.Columns[8].Visible = true;
            dgvPacientes.Columns[10].Visible = true;
            lblTotalRegistros.Text = "Total de registros: " + dgvPacientes.Rows.Count;
        }

        // Método para buscar pacientes
        void find(String value)
        {
            String sql = "Exec spBuscarPacientesInactivos '" + value + "'";
            dgvPacientes.DataSource = objPaciente.all(sql);
            // Ocultando todas las columnas
            for (int i = 0; i <= 27; i++)
            {
                dgvPacientes.Columns[i].Visible = false;
            }
            dgvPacientes.Columns[1].Visible = true;
            dgvPacientes.Columns[2].Visible = true;
            dgvPacientes.Columns[3].Visible = true;
            dgvPacientes.Columns[4].Visible = true;
            dgvPacientes.Columns[8].Visible = true;
            dgvPacientes.Columns[10].Visible = true;
            lblTotalRegistros.Text = "Total de registros: " + dgvPacientes.Rows.Count;
        }

        void reset()
        {
            objPaciente.id = -1;
        }

        // Método para cargar los datos a la clase Paciente
        void load()
        {
            objPaciente.id = Convert.ToInt32(dgvPacientes.SelectedRows[0].Cells[0].Value.ToString());
            objPaciente.dui = dgvPacientes.SelectedRows[0].Cells[1].Value.ToString();
            objPaciente.noAfiliacion = dgvPacientes.SelectedRows[0].Cells[2].Value.ToString();
            objPaciente.nombres = dgvPacientes.SelectedRows[0].Cells[3].Value.ToString();
            objPaciente.apellidos = dgvPacientes.SelectedRows[0].Cells[4].Value.ToString();
            objPaciente.centroAtencion = dgvPacientes.SelectedRows[0].Cells[5].Value.ToString();
            objPaciente.fecha = dgvPacientes.SelectedRows[0].Cells[6].Value.ToString();
            objPaciente.calidad = dgvPacientes.SelectedRows[0].Cells[7].Value.ToString();
            objPaciente.sexo = dgvPacientes.SelectedRows[0].Cells[8].Value.ToString();
            objPaciente.estadoCivil = dgvPacientes.SelectedRows[0].Cells[9].Value.ToString();
            objPaciente.edad = Convert.ToInt32(dgvPacientes.SelectedRows[0].Cells[10].Value.ToString());
            objPaciente.fechaNacimiento = dgvPacientes.SelectedRows[0].Cells[11].Value.ToString();
            objPaciente.lugarNacimiento = dgvPacientes.SelectedRows[0].Cells[12].Value.ToString();
            objPaciente.direccionDomicilio = dgvPacientes.SelectedRows[0].Cells[13].Value.ToString();
            objPaciente.telDomicilio = dgvPacientes.SelectedRows[0].Cells[14].Value.ToString();
            objPaciente.direccionTrabajo = dgvPacientes.SelectedRows[0].Cells[15].Value.ToString();
            objPaciente.telTrabajo = dgvPacientes.SelectedRows[0].Cells[16].Value.ToString();
            objPaciente.ocupacion = dgvPacientes.SelectedRows[0].Cells[17].Value.ToString();
            objPaciente.patrono = dgvPacientes.SelectedRows[0].Cells[18].Value.ToString();
            objPaciente.actividadEconomica = dgvPacientes.SelectedRows[0].Cells[19].Value.ToString();
            objPaciente.nombrePadre = dgvPacientes.SelectedRows[0].Cells[20].Value.ToString();
            objPaciente.nombreMadre = dgvPacientes.SelectedRows[0].Cells[21].Value.ToString();
            objPaciente.nombreConyugue = dgvPacientes.SelectedRows[0].Cells[22].Value.ToString();
            objPaciente.nombreEmergencia = dgvPacientes.SelectedRows[0].Cells[23].Value.ToString();
            objPaciente.telEmergencia = dgvPacientes.SelectedRows[0].Cells[24].Value.ToString();
            objPaciente.proporcionadorDatos = dgvPacientes.SelectedRows[0].Cells[25].Value.ToString();
            objPaciente.archivista = dgvPacientes.SelectedRows[0].Cells[26].Value.ToString();
            objPaciente.estado = Convert.ToInt32(dgvPacientes.SelectedRows[0].Cells[27].Value.ToString());
        }

        // Metodo para cambiar imagen a escala de grises
        void grayScale(Panel pnl, Image img)
        {
            Bitmap bmp = new Bitmap(img);
            // Obteniendo dimension de imagen
            int width = bmp.Width;
            int height = bmp.Height;

            // Color del pixel
            Color p;

            // Escala de grises
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Obteniendo valor del pixel
                    p = bmp.GetPixel(x, y);

                    // Extrayendo el componente ARGB del pixel
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    // Encontrando promedio
                    int avg = (r + g + b) / 3;

                    // Asigando nuevo valor al pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                }
            }

            //load grayscale image in picturebox2
            pnl.BackgroundImage = bmp;
        }

        // Método para habilitar y deshabilitar los botones
        void enableButtons()
        {
            // Verificando si hay o no registros
            if (dgvPacientes.RowCount == 0)
            {
                pnlReset.Enabled = false;
                pnlDelete.Enabled = false;
                grayScale(pnlReset, Properties.Resources.Restart);
                grayScale(pnlDelete, Properties.Resources.Remove_User_Male);
            }
            else
            {
                pnlReset.Enabled = true;
                pnlDelete.Enabled = true;
                pnlReset.BackgroundImage = Properties.Resources.Restart;
                pnlDelete.BackgroundImage = Properties.Resources.Remove_User_Male;
            }
        }

        Boolean existsRows()
        {
            Boolean estado = false;
            if (dgvPacientes.Rows.Count > 0)
                estado = true;
            return estado;
        }


        void reactivate()
        {
            if (objPaciente.id > 0)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea reactivar el expediente del paciente " + objPaciente.nombres + " " + objPaciente.apellidos + ", número de afiliación " + objPaciente.noAfiliacion + "?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    if (objPaciente.crud("reactivate") > 0)
                    {
                        frmConf = new FrmConfirmDialog("Mensaje", "Expediente reactivado correctamente.", "Ok", "No", "info");
                        frmConf.ShowDialog();
                    }
                    else
                    {
                        frmConf = new FrmConfirmDialog("Error", "Error al reactivar el expediente.", "Ok", "No", "error");
                        frmConf.ShowDialog();
                    }
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione el expediente a reactivar.", "Ok", "No", "alert");
                frmConf.ShowDialog();
            }
            all();
            reset();
            enableButtons();
        }

        // Método para eliminar un expediente
        void delete()
        {
            if (objPaciente.id > 0)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea eliminar permanentemente el expediente del paciente " + objPaciente.nombres + " " + objPaciente.apellidos + "?\nLos datos no podran ser restaurados.", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    if (objPaciente.crud("delete") > 0)
                    {
                        frmConf = new FrmConfirmDialog("Mensaje", "Expediente eliminado correctamente.", "Ok", "No", "info");
                        frmConf.ShowDialog();
                    }
                    else
                    {
                        frmConf = new FrmConfirmDialog("Error", "Se produjjo un error al eliminar el expediente.", "Ok", "No", "error");
                        frmConf.ShowDialog();
                    }
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione el expediente a eliminar.", "Ok", "No", "alert");
                frmConf.ShowDialog();
            }
            all();
            reset();
            enableButtons();
        }
        #endregion
        public FrmExpedienteAnulado()
        {
            InitializeComponent();
        }

        public FrmExpedienteAnulado(String active, Int32 type)
        {
            InitializeComponent();
            this.userActive = active;
            this.userType = type;
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            FrmMain objMain = new FrmMain(userActive, userType);
            FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana expedientes anulados y volver al menú?", "Si", "No", "question");
            if (frmConf.ShowDialog() == DialogResult.OK)
            {
                objMain.Show();
                this.Close();
            }
        }

        private void pnlMinimize_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(35, 35, 35), new TransitionType_Linear(200));
        }

        private void pnlMinimize_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void FrmExpedienteAnulado_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            all();
            if (!existsRows())
                dgvPacientes.Enabled = false;
            enableButtons();
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void pnlDelete_Click(object sender, EventArgs e)
        {
            if (existsRows())
                delete();
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Debe haber al menos un expediente anulado para continuar.", "Ok", "No", "alert");
                frmConf.ShowDialog();
            }
        }

        private void pnlReset_Click(object sender, EventArgs e)
        {
            if (existsRows())
                reactivate();
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Debe haber al menos un expediente anulado para continuar.", "Ok", "No", "alert");
                frmConf.ShowDialog();
            }
        }

        private void txtFiltrar_TextChanged(object sender, EventArgs e)
        {
            find(txtFiltrar.Text.Replace("'", "''"));
            enableButtons();
        }

        private void dgvPacientes_Click(object sender, EventArgs e)
        {
            if (existsRows())
            {
                reset();
                load();
            }
        }

        private void pnlReset_MouseHover(object sender, EventArgs e)
        {
            pnlReset.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlReset_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlReset, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlDelete_MouseHover(object sender, EventArgs e)
        {
            pnlDelete.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlDelete_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlDelete, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void lblActivos_Click(object sender, EventArgs e)
        {
            FrmExpediente objExp = new FrmExpediente(userActive, userType);
            FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana expedientes anulados e ir a la ventana de expedientes activos?", "Si", "No", "question");
            if (frmConf.ShowDialog() == DialogResult.OK)
            {
                objExp.Show();
                this.Close();
            }
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

    }
}
