﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmExpediente : Form
    {
        #region Instancias / Variables
        Paciente objPaciente = new Paciente();
        bool adding = false, editing = false;
        String userActive;
        Int32 userType;
        #endregion

        #region Metodos de la clase
        // Método para mostrar expedietes
        void all()
        {
            String sql = "Exec spMostrarPacientesActivos";
            dgvPacientes.DataSource = objPaciente.all(sql);
            // Ocultando todas las columnas
            for (int i = 0; i <= 27; i++)
            {
                dgvPacientes.Columns[i].Visible = false;
            }
            dgvPacientes.Columns[1].Visible = true;
            dgvPacientes.Columns[2].Visible = true;
            dgvPacientes.Columns[3].Visible = true;
            dgvPacientes.Columns[4].Visible = true;
            dgvPacientes.Columns[8].Visible = true;
            dgvPacientes.Columns[10].Visible = true;
            lblTotalRegistros.Text = "Total de registros: " + dgvPacientes.Rows.Count;
        }

        // Método para buscar pacientes
        void find(String value)
        {
            String sql = "Exec spBuscarPacientesActivos '" + value + "'";
            dgvPacientes.DataSource = objPaciente.all(sql);
            // Ocultando todas las columnas
            for (int i = 0; i <= 27; i++)
            {
                dgvPacientes.Columns[i].Visible = false;
            }
            dgvPacientes.Columns[1].Visible = true;
            dgvPacientes.Columns[2].Visible = true;
            dgvPacientes.Columns[3].Visible = true;
            dgvPacientes.Columns[4].Visible = true;
            dgvPacientes.Columns[8].Visible = true;
            dgvPacientes.Columns[10].Visible = true;
            lblTotalRegistros.Text = "Total de registros: " + dgvPacientes.Rows.Count;
        }

        // Metodo para cambiar imagen a escala de grises
        void grayScale(Panel pnl, Image img)
        {
            Bitmap bmp = new Bitmap(img);
            // Obteniendo dimension de imagen
            int width = bmp.Width;
            int height = bmp.Height;

            // Color del pixel
            Color p;

            // Escala de grises
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Obteniendo valor del pixel
                    p = bmp.GetPixel(x, y);

                    // Extrayendo el componente ARGB del pixel
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    // Encontrando promedio
                    int avg = (r + g + b) / 3;

                    // Asigando nuevo valor al pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                }
            }

            //load grayscale image in picturebox2
            pnl.BackgroundImage = bmp;
        }

        // Método para habilitar y deshabilitar los botones
        void enableButtons()
        {
            // Verificando si hay o no registros
            if (dgvPacientes.RowCount == 0)
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlPrint.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_User_Male;
                grayScale(pnlEdit, Properties.Resources.Registration);
                grayScale(pnlDelete, Properties.Resources.Remove_User_Male);
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
                grayScale(pnlPrint, Properties.Resources.printer);
            }
            else
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = true;
                pnlDelete.Enabled = true;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlPrint.Enabled = true;
                pnlAdd.BackgroundImage = Properties.Resources.Add_User_Male;
                pnlEdit.BackgroundImage = Properties.Resources.Registration;
                pnlDelete.BackgroundImage = Properties.Resources.Remove_User_Male;
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
                pnlPrint.BackgroundImage = Properties.Resources.printer;
            }

            // Verificando si se está agregando
            if (adding == true || editing == true)
            {
                pnlAdd.Enabled = false;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = true;
                pnlCancel.Enabled = true;
                pnlPrint.Enabled = false;
                grayScale(pnlAdd, Properties.Resources.Add_User_Male);
                grayScale(pnlEdit, Properties.Resources.Registration);
                grayScale(pnlDelete, Properties.Resources.Remove_User_Male);
                pnlSave.BackgroundImage = Properties.Resources.Save;
                pnlCancel.BackgroundImage = Properties.Resources.Cancel;
                grayScale(pnlPrint, Properties.Resources.printer);
            }
        }

        // Método para habilitar y deshabilitar los controles
        void enableControls(Boolean option)
        {
            cmbCalidad.Enabled = option;
            cmbEstadoCivil.Enabled = option;
            cmbSexo.Enabled = option;
            txtDirDomicilio.Enabled = option;
            txtLugarNac.Enabled = option;
            txtFechaNac.Enabled = option;
            txtEdad.Enabled = option;
            txtDui.Enabled = option;
            txtNoAfiliacion.Enabled = option;
            txtNombres.Enabled = option;
            txtApellidos.Enabled = option;
            txtFecha.Enabled = option;
            txtConyugue.Enabled = option;
            txtDirTrabajo.Enabled = option;
            txtEmergencia.Enabled = option;
            txtMadre.Enabled = option;
            txtNombreArch.Enabled = option;
            txtOcupacion.Enabled = option;
            txtPadre.Enabled = option;
            txtPatrono.Enabled = option;
            txtProporcionador.Enabled = option;
            txtTelDomicilio.Enabled = option;
            txtTelEmergencia.Enabled = option;
            txtTelTrabajo.Enabled = option;
            txtCentroAtencion.Enabled = option;
            ckbAct1.Enabled = option;
            ckbAct2.Enabled = option;
            ckbAct3.Enabled = option;
            ckbAct4.Enabled = option;
            ckbAct5.Enabled = option;
            ckbAct6.Enabled = option;
            ckbAct7.Enabled = option;
            ckbAct8.Enabled = option;
            ckbAct9.Enabled = option;
            txtFiltrar.Enabled = !option;
            txtDui.Focus();
        }

        // Método para resetear controles
        void reset()
        {
            cmbCalidad.SelectedIndex = 0;
            cmbEstadoCivil.SelectedIndex = 0;
            cmbSexo.SelectedIndex = 0;
            txtDirDomicilio.Clear();
            txtLugarNac.Clear();
            txtFechaNac.Clear();
            txtEdad.Clear();
            txtDui.Clear();
            txtNoAfiliacion.Clear();
            txtNombres.Clear();
            txtApellidos.Clear();
            txtFecha.Clear();
            txtConyugue.Clear();
            txtDirTrabajo.Clear();
            txtEmergencia.Clear();
            txtMadre.Clear();
            txtNombreArch.Clear();
            txtOcupacion.Clear();
            txtPadre.Clear();
            txtPatrono.Clear();
            txtProporcionador.Clear();
            txtTelDomicilio.Clear();
            txtTelEmergencia.Clear();
            txtTelTrabajo.Clear();
            txtCentroAtencion.Clear();
            ckbAct1.Checked = false;
            ckbAct2.Checked = false;
            ckbAct3.Checked = false;
            ckbAct4.Checked = false;
            ckbAct5.Checked = false;
            ckbAct6.Checked = false;
            ckbAct7.Checked = false;
            ckbAct8.Checked = false;
            ckbAct9.Checked = false;
            txtDui.Focus();
        }

        // Método para verificar si existen registros
        Boolean existsRows()
        {
            Boolean estado = false;
            if (dgvPacientes.Rows.Count > 0)
                estado = true;
            return estado;
        }

        Boolean isValidCheckBox()
        {
            Boolean estado = false;
            if (ckbAct1.Checked == true)
                estado = true;
            if (ckbAct2.Checked == true)
                estado = true;
            if (ckbAct3.Checked == true)
                estado = true;
            if (ckbAct4.Checked == true)
                estado = true;
            if (ckbAct5.Checked == true)
                estado = true;
            if (ckbAct6.Checked == true)
                estado = true;
            if (ckbAct7.Checked == true)
                estado = true;
            if (ckbAct8.Checked == true)
                estado = true;
            if (ckbAct9.Checked == true)
                estado = true;
            return estado;
        }

        Boolean isValid()
        {
            Boolean estado = true;
            epError.Clear();
            if (cmbCalidad.SelectedIndex == 0)
            {
                epError.SetError(cmbCalidad, "Seleccione la calidad del paciente");
                estado = false;
            }
            if (cmbEstadoCivil.SelectedIndex == 0)
            {
                epError.SetError(cmbEstadoCivil, "Seleccione el estado civil del paciente");    
                estado = false;
            }
            if (cmbSexo.SelectedIndex == 0)
            {
                epError.SetError(cmbSexo, "Seleccione el sexo del paciente");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtDui.Text))
            {
                epError.SetError(txtDui, "Ingrese el DUI del paciente");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtNoAfiliacion.Text))
            {
                epError.SetError(txtNoAfiliacion, "Ingrese el # de afiliación del paciente");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtFecha.Text))
            {
                epError.SetError(txtFecha, "Ingrese la fecha");
                estado = false;
            }
            else
            {
                try
                {
                    String fecha = Convert.ToDateTime(txtFecha.Text).ToString("dd/MM/yyyy");
                    Convert.ToDateTime(fecha);
                }
                catch
                {
                    epError.SetError(txtFecha, "Ingrese una fecha correcta");
                    estado = false;
                }
            }
            if (String.IsNullOrEmpty(txtEdad.Text))
            {
                epError.SetError(txtEdad, "Ingrese la edad del paciente");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtFechaNac.Text))
            {
                epError.SetError(txtFechaNac, "Ingrese la fecha de nacimineto del paciente");
                estado = false;
            }
            else
            {
                try
                {
                    String fecha = Convert.ToDateTime(txtFechaNac.Text).ToString("dd/MM/yyyy");
                    Convert.ToDateTime(fecha);
                }
                catch
                {
                    epError.SetError(txtFechaNac, "Ingrese una fecha correcta");
                    estado = false;
                }
            }
            if (String.IsNullOrEmpty(txtTelDomicilio.Text))
            {
                epError.SetError(txtTelDomicilio, "Ingrese el teléfono del domicilio actual del paciente");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtTelTrabajo.Text))
            {
                epError.SetError(txtTelTrabajo, "Ingrese el teléfono del trabajo actual del paciente");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtTelEmergencia.Text))
            {
                epError.SetError(txtTelEmergencia, "Ingrese el teléfono de emergencia del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtNombres.Text))
            {
                epError.SetError(txtNombres, "Ingrese el(los) nombre(s) del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtApellidos.Text))
            {
                epError.SetError(txtApellidos, "Ingrese el(los) apellido(s) del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtCentroAtencion.Text))
            {
                epError.SetError(txtCentroAtencion, "Ingrese el centro de atención");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtDirDomicilio.Text))
            {
                epError.SetError(txtDirDomicilio, "Ingrese la dirección del domicilio actual del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtDirTrabajo.Text))
            {
                epError.SetError(txtDirTrabajo, "Ingrese la dirección del trabajo actual del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtOcupacion.Text))
            {
                epError.SetError(txtOcupacion, "Ingrese la ocupación del trabajo actual del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtPatrono.Text))
            {
                epError.SetError(txtPatrono, "Ingrese el nombre del patrón del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtLugarNac.Text))
            {
                epError.SetError(txtLugarNac, "Ingrese el lugar de nacimiento del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtPadre.Text))
            {
                epError.SetError(txtPadre, "Ingrese el nombre del padre del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtMadre.Text))
            {
                epError.SetError(txtMadre, "Ingrese el nombre de la madre del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtConyugue.Text))
                objPaciente.nombreConyugue = "N/A";
            else
                objPaciente.nombreConyugue = txtConyugue.Text.Replace("'", "''");
            if (String.IsNullOrWhiteSpace(txtEmergencia.Text))
            {
                epError.SetError(txtEmergencia, "Ingrese el telefono de emergencia del paciente");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtNombreArch.Text))
            {
                epError.SetError(txtNombreArch, "Ingrese el nombre del archivista");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtProporcionador.Text))
            {
                epError.SetError(txtProporcionador, "Ingrese el nombre del proporcionador");
                estado = false;
            }
            if (!isValidCheckBox())
            {
                epError.SetError(ckbAct1, "Seleccione al menos una actividad economica");
                estado = false;
            }
            return estado;
        }

        // Método para cargar los datos a la clase Paciente
        void load()
        {
            objPaciente.id = Convert.ToInt32(dgvPacientes.SelectedRows[0].Cells[0].Value.ToString());
            objPaciente.dui = dgvPacientes.SelectedRows[0].Cells[1].Value.ToString();
            objPaciente.noAfiliacion = dgvPacientes.SelectedRows[0].Cells[2].Value.ToString();
            objPaciente.nombres = dgvPacientes.SelectedRows[0].Cells[3].Value.ToString();
            objPaciente.apellidos = dgvPacientes.SelectedRows[0].Cells[4].Value.ToString();
            objPaciente.centroAtencion = dgvPacientes.SelectedRows[0].Cells[5].Value.ToString();
            objPaciente.fecha = dgvPacientes.SelectedRows[0].Cells[6].Value.ToString();
            objPaciente.calidad = dgvPacientes.SelectedRows[0].Cells[7].Value.ToString();
            objPaciente.sexo = dgvPacientes.SelectedRows[0].Cells[8].Value.ToString();
            objPaciente.estadoCivil = dgvPacientes.SelectedRows[0].Cells[9].Value.ToString();
            objPaciente.edad = Convert.ToInt32(dgvPacientes.SelectedRows[0].Cells[10].Value.ToString());
            objPaciente.fechaNacimiento = dgvPacientes.SelectedRows[0].Cells[11].Value.ToString();
            objPaciente.lugarNacimiento = dgvPacientes.SelectedRows[0].Cells[12].Value.ToString();
            objPaciente.direccionDomicilio = dgvPacientes.SelectedRows[0].Cells[13].Value.ToString();
            objPaciente.telDomicilio = dgvPacientes.SelectedRows[0].Cells[14].Value.ToString();
            objPaciente.direccionTrabajo = dgvPacientes.SelectedRows[0].Cells[15].Value.ToString();
            objPaciente.telTrabajo = dgvPacientes.SelectedRows[0].Cells[16].Value.ToString();
            objPaciente.ocupacion = dgvPacientes.SelectedRows[0].Cells[17].Value.ToString();
            objPaciente.patrono = dgvPacientes.SelectedRows[0].Cells[18].Value.ToString();
            objPaciente.actividadEconomica = dgvPacientes.SelectedRows[0].Cells[19].Value.ToString();
            objPaciente.nombrePadre = dgvPacientes.SelectedRows[0].Cells[20].Value.ToString();
            objPaciente.nombreMadre = dgvPacientes.SelectedRows[0].Cells[21].Value.ToString();
            objPaciente.nombreConyugue = dgvPacientes.SelectedRows[0].Cells[22].Value.ToString();
            objPaciente.nombreEmergencia = dgvPacientes.SelectedRows[0].Cells[23].Value.ToString();
            objPaciente.telEmergencia = dgvPacientes.SelectedRows[0].Cells[24].Value.ToString();
            objPaciente.proporcionadorDatos = dgvPacientes.SelectedRows[0].Cells[25].Value.ToString();
            objPaciente.archivista = dgvPacientes.SelectedRows[0].Cells[26].Value.ToString();
            objPaciente.estado = Convert.ToInt32(dgvPacientes.SelectedRows[0].Cells[27].Value.ToString());
        }

        // Método para leer atributos y mostrar en ComboBox
        void readComboBox()
        {
            // Combox Calidad
            if (objPaciente.calidad == "Asegurado")
                cmbCalidad.SelectedIndex = 1;
            else if (objPaciente.calidad == "Beneficiario(a)")
                cmbCalidad.SelectedIndex = 2;
            else if (objPaciente.calidad == "Pensionado")
                cmbCalidad.SelectedIndex = 3;
            else if (objPaciente.calidad == "Hijo")
                cmbCalidad.SelectedIndex = 4;
            // ComboBox Sexo
            if (objPaciente.sexo.Contains("Masculino"))
                cmbSexo.SelectedIndex = 1;
            else if (objPaciente.sexo.Contains("Femenino"))
                cmbSexo.SelectedIndex = 2;
            // ComboBox Estado Civil
            if (objPaciente.estadoCivil == "Soltero")
                cmbEstadoCivil.SelectedIndex = 1;
            else if (objPaciente.estadoCivil == "Casado")
                cmbEstadoCivil.SelectedIndex = 2;
            else if (objPaciente.estadoCivil == "Divorciado")
                cmbEstadoCivil.SelectedIndex = 3;
            else if (objPaciente.estadoCivil == "Viudo")
                cmbEstadoCivil.SelectedIndex = 4;
        }

        // Método para leer atributo ActividadEconomica y mostrar en checkbox
        void readCheckBox()
        {
            if (objPaciente.actividadEconomica.Contains("Agricultura, Caza, Silvicultura y Pezca"))
                ckbAct1.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Explotacón de Minas y Canteras"))
                ckbAct2.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Industrias Manufactureras"))
                ckbAct3.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Electricidad, Gaz y Agua"))
                ckbAct4.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Construcción"))
                ckbAct5.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Comercio por mayor y menor, Restaurantes, Hotel"))
                ckbAct6.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Transportes, Almacenamiento y Comunicaciónes"))
                ckbAct7.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Establecimientos financieros, seguros, bienes inmuebles y servicios prestados a empresas"))
                ckbAct8.Checked = true;
            if (objPaciente.actividadEconomica.Contains("Servicios comunales, sociales y personales"))
                ckbAct9.Checked = true;
        }

        // Enviar datos a la clase Paciente
        void send()
        {
            String act = "";
            if (ckbAct1.Checked == true)
                act += "Agricultura, Caza, Silvicultura y Pezca ";
            if (ckbAct2.Checked == true)
                act += "Explotacón de Minas y Canteras ";
            if (ckbAct3.Checked == true)
                act += "Industrias Manufactureras ";
            if (ckbAct4.Checked == true)
                act += "Electricidad, Gaz y Agua ";
            if (ckbAct5.Checked == true)
                act += "Construcción ";
            if (ckbAct6.Checked == true)
                act += "Comercio por mayor y menor, Restaurantes, Hotel ";
            if (ckbAct7.Checked == true)
                act += "Transportes, Almacenamiento y Comunicaciónes ";
            if (ckbAct8.Checked == true)
                act += "Establecimientos financieros, seguros, bienes inmuebles y servicios prestados a empresas ";
            if (ckbAct9.Checked == true)
                act += "Servicios comunales, sociales y personales ";
            objPaciente.dui = txtDui.Text.Replace("'", "''");
            objPaciente.noAfiliacion = txtNoAfiliacion.Text.Replace("'", "''");
            objPaciente.nombres = txtNombres.Text.Replace("'", "''");
            objPaciente.apellidos = txtApellidos.Text.Replace("'", "''");
            objPaciente.centroAtencion = txtCentroAtencion.Text.Replace("'", "''");
            objPaciente.fecha = txtFecha.Text.Replace("'", "''");
            objPaciente.calidad = cmbCalidad.Text.Replace("'", "''");
            objPaciente.sexo = cmbSexo.Text.Replace("'", "''");
            objPaciente.estadoCivil = cmbEstadoCivil.Text.Replace("'", "''");
            objPaciente.edad = Convert.ToInt32(txtEdad.Text.Replace("'", "''"));
            objPaciente.fechaNacimiento = txtFechaNac.Text.Replace("'", "''");
            objPaciente.lugarNacimiento = txtLugarNac.Text.Replace("'", "''");
            objPaciente.direccionDomicilio = txtDirDomicilio.Text.Replace("'", "''");
            objPaciente.telDomicilio = txtTelDomicilio.Text.Replace("'", "''");
            objPaciente.direccionTrabajo = txtDirTrabajo.Text.Replace("'", "''");
            objPaciente.telTrabajo = txtTelTrabajo.Text.Replace("'", "''");
            objPaciente.ocupacion = txtOcupacion.Text.Replace("'", "''");
            objPaciente.patrono = txtPatrono.Text.Replace("'", "''");
            objPaciente.actividadEconomica = act.Replace("'", "''");
            objPaciente.nombrePadre = txtPadre.Text.Replace("'", "''");
            objPaciente.nombreMadre = txtMadre.Text.Replace("'", "''");
            objPaciente.nombreEmergencia = txtEmergencia.Text.Replace("'", "''");
            objPaciente.telEmergencia = txtTelEmergencia.Text.Replace("'", "''");
            objPaciente.proporcionadorDatos = txtProporcionador.Text.Replace("'", "''");
            objPaciente.archivista = txtNombreArch.Text.Replace("'", "''");
            objPaciente.estado = 1;
        }

        // Método para leer los atributos de la clase Paciente y mostrarlos en controles
        void read()
        {
            txtDui.Text = objPaciente.dui;
            txtNoAfiliacion.Text = objPaciente.noAfiliacion;
            txtNombres.Text = objPaciente.nombres;
            txtApellidos.Text = objPaciente.apellidos;
            txtCentroAtencion.Text = objPaciente.centroAtencion;
            txtFecha.Text = objPaciente.fecha;
            txtEdad.Text = objPaciente.edad.ToString();
            txtFechaNac.Text = objPaciente.fechaNacimiento;
            txtLugarNac.Text = objPaciente.lugarNacimiento;
            txtDirDomicilio.Text = objPaciente.direccionDomicilio;
            txtTelDomicilio.Text = objPaciente.telDomicilio;
            txtDirTrabajo.Text = objPaciente.direccionTrabajo;
            txtTelTrabajo.Text = objPaciente.telTrabajo;
            txtOcupacion.Text = objPaciente.ocupacion;
            txtPatrono.Text = objPaciente.patrono;
            txtPadre.Text = objPaciente.nombrePadre;
            txtMadre.Text = objPaciente.nombreMadre;
            txtConyugue.Text = objPaciente.nombreConyugue;
            txtEmergencia.Text = objPaciente.nombreEmergencia;
            txtTelEmergencia.Text = objPaciente.telEmergencia;
            txtProporcionador.Text = objPaciente.proporcionadorDatos;
            txtNombreArch.Text = objPaciente.archivista;
            readCheckBox();
            readComboBox();
        }
        #endregion

        #region CRUD
        void add()
        {
            adding = true;
            editing = false;
            reset();
            enableControls(true);
            enableButtons();
            dgvPacientes.Enabled = false;
            tbcPacientes.SelectedIndex = 1;
            String fecha = DateTime.Today.ToString("dd/MM/yyyy");
            txtFecha.Text = fecha;
        }

        void edit()
        {
            try
            {
                if (isValid())
                {
                    adding = false;
                    editing = true;
                    enableControls(true);
                    dgvPacientes.Enabled = false;
                    tbcPacientes.SelectedIndex = 1;
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Selecione un expediente para editar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcPacientes.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void delete()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea anular el expediente del paciente " + objPaciente.nombres + " " + objPaciente.apellidos + ", número de afiliación " + objPaciente.noAfiliacion + "?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        if (objPaciente.crud("null") > 0)
                        {
                            frmConf = new FrmConfirmDialog("Mensaje", "Expediente anulado correctamente.", "Ok", "No", "info");
                            frmConf.ShowDialog();
                        }
                        else
                        {
                            frmConf = new FrmConfirmDialog("Error", "Error al anular el expediente.", "Ok", "No", "error");
                            frmConf.ShowDialog();
                        }
                        all();
                        reset();
                        tbcPacientes.SelectedIndex = 0;
                    }
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione un expediete para eliminar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcPacientes.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void save()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea guardar el expediente?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        send();
                        if (adding == true && editing == false) // Si se está Ingresando Datos
                        {
                            if (objPaciente.crud("save") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Expediente guardado correctamente.", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcPacientes.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al guardar el expediente.", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        else if (adding == false && editing == true) // Si se está Modificando Datos
                        {
                            if (objPaciente.crud("update") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Expediente editado correctamente", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcPacientes.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al editar el expediente", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        adding = false;
                        editing = false;
                        reset();
                        enableControls(false);
                        dgvPacientes.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void cancel()
        {
            adding = false;
            editing = false;
            reset();
            enableButtons();
            enableControls(false);
            epError.Clear();
            dgvPacientes.Enabled = true;
            tbcPacientes.SelectedIndex = 0;
        }

        #endregion

        public FrmExpediente()
        {
            InitializeComponent();
        }

        public FrmExpediente(String active, Int32 type)
        {
            InitializeComponent();
            this.userActive = active;
            this.userType = type;
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void FrmExpediente_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            all();
            reset();
            enableControls(false);
            enableButtons();
            if (!existsRows())
                dgvPacientes.Enabled = false;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            FrmMain objMain = new FrmMain(userActive, userType);
            if (!adding && !editing)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana expedientes y volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "Todos los datos no guardados se perderan.\n¿Seguro que desea volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
        }

        private void pnlAdd_MouseHover(object sender, EventArgs e)
        {
            pnlAdd.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlAdd_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlAdd, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlEdit_MouseHover(object sender, EventArgs e)
        {
            pnlEdit.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlEdit_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlEdit, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlDelete_MouseHover(object sender, EventArgs e)
        {
            pnlDelete.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlDelete_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlDelete, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlSave_MouseHover(object sender, EventArgs e)
        {
            pnlSave.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlSave_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSave, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlCancel_MouseHover(object sender, EventArgs e)
        {
            pnlCancel.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlCancel_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlCancel, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void dgvPacientes_Click(object sender, EventArgs e)
        {
            if (dgvPacientes.Rows.Count > 0)
            {
                reset();
                load();
                read();
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "Debe haber al menos un paciente registrado para continuar.", "Ok", "No", "info");
                frmConf.ShowDialog();
            }
        }

        private void dgvPacientes_DoubleClick(object sender, EventArgs e)
        {
            tbcPacientes.SelectedIndex = 1;
        }

        private void pnlMinimize_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(35, 35, 35), new TransitionType_Linear(200));
        }

        private void pnlMinimize_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pnlAdd_Click(object sender, EventArgs e)
        {
            add();
        }

        private void pnlEdit_Click(object sender, EventArgs e)
        {
            edit();
        }

        private void pnlDelete_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void pnlSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void pnlCancel_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void txtFiltrar_TextChanged(object sender, EventArgs e)
        {
            find(txtFiltrar.Text.Replace("'", "''"));
            enableButtons();
        }

        private void lblAnulados_Click(object sender, EventArgs e)
        {
            FrmExpedienteAnulado objExpAn = new FrmExpedienteAnulado(userActive, userType);
            if (!adding && !editing)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana expedientes e ir a la ventana de expedientes anulados?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objExpAn.Show();
                    this.Close();
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "Todos los datos no guardados se perderan.\n¿Seguro que desea volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objExpAn.Show();
                    this.Close();
                }
            }
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

        private void pnlPrint_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                FrmReportePaciente frmRepPac = new FrmReportePaciente(objPaciente.id);
                frmRepPac.Show();
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Selecione un expediente para imprimir.\nSe recomienda dar doble click sobre el expediente deseado.", "Ok", "No", "alert");
                frmConf.ShowDialog();
                epError.Clear();
                tbcPacientes.SelectedIndex = 0;
            }
        }

    }
}
