﻿namespace Clinica.Vistas
{
    partial class FrmReporteConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReporteConsulta));
            this.spReporteConsultasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtsConsultas = new Clinica.DataSets.dtsConsultas();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.spReporteConsultasTableAdapter = new Clinica.DataSets.dtsConsultasTableAdapters.spReporteConsultasTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.spReporteConsultasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsConsultas)).BeginInit();
            this.SuspendLayout();
            // 
            // spReporteConsultasBindingSource
            // 
            this.spReporteConsultasBindingSource.DataMember = "spReporteConsultas";
            this.spReporteConsultasBindingSource.DataSource = this.dtsConsultas;
            // 
            // dtsConsultas
            // 
            this.dtsConsultas.DataSetName = "dtsConsultas";
            this.dtsConsultas.EnforceConstraints = false;
            this.dtsConsultas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportViewer1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            reportDataSource1.Name = "Consultas";
            reportDataSource1.Value = this.spReporteConsultasBindingSource;
            reportDataSource2.Name = "ReporteConsultas";
            reportDataSource2.Value = this.spReporteConsultasBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Clinica.Reportes.rptConsultas.rdlc";
            this.reportViewer1.LocalReport.ReportPath = "../../Reportes/rptConsultas.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ShowBackButton = false;
            this.reportViewer1.ShowContextMenu = false;
            this.reportViewer1.ShowCredentialPrompts = false;
            this.reportViewer1.ShowDocumentMapButton = false;
            this.reportViewer1.ShowFindControls = false;
            this.reportViewer1.ShowPageNavigationControls = false;
            this.reportViewer1.ShowParameterPrompts = false;
            this.reportViewer1.ShowPromptAreaButton = false;
            this.reportViewer1.ShowStopButton = false;
            this.reportViewer1.Size = new System.Drawing.Size(932, 549);
            this.reportViewer1.TabIndex = 0;
            // 
            // spReporteConsultasTableAdapter
            // 
            this.spReporteConsultasTableAdapter.ClearBeforeFill = true;
            // 
            // FrmReporteConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(50)))), ((int)(((byte)(56)))));
            this.ClientSize = new System.Drawing.Size(932, 549);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmReporteConsulta";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reporte general de últimas 50 consultas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmReporteConsulta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.spReporteConsultasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsConsultas)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private DataSets.dtsConsultas dtsConsultas;
        private System.Windows.Forms.BindingSource spReporteConsultasBindingSource;
        private DataSets.dtsConsultasTableAdapters.spReporteConsultasTableAdapter spReporteConsultasTableAdapter;
    }
}