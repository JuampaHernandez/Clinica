﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Vistas
{
    public partial class SplashScreen : Form
    {
        #region Variables / Instancias
        Int16 i = 0;
        FrmLogin objLogin = new FrmLogin();
        #endregion
        public SplashScreen()
        {
            InitializeComponent();
        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            tmrContador.Start();
        }

        private void tmrContador_Tick(object sender, EventArgs e)
        {
            if (i < 50)
                i++;
            else
            {
                tmrContador.Stop();
                objLogin.Show();
                this.Hide();
            }
        }
    }
}
