﻿namespace Clinica.Vistas
{
    partial class FrmExpedienteAnulado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExpedienteAnulado));
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.pnlMinimize = new System.Windows.Forms.Panel();
            this.pnlSalir = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlReset = new System.Windows.Forms.Panel();
            this.pnlDelete = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.tbcPacientes = new System.Windows.Forms.TabControl();
            this.tbpBuscar = new System.Windows.Forms.TabPage();
            this.lblActivos = new System.Windows.Forms.Label();
            this.lblTotalRegistros = new System.Windows.Forms.Label();
            this.txtFiltrar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.dgvPacientes = new System.Windows.Forms.DataGridView();
            this.pcbLogo = new System.Windows.Forms.PictureBox();
            this.tltAyuda = new System.Windows.Forms.ToolTip(this.components);
            this.pnlTitulo.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbcPacientes.SuspendLayout();
            this.tbpBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacientes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.pnlTitulo.Controls.Add(this.pnlMinimize);
            this.pnlTitulo.Controls.Add(this.pnlSalir);
            this.pnlTitulo.Controls.Add(this.label1);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(1362, 39);
            this.pnlTitulo.TabIndex = 0;
            // 
            // pnlMinimize
            // 
            this.pnlMinimize.BackgroundImage = global::Clinica.Properties.Resources.Horizontal_Line_96px;
            this.pnlMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMinimize.Location = new System.Drawing.Point(1286, 0);
            this.pnlMinimize.Name = "pnlMinimize";
            this.pnlMinimize.Size = new System.Drawing.Size(38, 39);
            this.pnlMinimize.TabIndex = 2;
            this.tltAyuda.SetToolTip(this.pnlMinimize, "Minimizar");
            this.pnlMinimize.Click += new System.EventHandler(this.pnlMinimize_Click);
            this.pnlMinimize.MouseLeave += new System.EventHandler(this.pnlMinimize_MouseLeave);
            this.pnlMinimize.MouseHover += new System.EventHandler(this.pnlMinimize_MouseHover);
            // 
            // pnlSalir
            // 
            this.pnlSalir.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.pnlSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSalir.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSalir.Location = new System.Drawing.Point(1324, 0);
            this.pnlSalir.Name = "pnlSalir";
            this.pnlSalir.Size = new System.Drawing.Size(38, 39);
            this.pnlSalir.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlSalir, "Cerrar");
            this.pnlSalir.Click += new System.EventHandler(this.pnlSalir_Click);
            this.pnlSalir.MouseLeave += new System.EventHandler(this.pnlSalir_MouseLeave);
            this.pnlSalir.MouseHover += new System.EventHandler(this.pnlSalir_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semilight", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "EXPEDIENTES ANULADOS";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pnlReset);
            this.panel1.Controls.Add(this.pnlDelete);
            this.panel1.Location = new System.Drawing.Point(1235, 74);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(119, 56);
            this.panel1.TabIndex = 2;
            // 
            // pnlReset
            // 
            this.pnlReset.BackgroundImage = global::Clinica.Properties.Resources.Restart;
            this.pnlReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlReset.Location = new System.Drawing.Point(15, 9);
            this.pnlReset.Name = "pnlReset";
            this.pnlReset.Size = new System.Drawing.Size(38, 39);
            this.pnlReset.TabIndex = 0;
            this.tltAyuda.SetToolTip(this.pnlReset, "Recuperar expediente");
            this.pnlReset.Click += new System.EventHandler(this.pnlReset_Click);
            this.pnlReset.MouseLeave += new System.EventHandler(this.pnlReset_MouseLeave);
            this.pnlReset.MouseHover += new System.EventHandler(this.pnlReset_MouseHover);
            // 
            // pnlDelete
            // 
            this.pnlDelete.BackgroundImage = global::Clinica.Properties.Resources.Remove_User_Male;
            this.pnlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelete.Location = new System.Drawing.Point(66, 9);
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.Size = new System.Drawing.Size(38, 39);
            this.pnlDelete.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlDelete, "Eliminar definitivamente");
            this.pnlDelete.Click += new System.EventHandler(this.pnlDelete_Click);
            this.pnlDelete.MouseLeave += new System.EventHandler(this.pnlDelete_MouseLeave);
            this.pnlDelete.MouseHover += new System.EventHandler(this.pnlDelete_MouseHover);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(115, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(580, 47);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lista de pacientes inactivos (anulados)";
            // 
            // tbcPacientes
            // 
            this.tbcPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcPacientes.Controls.Add(this.tbpBuscar);
            this.tbcPacientes.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcPacientes.Location = new System.Drawing.Point(12, 150);
            this.tbcPacientes.Name = "tbcPacientes";
            this.tbcPacientes.SelectedIndex = 0;
            this.tbcPacientes.Size = new System.Drawing.Size(1342, 571);
            this.tbcPacientes.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcPacientes.TabIndex = 3;
            // 
            // tbpBuscar
            // 
            this.tbpBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpBuscar.Controls.Add(this.lblActivos);
            this.tbpBuscar.Controls.Add(this.lblTotalRegistros);
            this.tbpBuscar.Controls.Add(this.txtFiltrar);
            this.tbpBuscar.Controls.Add(this.lblBuscar);
            this.tbpBuscar.Controls.Add(this.dgvPacientes);
            this.tbpBuscar.Location = new System.Drawing.Point(4, 26);
            this.tbpBuscar.Name = "tbpBuscar";
            this.tbpBuscar.Padding = new System.Windows.Forms.Padding(3);
            this.tbpBuscar.Size = new System.Drawing.Size(1334, 541);
            this.tbpBuscar.TabIndex = 0;
            this.tbpBuscar.Text = "Pacientes";
            // 
            // lblActivos
            // 
            this.lblActivos.AutoSize = true;
            this.lblActivos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.lblActivos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblActivos.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lblActivos.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lblActivos.Location = new System.Drawing.Point(1250, 6);
            this.lblActivos.Margin = new System.Windows.Forms.Padding(3);
            this.lblActivos.Name = "lblActivos";
            this.lblActivos.Padding = new System.Windows.Forms.Padding(2);
            this.lblActivos.Size = new System.Drawing.Size(79, 23);
            this.lblActivos.TabIndex = 2;
            this.lblActivos.Text = "Ver activos";
            this.lblActivos.Click += new System.EventHandler(this.lblActivos_Click);
            // 
            // lblTotalRegistros
            // 
            this.lblTotalRegistros.AutoSize = true;
            this.lblTotalRegistros.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalRegistros.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRegistros.ForeColor = System.Drawing.Color.Gray;
            this.lblTotalRegistros.Location = new System.Drawing.Point(1166, 62);
            this.lblTotalRegistros.Name = "lblTotalRegistros";
            this.lblTotalRegistros.Size = new System.Drawing.Size(116, 19);
            this.lblTotalRegistros.TabIndex = 3;
            this.lblTotalRegistros.Text = "Total de registros: ";
            // 
            // txtFiltrar
            // 
            this.txtFiltrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.txtFiltrar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFiltrar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiltrar.ForeColor = System.Drawing.Color.LightGray;
            this.txtFiltrar.Location = new System.Drawing.Point(37, 43);
            this.txtFiltrar.Name = "txtFiltrar";
            this.txtFiltrar.Size = new System.Drawing.Size(338, 22);
            this.txtFiltrar.TabIndex = 1;
            this.txtFiltrar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtFiltrar, "Ingrese el N° de afiliación");
            this.txtFiltrar.TextChanged += new System.EventHandler(this.txtFiltrar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.BackColor = System.Drawing.Color.Transparent;
            this.lblBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.Location = new System.Drawing.Point(33, 19);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(59, 21);
            this.lblBuscar.TabIndex = 0;
            this.lblBuscar.Text = "Buscar:";
            // 
            // dgvPacientes
            // 
            this.dgvPacientes.AllowUserToAddRows = false;
            this.dgvPacientes.AllowUserToDeleteRows = false;
            this.dgvPacientes.AllowUserToResizeColumns = false;
            this.dgvPacientes.AllowUserToResizeRows = false;
            this.dgvPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPacientes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvPacientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPacientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPacientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPacientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPacientes.ColumnHeadersHeight = 40;
            this.dgvPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPacientes.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPacientes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPacientes.EnableHeadersVisualStyles = false;
            this.dgvPacientes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvPacientes.Location = new System.Drawing.Point(6, 84);
            this.dgvPacientes.MultiSelect = false;
            this.dgvPacientes.Name = "dgvPacientes";
            this.dgvPacientes.ReadOnly = true;
            this.dgvPacientes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPacientes.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.LightGray;
            this.dgvPacientes.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPacientes.RowTemplate.Height = 35;
            this.dgvPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPacientes.Size = new System.Drawing.Size(1322, 451);
            this.dgvPacientes.TabIndex = 4;
            this.dgvPacientes.Click += new System.EventHandler(this.dgvPacientes_Click);
            // 
            // pcbLogo
            // 
            this.pcbLogo.Image = global::Clinica.Properties.Resources.Hospital;
            this.pcbLogo.Location = new System.Drawing.Point(22, 55);
            this.pcbLogo.Name = "pcbLogo";
            this.pcbLogo.Size = new System.Drawing.Size(87, 83);
            this.pcbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbLogo.TabIndex = 18;
            this.pcbLogo.TabStop = false;
            // 
            // tltAyuda
            // 
            this.tltAyuda.AutomaticDelay = 400;
            this.tltAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.tltAyuda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tltAyuda.OwnerDraw = true;
            this.tltAyuda.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.tltAyuda_Draw);
            // 
            // FrmExpedienteAnulado
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(1362, 733);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pcbLogo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbcPacientes);
            this.Controls.Add(this.pnlTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Light", 14F);
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmExpedienteAnulado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Anulados";
            this.Load += new System.EventHandler(this.FrmExpedienteAnulado_Load);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tbcPacientes.ResumeLayout(false);
            this.tbpBuscar.ResumeLayout(false);
            this.tbpBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacientes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Panel pnlMinimize;
        private System.Windows.Forms.Panel pnlSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlReset;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.PictureBox pcbLogo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tbcPacientes;
        private System.Windows.Forms.TabPage tbpBuscar;
        private System.Windows.Forms.Label lblTotalRegistros;
        private System.Windows.Forms.TextBox txtFiltrar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.DataGridView dgvPacientes;
        private System.Windows.Forms.Label lblActivos;
        private System.Windows.Forms.ToolTip tltAyuda;
    }
}