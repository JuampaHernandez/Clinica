﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Vistas
{
    public partial class FrmReportePaciente : Form
    {
        #region
        int idPaciente;
        #endregion
        public FrmReportePaciente()
        {
            InitializeComponent();
        }

        public FrmReportePaciente(int id)
        {
            InitializeComponent();
            this.idPaciente = id;
        }

        private void FrmReportePaciente_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dtsConsultas.spReporteConsultasMes' Puede moverla o quitarla según sea necesario.
            this.spReportePacienteTableAdapter.Fill(this.dtsPacientes.spReportePaciente, idPaciente);
            this.reportViewer1.RefreshReport();
        }
    }
}
