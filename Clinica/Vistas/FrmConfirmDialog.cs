﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmConfirmDialog : Form
    {
        #region Variables
        String titulo, descripcion, okText, cancelText, tipo;
        int move, x, y;
        #endregion
        public FrmConfirmDialog()
        {
            InitializeComponent();
            this.titulo = "Mensaje";
            this.descripcion = "Descripción del mensaje.";
            this.okText = "Si";
            this.cancelText = "No";
            this.tipo = "question";
        }

        public FrmConfirmDialog(String title, String desc, String ok, String cancel, String type)
        {
            InitializeComponent();
            this.titulo = title;
            this.descripcion = desc;
            this.okText = ok;
            this.cancelText = cancel;
            this.tipo = type;
        }

        private void FrmConfirmDialog_Load(object sender, EventArgs e)
        {
            lblTitulo.Text = this.titulo;
            lblDescripcion.Text = this.descripcion;
            btnOk.Text = this.okText;
            btnCancel.Text = this.cancelText;
            if (this.tipo == "question")
                pnlLogo.BackgroundImage = Clinica.Properties.Resources.quest;
            else if (this.tipo == "info")
            {
                pnlLogo.BackgroundImage = Clinica.Properties.Resources.info;
                btnCancel.Visible = false;
                btnOk.Location = new Point(366, 127);
            }
            else if (this.tipo == "infoRec")
                pnlLogo.BackgroundImage = Clinica.Properties.Resources.info;
            else if (this.tipo == "alert")
            {
                pnlLogo.BackgroundImage = Clinica.Properties.Resources.alert;
                btnCancel.Visible = false;
                btnOk.Location = new Point(366, 127);
            }
            else if (this.tipo == "error")
            {
                pnlLogo.BackgroundImage = Clinica.Properties.Resources.error;
                btnCancel.Visible = false;
                btnOk.Location = new Point(366, 127);
            }
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(21, 21, 21), new TransitionType_Linear(200));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (this.tipo == "question" || this.tipo == "infoRec")
                this.DialogResult = DialogResult.OK;
            else
                this.Close();
        }

        private void FrmConfirmDialog_MouseDown(object sender, MouseEventArgs e)
        {
            move = 1;
            x = e.X;
            y = e.Y;
        }

        private void FrmConfirmDialog_MouseUp(object sender, MouseEventArgs e)
        {
            move = 0;
        }

        private void FrmConfirmDialog_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == 1)
                SetDesktopLocation(MousePosition.X - x, MousePosition.Y - y);
        }
    }
}
