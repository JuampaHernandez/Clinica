﻿namespace Clinica.Vistas
{
    partial class FrmPlanificacionC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPlanificacionC));
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.pnlMinimize = new System.Windows.Forms.Panel();
            this.pnlSalir = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.tltAyuda = new System.Windows.Forms.ToolTip(this.components);
            this.pnlAdd = new System.Windows.Forms.Panel();
            this.pnlCancel = new System.Windows.Forms.Panel();
            this.pnlDelete = new System.Windows.Forms.Panel();
            this.pnlSave = new System.Windows.Forms.Panel();
            this.pnlEdit = new System.Windows.Forms.Panel();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.rdbSubsecuente = new System.Windows.Forms.RadioButton();
            this.rdbInscripcion = new System.Windows.Forms.RadioButton();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.txtResp = new System.Windows.Forms.TextBox();
            this.txtFecha = new System.Windows.Forms.MaskedTextBox();
            this.txtNoAfiliacion = new System.Windows.Forms.MaskedTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pcbLogo = new System.Windows.Forms.PictureBox();
            this.lblTitulo2 = new System.Windows.Forms.Label();
            this.tbcEntregas = new System.Windows.Forms.TabControl();
            this.tbpBuscar = new System.Windows.Forms.TabPage();
            this.lblTotalRegistros = new System.Windows.Forms.Label();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.dgvEntregas = new System.Windows.Forms.DataGridView();
            this.tbpCUD = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbMetodo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTipo = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlTitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).BeginInit();
            this.tbcEntregas.SuspendLayout();
            this.tbpBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEntregas)).BeginInit();
            this.tbpCUD.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.pnlTitulo.Controls.Add(this.pnlMinimize);
            this.pnlTitulo.Controls.Add(this.pnlSalir);
            this.pnlTitulo.Controls.Add(this.label1);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(849, 39);
            this.pnlTitulo.TabIndex = 1;
            this.pnlTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitulo_MouseDown);
            this.pnlTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlTitulo_MouseMove);
            this.pnlTitulo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlTitulo_MouseUp);
            // 
            // pnlMinimize
            // 
            this.pnlMinimize.BackgroundImage = global::Clinica.Properties.Resources.Horizontal_Line_96px;
            this.pnlMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMinimize.Location = new System.Drawing.Point(773, 0);
            this.pnlMinimize.Name = "pnlMinimize";
            this.pnlMinimize.Size = new System.Drawing.Size(38, 39);
            this.pnlMinimize.TabIndex = 2;
            this.tltAyuda.SetToolTip(this.pnlMinimize, "Minimizar");
            this.pnlMinimize.Click += new System.EventHandler(this.pnlMinimize_Click);
            this.pnlMinimize.MouseLeave += new System.EventHandler(this.pnlMinimize_MouseLeave);
            this.pnlMinimize.MouseHover += new System.EventHandler(this.pnlMinimize_MouseHover);
            // 
            // pnlSalir
            // 
            this.pnlSalir.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.pnlSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSalir.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSalir.Location = new System.Drawing.Point(811, 0);
            this.pnlSalir.Name = "pnlSalir";
            this.pnlSalir.Size = new System.Drawing.Size(38, 39);
            this.pnlSalir.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlSalir, "Cerrar");
            this.pnlSalir.Click += new System.EventHandler(this.pnlSalir_Click);
            this.pnlSalir.MouseLeave += new System.EventHandler(this.pnlSalir_MouseLeave);
            this.pnlSalir.MouseHover += new System.EventHandler(this.pnlSalir_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semilight", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(296, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "REGISTRO DIARIO DE CONSULTAS";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // tltAyuda
            // 
            this.tltAyuda.AutomaticDelay = 400;
            this.tltAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.tltAyuda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tltAyuda.OwnerDraw = true;
            this.tltAyuda.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.tltAyuda_Draw);
            // 
            // pnlAdd
            // 
            this.pnlAdd.BackgroundImage = global::Clinica.Properties.Resources.Add_File1;
            this.pnlAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAdd.Location = new System.Drawing.Point(8, 8);
            this.pnlAdd.Name = "pnlAdd";
            this.pnlAdd.Size = new System.Drawing.Size(38, 39);
            this.pnlAdd.TabIndex = 0;
            this.tltAyuda.SetToolTip(this.pnlAdd, "Nueva entrega");
            this.pnlAdd.Click += new System.EventHandler(this.pnlAdd_Click);
            this.pnlAdd.MouseLeave += new System.EventHandler(this.pnlAdd_MouseLeave);
            this.pnlAdd.MouseHover += new System.EventHandler(this.pnlAdd_MouseHover);
            // 
            // pnlCancel
            // 
            this.pnlCancel.BackgroundImage = global::Clinica.Properties.Resources.Cancel;
            this.pnlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCancel.Location = new System.Drawing.Point(184, 8);
            this.pnlCancel.Name = "pnlCancel";
            this.pnlCancel.Size = new System.Drawing.Size(38, 39);
            this.pnlCancel.TabIndex = 2;
            this.tltAyuda.SetToolTip(this.pnlCancel, "Cancelar");
            this.pnlCancel.Click += new System.EventHandler(this.pnlCancel_Click);
            this.pnlCancel.MouseLeave += new System.EventHandler(this.pnlCancel_MouseLeave);
            this.pnlCancel.MouseHover += new System.EventHandler(this.pnlCancel_MouseHover);
            // 
            // pnlDelete
            // 
            this.pnlDelete.BackgroundImage = global::Clinica.Properties.Resources.Delete1;
            this.pnlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelete.Location = new System.Drawing.Point(96, 8);
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.Size = new System.Drawing.Size(38, 39);
            this.pnlDelete.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlDelete, "Eliminar entrega");
            this.pnlDelete.Click += new System.EventHandler(this.pnlDelete_Click);
            this.pnlDelete.MouseLeave += new System.EventHandler(this.pnlDelete_MouseLeave);
            this.pnlDelete.MouseHover += new System.EventHandler(this.pnlDelete_MouseHover);
            // 
            // pnlSave
            // 
            this.pnlSave.BackgroundImage = global::Clinica.Properties.Resources.Save;
            this.pnlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSave.Location = new System.Drawing.Point(140, 8);
            this.pnlSave.Name = "pnlSave";
            this.pnlSave.Size = new System.Drawing.Size(38, 39);
            this.pnlSave.TabIndex = 13;
            this.tltAyuda.SetToolTip(this.pnlSave, "Guardar");
            this.pnlSave.Click += new System.EventHandler(this.pnlSave_Click);
            this.pnlSave.MouseLeave += new System.EventHandler(this.pnlSave_MouseLeave);
            this.pnlSave.MouseHover += new System.EventHandler(this.pnlSave_MouseHover);
            // 
            // pnlEdit
            // 
            this.pnlEdit.BackgroundImage = global::Clinica.Properties.Resources.Edit_File;
            this.pnlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlEdit.Location = new System.Drawing.Point(52, 8);
            this.pnlEdit.Name = "pnlEdit";
            this.pnlEdit.Size = new System.Drawing.Size(38, 39);
            this.pnlEdit.TabIndex = 11;
            this.tltAyuda.SetToolTip(this.pnlEdit, "Editar entrega");
            this.pnlEdit.Click += new System.EventHandler(this.pnlEdit_Click);
            this.pnlEdit.MouseLeave += new System.EventHandler(this.pnlEdit_MouseLeave);
            this.pnlEdit.MouseHover += new System.EventHandler(this.pnlEdit_MouseHover);
            // 
            // txtBuscar
            // 
            this.txtBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscar.ForeColor = System.Drawing.Color.LightGray;
            this.txtBuscar.Location = new System.Drawing.Point(37, 43);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(338, 22);
            this.txtBuscar.TabIndex = 1;
            this.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtBuscar, "Ingrese un numero de afiliación para buscar");
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // rdbSubsecuente
            // 
            this.rdbSubsecuente.AutoSize = true;
            this.rdbSubsecuente.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.rdbSubsecuente.Location = new System.Drawing.Point(139, 32);
            this.rdbSubsecuente.Name = "rdbSubsecuente";
            this.rdbSubsecuente.Size = new System.Drawing.Size(114, 25);
            this.rdbSubsecuente.TabIndex = 1;
            this.rdbSubsecuente.Text = "Subsecuente";
            this.tltAyuda.SetToolTip(this.rdbSubsecuente, "Consulta subsecuente");
            this.rdbSubsecuente.UseVisualStyleBackColor = false;
            this.rdbSubsecuente.CheckedChanged += new System.EventHandler(this.rdbSubsecuente_CheckedChanged);
            // 
            // rdbInscripcion
            // 
            this.rdbInscripcion.AutoSize = true;
            this.rdbInscripcion.Checked = true;
            this.rdbInscripcion.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.rdbInscripcion.Location = new System.Drawing.Point(23, 32);
            this.rdbInscripcion.Name = "rdbInscripcion";
            this.rdbInscripcion.Size = new System.Drawing.Size(99, 25);
            this.rdbInscripcion.TabIndex = 0;
            this.rdbInscripcion.TabStop = true;
            this.rdbInscripcion.Text = "Inscripción";
            this.tltAyuda.SetToolTip(this.rdbInscripcion, "Consulta de inscripción");
            this.rdbInscripcion.UseVisualStyleBackColor = false;
            this.rdbInscripcion.CheckedChanged += new System.EventHandler(this.rdbInscripcion_CheckedChanged);
            // 
            // txtNombres
            // 
            this.txtNombres.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNombres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombres.Enabled = false;
            this.txtNombres.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombres.ForeColor = System.Drawing.Color.LightGray;
            this.txtNombres.Location = new System.Drawing.Point(315, 87);
            this.txtNombres.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(213, 22);
            this.txtNombres.TabIndex = 3;
            this.txtNombres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtNombres, "Nombres de paciente");
            // 
            // txtApellidos
            // 
            this.txtApellidos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtApellidos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApellidos.Enabled = false;
            this.txtApellidos.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidos.ForeColor = System.Drawing.Color.LightGray;
            this.txtApellidos.Location = new System.Drawing.Point(556, 87);
            this.txtApellidos.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(213, 22);
            this.txtApellidos.TabIndex = 22;
            this.txtApellidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtApellidos, "Apellidos de paciente");
            // 
            // txtResp
            // 
            this.txtResp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtResp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtResp.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResp.ForeColor = System.Drawing.Color.LightGray;
            this.txtResp.Location = new System.Drawing.Point(38, 240);
            this.txtResp.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtResp.Name = "txtResp";
            this.txtResp.Size = new System.Drawing.Size(348, 22);
            this.txtResp.TabIndex = 30;
            this.txtResp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtResp, "Nombres de paciente");
            // 
            // txtFecha
            // 
            this.txtFecha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtFecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFecha.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtFecha.Location = new System.Drawing.Point(38, 87);
            this.txtFecha.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtFecha.Mask = "00/00/0000";
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.PromptChar = '.';
            this.txtFecha.Size = new System.Drawing.Size(131, 22);
            this.txtFecha.TabIndex = 18;
            this.txtFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtFecha, "Fecha de entrega");
            this.txtFecha.ValidatingType = typeof(System.DateTime);
            // 
            // txtNoAfiliacion
            // 
            this.txtNoAfiliacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoAfiliacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNoAfiliacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNoAfiliacion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoAfiliacion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNoAfiliacion.Location = new System.Drawing.Point(201, 87);
            this.txtNoAfiliacion.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNoAfiliacion.Mask = "000000000";
            this.txtNoAfiliacion.Name = "txtNoAfiliacion";
            this.txtNoAfiliacion.PromptChar = '.';
            this.txtNoAfiliacion.Size = new System.Drawing.Size(86, 22);
            this.txtNoAfiliacion.TabIndex = 20;
            this.txtNoAfiliacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtNoAfiliacion, "Número de afiliación de paciente");
            this.txtNoAfiliacion.TextChanged += new System.EventHandler(this.txtNoAfiliacion_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pnlAdd);
            this.panel1.Controls.Add(this.pnlCancel);
            this.panel1.Controls.Add(this.pnlDelete);
            this.panel1.Controls.Add(this.pnlSave);
            this.panel1.Controls.Add(this.pnlEdit);
            this.panel1.Location = new System.Drawing.Point(601, 71);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(230, 56);
            this.panel1.TabIndex = 20;
            // 
            // pcbLogo
            // 
            this.pcbLogo.Image = global::Clinica.Properties.Resources.Hospital;
            this.pcbLogo.Location = new System.Drawing.Point(16, 54);
            this.pcbLogo.Name = "pcbLogo";
            this.pcbLogo.Size = new System.Drawing.Size(87, 83);
            this.pcbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbLogo.TabIndex = 22;
            this.pcbLogo.TabStop = false;
            // 
            // lblTitulo2
            // 
            this.lblTitulo2.AutoSize = true;
            this.lblTitulo2.Font = new System.Drawing.Font("Segoe UI Light", 20F);
            this.lblTitulo2.Location = new System.Drawing.Point(109, 58);
            this.lblTitulo2.Name = "lblTitulo2";
            this.lblTitulo2.Size = new System.Drawing.Size(395, 74);
            this.lblTitulo2.TabIndex = 19;
            this.lblTitulo2.Text = "Listado de consultas (planificación\r\nfamiliar)\r\n";
            // 
            // tbcEntregas
            // 
            this.tbcEntregas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcEntregas.Controls.Add(this.tbpBuscar);
            this.tbcEntregas.Controls.Add(this.tbpCUD);
            this.tbcEntregas.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcEntregas.ItemSize = new System.Drawing.Size(96, 20);
            this.tbcEntregas.Location = new System.Drawing.Point(12, 156);
            this.tbcEntregas.Name = "tbcEntregas";
            this.tbcEntregas.SelectedIndex = 0;
            this.tbcEntregas.Size = new System.Drawing.Size(820, 360);
            this.tbcEntregas.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcEntregas.TabIndex = 21;
            // 
            // tbpBuscar
            // 
            this.tbpBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpBuscar.Controls.Add(this.lblTotalRegistros);
            this.tbpBuscar.Controls.Add(this.txtBuscar);
            this.tbpBuscar.Controls.Add(this.lblBuscar);
            this.tbpBuscar.Controls.Add(this.dgvEntregas);
            this.tbpBuscar.Location = new System.Drawing.Point(4, 24);
            this.tbpBuscar.Name = "tbpBuscar";
            this.tbpBuscar.Padding = new System.Windows.Forms.Padding(3);
            this.tbpBuscar.Size = new System.Drawing.Size(812, 332);
            this.tbpBuscar.TabIndex = 0;
            this.tbpBuscar.Text = "Entregas";
            // 
            // lblTotalRegistros
            // 
            this.lblTotalRegistros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalRegistros.AutoSize = true;
            this.lblTotalRegistros.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalRegistros.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRegistros.ForeColor = System.Drawing.Color.Gray;
            this.lblTotalRegistros.Location = new System.Drawing.Point(647, 62);
            this.lblTotalRegistros.Name = "lblTotalRegistros";
            this.lblTotalRegistros.Size = new System.Drawing.Size(116, 19);
            this.lblTotalRegistros.TabIndex = 2;
            this.lblTotalRegistros.Text = "Total de entregas: ";
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.BackColor = System.Drawing.Color.Transparent;
            this.lblBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.Location = new System.Drawing.Point(33, 19);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(59, 21);
            this.lblBuscar.TabIndex = 0;
            this.lblBuscar.Text = "Buscar:";
            // 
            // dgvEntregas
            // 
            this.dgvEntregas.AllowUserToAddRows = false;
            this.dgvEntregas.AllowUserToDeleteRows = false;
            this.dgvEntregas.AllowUserToResizeColumns = false;
            this.dgvEntregas.AllowUserToResizeRows = false;
            this.dgvEntregas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEntregas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEntregas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvEntregas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEntregas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvEntregas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEntregas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEntregas.ColumnHeadersHeight = 45;
            this.dgvEntregas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEntregas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEntregas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvEntregas.EnableHeadersVisualStyles = false;
            this.dgvEntregas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(50)))), ((int)(((byte)(56)))));
            this.dgvEntregas.Location = new System.Drawing.Point(9, 84);
            this.dgvEntregas.MultiSelect = false;
            this.dgvEntregas.Name = "dgvEntregas";
            this.dgvEntregas.ReadOnly = true;
            this.dgvEntregas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvEntregas.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.LightGray;
            this.dgvEntregas.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvEntregas.RowTemplate.Height = 35;
            this.dgvEntregas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEntregas.Size = new System.Drawing.Size(800, 242);
            this.dgvEntregas.TabIndex = 3;
            this.dgvEntregas.Click += new System.EventHandler(this.dgvEntregas_Click);
            this.dgvEntregas.DoubleClick += new System.EventHandler(this.dgvEntregas_DoubleClick);
            // 
            // tbpCUD
            // 
            this.tbpCUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpCUD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpCUD.Controls.Add(this.txtResp);
            this.tbpCUD.Controls.Add(this.label6);
            this.tbpCUD.Controls.Add(this.cmbMetodo);
            this.tbpCUD.Controls.Add(this.label5);
            this.tbpCUD.Controls.Add(this.cmbTipo);
            this.tbpCUD.Controls.Add(this.label15);
            this.tbpCUD.Controls.Add(this.txtApellidos);
            this.tbpCUD.Controls.Add(this.label3);
            this.tbpCUD.Controls.Add(this.txtFecha);
            this.tbpCUD.Controls.Add(this.label12);
            this.tbpCUD.Controls.Add(this.label4);
            this.tbpCUD.Controls.Add(this.txtNoAfiliacion);
            this.tbpCUD.Controls.Add(this.groupBox2);
            this.tbpCUD.Controls.Add(this.txtNombres);
            this.tbpCUD.Controls.Add(this.label2);
            this.tbpCUD.Location = new System.Drawing.Point(4, 24);
            this.tbpCUD.Name = "tbpCUD";
            this.tbpCUD.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCUD.Size = new System.Drawing.Size(812, 332);
            this.tbpCUD.TabIndex = 1;
            this.tbpCUD.Text = "Gestionar";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(34, 216);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(177, 21);
            this.label6.TabIndex = 29;
            this.label6.Text = "Nombre del responsable:";
            // 
            // cmbMetodo
            // 
            this.cmbMetodo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMetodo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbMetodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMetodo.DropDownWidth = 153;
            this.cmbMetodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbMetodo.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbMetodo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbMetodo.IntegralHeight = false;
            this.cmbMetodo.Items.AddRange(new object[] {
            "--Seleccionar--",
            "Oral alta",
            "Oral baja",
            "DIUS",
            "Inyect. Mensual",
            "Inyect. Trimestral",
            "Esteril",
            "Ninguno"});
            this.cmbMetodo.Location = new System.Drawing.Point(570, 161);
            this.cmbMetodo.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbMetodo.Name = "cmbMetodo";
            this.cmbMetodo.Size = new System.Drawing.Size(200, 28);
            this.cmbMetodo.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(566, 138);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 20);
            this.label5.TabIndex = 27;
            this.label5.Text = "Método:";
            // 
            // cmbTipo
            // 
            this.cmbTipo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTipo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipo.DropDownWidth = 153;
            this.cmbTipo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTipo.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbTipo.IntegralHeight = false;
            this.cmbTipo.Items.AddRange(new object[] {
            "--Seleccionar--",
            "Normal",
            "Por morbilidad",
            "Por falla"});
            this.cmbTipo.Location = new System.Drawing.Point(342, 161);
            this.cmbTipo.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbTipo.Name = "cmbTipo";
            this.cmbTipo.Size = new System.Drawing.Size(200, 28);
            this.cmbTipo.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(338, 138);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 20);
            this.label15.TabIndex = 25;
            this.label15.Text = "Tipo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(552, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 21);
            this.label3.TabIndex = 21;
            this.label3.Text = "Apellidos:";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(38, 64);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 20);
            this.label12.TabIndex = 17;
            this.label12.Text = "Fecha:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(197, 64);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.TabIndex = 19;
            this.label4.Text = "N° Afiliacion:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbSubsecuente);
            this.groupBox2.Controls.Add(this.rdbInscripcion);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Location = new System.Drawing.Point(38, 125);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(274, 76);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo de consulta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(311, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombres:";
            // 
            // FrmPlanificacionC
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(849, 528);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pcbLogo);
            this.Controls.Add(this.lblTitulo2);
            this.Controls.Add(this.tbcEntregas);
            this.Controls.Add(this.pnlTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Silver;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPlanificacionC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Entrega anticonceptivos";
            this.Load += new System.EventHandler(this.FrmPlanificacionA_Load);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).EndInit();
            this.tbcEntregas.ResumeLayout(false);
            this.tbpBuscar.ResumeLayout(false);
            this.tbpBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEntregas)).EndInit();
            this.tbpCUD.ResumeLayout(false);
            this.tbpCUD.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Panel pnlMinimize;
        private System.Windows.Forms.Panel pnlSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ErrorProvider epError;
        private System.Windows.Forms.ToolTip tltAyuda;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlAdd;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.Panel pnlSave;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.PictureBox pcbLogo;
        private System.Windows.Forms.Label lblTitulo2;
        private System.Windows.Forms.TabControl tbcEntregas;
        private System.Windows.Forms.TabPage tbpBuscar;
        private System.Windows.Forms.Label lblTotalRegistros;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.DataGridView dgvEntregas;
        private System.Windows.Forms.TabPage tbpCUD;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbSubsecuente;
        private System.Windows.Forms.RadioButton rdbInscripcion;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txtFecha;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtNoAfiliacion;
        private System.Windows.Forms.ComboBox cmbTipo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbMetodo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtResp;
        private System.Windows.Forms.Label label6;
    }
}