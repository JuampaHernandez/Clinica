﻿namespace Clinica.Vistas
{
    partial class FrmReportePlanC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReportePlanC));
            this.spReporteEntregasPlanCMesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtsEntregasPlanC = new Clinica.DataSets.dtsEntregasPlanC();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.spReporteEntregasPlanCMesTableAdapter = new Clinica.DataSets.dtsEntregasPlanCTableAdapters.spReporteEntregasPlanCMesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.spReporteEntregasPlanCMesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsEntregasPlanC)).BeginInit();
            this.SuspendLayout();
            // 
            // spReporteEntregasPlanCMesBindingSource
            // 
            this.spReporteEntregasPlanCMesBindingSource.DataMember = "spReporteEntregasPlanCMes";
            this.spReporteEntregasPlanCMesBindingSource.DataSource = this.dtsEntregasPlanC;
            // 
            // dtsEntregasPlanC
            // 
            this.dtsEntregasPlanC.DataSetName = "dtsEntregasPlanC";
            this.dtsEntregasPlanC.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportViewer1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            reportDataSource1.Name = "dtsEntregasPlanC";
            reportDataSource1.Value = this.spReporteEntregasPlanCMesBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Clinica.Reportes.rptEntregasPlanCMes.rdlc";
            this.reportViewer1.LocalReport.ReportPath = "../../Reportes/rptEntregasPlanCMes.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ShowBackButton = false;
            this.reportViewer1.ShowContextMenu = false;
            this.reportViewer1.ShowCredentialPrompts = false;
            this.reportViewer1.ShowDocumentMapButton = false;
            this.reportViewer1.ShowFindControls = false;
            this.reportViewer1.ShowPageNavigationControls = false;
            this.reportViewer1.ShowParameterPrompts = false;
            this.reportViewer1.ShowPromptAreaButton = false;
            this.reportViewer1.ShowStopButton = false;
            this.reportViewer1.Size = new System.Drawing.Size(932, 549);
            this.reportViewer1.TabIndex = 1;
            // 
            // spReporteEntregasPlanCMesTableAdapter
            // 
            this.spReporteEntregasPlanCMesTableAdapter.ClearBeforeFill = true;
            // 
            // FrmReportePlanC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 549);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmReportePlanC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reporte registro diario - Planificación Familiar";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmReportePlanC_Load);
            ((System.ComponentModel.ISupportInitialize)(this.spReporteEntregasPlanCMesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsEntregasPlanC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private DataSets.dtsEntregasPlanC dtsEntregasPlanC;
        private System.Windows.Forms.BindingSource spReporteEntregasPlanCMesBindingSource;
        private DataSets.dtsEntregasPlanCTableAdapters.spReporteEntregasPlanCMesTableAdapter spReporteEntregasPlanCMesTableAdapter;
    }
}