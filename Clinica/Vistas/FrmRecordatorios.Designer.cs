﻿namespace Clinica.Vistas
{
    partial class FrmRecordatorios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRecordatorios));
            this.lblTitulo2 = new System.Windows.Forms.Label();
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.pnlMinimize = new System.Windows.Forms.Panel();
            this.pnlSalir = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.tbcRecordatorios = new System.Windows.Forms.TabControl();
            this.tbpBuscar = new System.Windows.Forms.TabPage();
            this.lblTotalRegistros = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.dgvRecordatorios = new System.Windows.Forms.DataGridView();
            this.tbpCUD = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbRetrasado = new System.Windows.Forms.RadioButton();
            this.rdbPendiente = new System.Windows.Forms.RadioButton();
            this.rdbRealizado = new System.Windows.Forms.RadioButton();
            this.pnlEstado = new System.Windows.Forms.Panel();
            this.txtFecha = new System.Windows.Forms.MaskedTextBox();
            this.lblFeha = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.lblDescripcion = new System.Windows.Forms.Label();
            this.txtTituloRecordatorio = new System.Windows.Forms.TextBox();
            this.lblTituloRecordatorio = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlAdd = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlCancel = new System.Windows.Forms.Panel();
            this.pnlDelete = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlSave = new System.Windows.Forms.Panel();
            this.pnlEdit = new System.Windows.Forms.Panel();
            this.pcbLogo = new System.Windows.Forms.PictureBox();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.tltAyuda = new System.Windows.Forms.ToolTip(this.components);
            this.pnlTitulo.SuspendLayout();
            this.tbcRecordatorios.SuspendLayout();
            this.tbpBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecordatorios)).BeginInit();
            this.tbpCUD.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlAdd.SuspendLayout();
            this.pnlDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo2
            // 
            this.lblTitulo2.AutoSize = true;
            this.lblTitulo2.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo2.Location = new System.Drawing.Point(115, 63);
            this.lblTitulo2.Name = "lblTitulo2";
            this.lblTitulo2.Size = new System.Drawing.Size(372, 64);
            this.lblTitulo2.TabIndex = 1;
            this.lblTitulo2.Text = "Listado de recordatorios pendientes\r\ny realizados";
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.pnlTitulo.Controls.Add(this.pnlMinimize);
            this.pnlTitulo.Controls.Add(this.pnlSalir);
            this.pnlTitulo.Controls.Add(this.lblTitulo);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(849, 39);
            this.pnlTitulo.TabIndex = 0;
            this.pnlTitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlTitulo_MouseDown);
            this.pnlTitulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlTitulo_MouseMove);
            this.pnlTitulo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlTitulo_MouseUp);
            // 
            // pnlMinimize
            // 
            this.pnlMinimize.BackgroundImage = global::Clinica.Properties.Resources.Horizontal_Line_96px;
            this.pnlMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMinimize.Location = new System.Drawing.Point(773, 0);
            this.pnlMinimize.Name = "pnlMinimize";
            this.pnlMinimize.Size = new System.Drawing.Size(38, 39);
            this.pnlMinimize.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlMinimize, "Minimizar");
            this.pnlMinimize.Click += new System.EventHandler(this.pnlMinimize_Click);
            this.pnlMinimize.MouseLeave += new System.EventHandler(this.pnlMinimize_MouseLeave);
            this.pnlMinimize.MouseHover += new System.EventHandler(this.pnlMinimize_MouseHover);
            // 
            // pnlSalir
            // 
            this.pnlSalir.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.pnlSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSalir.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSalir.Location = new System.Drawing.Point(811, 0);
            this.pnlSalir.Name = "pnlSalir";
            this.pnlSalir.Size = new System.Drawing.Size(38, 39);
            this.pnlSalir.TabIndex = 2;
            this.tltAyuda.SetToolTip(this.pnlSalir, "Cerrar");
            this.pnlSalir.Click += new System.EventHandler(this.pnlSalir_Click);
            this.pnlSalir.MouseLeave += new System.EventHandler(this.pnlSalir_MouseLeave);
            this.pnlSalir.MouseHover += new System.EventHandler(this.pnlSalir_MouseHover);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semilight", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.White;
            this.lblTitulo.Location = new System.Drawing.Point(12, 7);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(157, 25);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "RECORDATORIOS";
            // 
            // tbcRecordatorios
            // 
            this.tbcRecordatorios.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcRecordatorios.Controls.Add(this.tbpBuscar);
            this.tbcRecordatorios.Controls.Add(this.tbpCUD);
            this.tbcRecordatorios.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcRecordatorios.ItemSize = new System.Drawing.Size(96, 20);
            this.tbcRecordatorios.Location = new System.Drawing.Point(12, 149);
            this.tbcRecordatorios.Name = "tbcRecordatorios";
            this.tbcRecordatorios.SelectedIndex = 0;
            this.tbcRecordatorios.Size = new System.Drawing.Size(829, 366);
            this.tbcRecordatorios.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcRecordatorios.TabIndex = 3;
            // 
            // tbpBuscar
            // 
            this.tbpBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpBuscar.Controls.Add(this.lblTotalRegistros);
            this.tbpBuscar.Controls.Add(this.txtBuscar);
            this.tbpBuscar.Controls.Add(this.lblBuscar);
            this.tbpBuscar.Controls.Add(this.dgvRecordatorios);
            this.tbpBuscar.Location = new System.Drawing.Point(4, 24);
            this.tbpBuscar.Name = "tbpBuscar";
            this.tbpBuscar.Padding = new System.Windows.Forms.Padding(3);
            this.tbpBuscar.Size = new System.Drawing.Size(821, 338);
            this.tbpBuscar.TabIndex = 0;
            this.tbpBuscar.Text = "Recordatorios";
            // 
            // lblTotalRegistros
            // 
            this.lblTotalRegistros.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalRegistros.AutoSize = true;
            this.lblTotalRegistros.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalRegistros.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRegistros.ForeColor = System.Drawing.Color.Gray;
            this.lblTotalRegistros.Location = new System.Drawing.Point(639, 68);
            this.lblTotalRegistros.Name = "lblTotalRegistros";
            this.lblTotalRegistros.Size = new System.Drawing.Size(145, 19);
            this.lblTotalRegistros.TabIndex = 2;
            this.lblTotalRegistros.Text = "Total de recordatorios: ";
            // 
            // txtBuscar
            // 
            this.txtBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscar.ForeColor = System.Drawing.Color.LightGray;
            this.txtBuscar.Location = new System.Drawing.Point(37, 43);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(338, 22);
            this.txtBuscar.TabIndex = 1;
            this.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtBuscar, "Escriba el Tíitulo o la Fecha de un recordatorio");
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.BackColor = System.Drawing.Color.Transparent;
            this.lblBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.Location = new System.Drawing.Point(33, 19);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(59, 21);
            this.lblBuscar.TabIndex = 0;
            this.lblBuscar.Text = "Buscar:";
            // 
            // dgvRecordatorios
            // 
            this.dgvRecordatorios.AllowUserToAddRows = false;
            this.dgvRecordatorios.AllowUserToDeleteRows = false;
            this.dgvRecordatorios.AllowUserToResizeColumns = false;
            this.dgvRecordatorios.AllowUserToResizeRows = false;
            this.dgvRecordatorios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRecordatorios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRecordatorios.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvRecordatorios.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvRecordatorios.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvRecordatorios.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvRecordatorios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvRecordatorios.ColumnHeadersHeight = 45;
            this.dgvRecordatorios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvRecordatorios.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvRecordatorios.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvRecordatorios.EnableHeadersVisualStyles = false;
            this.dgvRecordatorios.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvRecordatorios.Location = new System.Drawing.Point(9, 90);
            this.dgvRecordatorios.MultiSelect = false;
            this.dgvRecordatorios.Name = "dgvRecordatorios";
            this.dgvRecordatorios.ReadOnly = true;
            this.dgvRecordatorios.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvRecordatorios.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.LightGray;
            this.dgvRecordatorios.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvRecordatorios.RowTemplate.Height = 40;
            this.dgvRecordatorios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRecordatorios.Size = new System.Drawing.Size(809, 242);
            this.dgvRecordatorios.TabIndex = 3;
            this.dgvRecordatorios.Click += new System.EventHandler(this.dgvRecordatorios_Click);
            this.dgvRecordatorios.DoubleClick += new System.EventHandler(this.dgvRecordatorios_DoubleClick);
            // 
            // tbpCUD
            // 
            this.tbpCUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpCUD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpCUD.Controls.Add(this.groupBox1);
            this.tbpCUD.Controls.Add(this.pnlEstado);
            this.tbpCUD.Controls.Add(this.txtFecha);
            this.tbpCUD.Controls.Add(this.lblFeha);
            this.tbpCUD.Controls.Add(this.txtDescripcion);
            this.tbpCUD.Controls.Add(this.lblDescripcion);
            this.tbpCUD.Controls.Add(this.txtTituloRecordatorio);
            this.tbpCUD.Controls.Add(this.lblTituloRecordatorio);
            this.tbpCUD.Location = new System.Drawing.Point(4, 24);
            this.tbpCUD.Name = "tbpCUD";
            this.tbpCUD.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCUD.Size = new System.Drawing.Size(821, 338);
            this.tbpCUD.TabIndex = 1;
            this.tbpCUD.Text = "Gestionar";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbRetrasado);
            this.groupBox1.Controls.Add(this.rdbPendiente);
            this.groupBox1.Controls.Add(this.rdbRealizado);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Location = new System.Drawing.Point(537, 109);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(173, 137);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Estado";
            // 
            // rdbRetrasado
            // 
            this.rdbRetrasado.AutoSize = true;
            this.rdbRetrasado.Location = new System.Drawing.Point(38, 92);
            this.rdbRetrasado.Name = "rdbRetrasado";
            this.rdbRetrasado.Size = new System.Drawing.Size(95, 25);
            this.rdbRetrasado.TabIndex = 2;
            this.rdbRetrasado.Text = "Retrasado";
            this.rdbRetrasado.UseVisualStyleBackColor = true;
            this.rdbRetrasado.CheckedChanged += new System.EventHandler(this.rdbRetrasado_CheckedChanged);
            // 
            // rdbPendiente
            // 
            this.rdbPendiente.AutoSize = true;
            this.rdbPendiente.Checked = true;
            this.rdbPendiente.Location = new System.Drawing.Point(38, 61);
            this.rdbPendiente.Name = "rdbPendiente";
            this.rdbPendiente.Size = new System.Drawing.Size(95, 25);
            this.rdbPendiente.TabIndex = 1;
            this.rdbPendiente.TabStop = true;
            this.rdbPendiente.Text = "Pendiente";
            this.rdbPendiente.UseVisualStyleBackColor = true;
            this.rdbPendiente.CheckedChanged += new System.EventHandler(this.rdbPendiente_CheckedChanged);
            // 
            // rdbRealizado
            // 
            this.rdbRealizado.AutoSize = true;
            this.rdbRealizado.Location = new System.Drawing.Point(40, 30);
            this.rdbRealizado.Name = "rdbRealizado";
            this.rdbRealizado.Size = new System.Drawing.Size(92, 25);
            this.rdbRealizado.TabIndex = 0;
            this.rdbRealizado.Text = "Realizado";
            this.rdbRealizado.UseVisualStyleBackColor = true;
            this.rdbRealizado.CheckedChanged += new System.EventHandler(this.rdbRealizado_CheckedChanged);
            // 
            // pnlEstado
            // 
            this.pnlEstado.BackColor = System.Drawing.Color.Goldenrod;
            this.pnlEstado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlEstado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEstado.Location = new System.Drawing.Point(537, 252);
            this.pnlEstado.Name = "pnlEstado";
            this.pnlEstado.Size = new System.Drawing.Size(173, 30);
            this.pnlEstado.TabIndex = 7;
            this.tltAyuda.SetToolTip(this.pnlEstado, "Estado de actividad");
            // 
            // txtFecha
            // 
            this.txtFecha.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.txtFecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFecha.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtFecha.Location = new System.Drawing.Point(537, 62);
            this.txtFecha.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtFecha.Mask = "00/00/0000";
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.PromptChar = '.';
            this.txtFecha.Size = new System.Drawing.Size(173, 22);
            this.txtFecha.TabIndex = 3;
            this.txtFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtFecha, "Escriba la fecha de la actividad (dd/mm/aaaa)");
            this.txtFecha.ValidatingType = typeof(System.DateTime);
            // 
            // lblFeha
            // 
            this.lblFeha.AutoSize = true;
            this.lblFeha.BackColor = System.Drawing.Color.Transparent;
            this.lblFeha.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFeha.Location = new System.Drawing.Point(533, 38);
            this.lblFeha.Name = "lblFeha";
            this.lblFeha.Size = new System.Drawing.Size(53, 21);
            this.lblFeha.TabIndex = 2;
            this.lblFeha.Text = "Fecha:";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDescripcion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.ForeColor = System.Drawing.Color.LightGray;
            this.txtDescripcion.Location = new System.Drawing.Point(105, 133);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(404, 149);
            this.txtDescripcion.TabIndex = 5;
            // 
            // lblDescripcion
            // 
            this.lblDescripcion.AutoSize = true;
            this.lblDescripcion.BackColor = System.Drawing.Color.Transparent;
            this.lblDescripcion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescripcion.Location = new System.Drawing.Point(101, 109);
            this.lblDescripcion.Name = "lblDescripcion";
            this.lblDescripcion.Size = new System.Drawing.Size(91, 21);
            this.lblDescripcion.TabIndex = 4;
            this.lblDescripcion.Text = "Descripción:";
            // 
            // txtTituloRecordatorio
            // 
            this.txtTituloRecordatorio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.txtTituloRecordatorio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTituloRecordatorio.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTituloRecordatorio.ForeColor = System.Drawing.Color.LightGray;
            this.txtTituloRecordatorio.Location = new System.Drawing.Point(105, 62);
            this.txtTituloRecordatorio.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtTituloRecordatorio.Name = "txtTituloRecordatorio";
            this.txtTituloRecordatorio.Size = new System.Drawing.Size(404, 22);
            this.txtTituloRecordatorio.TabIndex = 1;
            this.txtTituloRecordatorio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblTituloRecordatorio
            // 
            this.lblTituloRecordatorio.AutoSize = true;
            this.lblTituloRecordatorio.BackColor = System.Drawing.Color.Transparent;
            this.lblTituloRecordatorio.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloRecordatorio.Location = new System.Drawing.Point(101, 38);
            this.lblTituloRecordatorio.Name = "lblTituloRecordatorio";
            this.lblTituloRecordatorio.Size = new System.Drawing.Size(153, 21);
            this.lblTituloRecordatorio.TabIndex = 0;
            this.lblTituloRecordatorio.Text = "Título de recodatorio:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pnlAdd);
            this.panel1.Controls.Add(this.pnlCancel);
            this.panel1.Controls.Add(this.pnlDelete);
            this.panel1.Controls.Add(this.pnlSave);
            this.panel1.Controls.Add(this.pnlEdit);
            this.panel1.Location = new System.Drawing.Point(607, 71);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(230, 56);
            this.panel1.TabIndex = 2;
            // 
            // pnlAdd
            // 
            this.pnlAdd.BackgroundImage = global::Clinica.Properties.Resources.Add_File1;
            this.pnlAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAdd.Controls.Add(this.panel2);
            this.pnlAdd.Location = new System.Drawing.Point(8, 8);
            this.pnlAdd.Name = "pnlAdd";
            this.pnlAdd.Size = new System.Drawing.Size(38, 39);
            this.pnlAdd.TabIndex = 10;
            this.tltAyuda.SetToolTip(this.pnlAdd, "Nuevo recordatorio");
            this.pnlAdd.Click += new System.EventHandler(this.pnlAdd_Click);
            this.pnlAdd.MouseLeave += new System.EventHandler(this.pnlAdd_MouseLeave);
            this.pnlAdd.MouseHover += new System.EventHandler(this.pnlAdd_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(44, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(38, 39);
            this.panel2.TabIndex = 11;
            // 
            // pnlCancel
            // 
            this.pnlCancel.BackgroundImage = global::Clinica.Properties.Resources.Cancel;
            this.pnlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCancel.Location = new System.Drawing.Point(184, 8);
            this.pnlCancel.Name = "pnlCancel";
            this.pnlCancel.Size = new System.Drawing.Size(38, 39);
            this.pnlCancel.TabIndex = 14;
            this.tltAyuda.SetToolTip(this.pnlCancel, "Cancelar");
            this.pnlCancel.Click += new System.EventHandler(this.pnlCancel_Click);
            this.pnlCancel.MouseLeave += new System.EventHandler(this.pnlCancel_MouseLeave);
            this.pnlCancel.MouseHover += new System.EventHandler(this.pnlCancel_MouseHover);
            // 
            // pnlDelete
            // 
            this.pnlDelete.BackgroundImage = global::Clinica.Properties.Resources.Delete1;
            this.pnlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelete.Controls.Add(this.panel6);
            this.pnlDelete.Location = new System.Drawing.Point(96, 8);
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.Size = new System.Drawing.Size(38, 39);
            this.pnlDelete.TabIndex = 12;
            this.tltAyuda.SetToolTip(this.pnlDelete, "Eliminar recordatorio");
            this.pnlDelete.Click += new System.EventHandler(this.pnlDelete_Click);
            this.pnlDelete.MouseLeave += new System.EventHandler(this.pnlDelete_MouseLeave);
            this.pnlDelete.MouseHover += new System.EventHandler(this.pnlDelete_MouseHover);
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(44, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(38, 39);
            this.panel6.TabIndex = 11;
            // 
            // pnlSave
            // 
            this.pnlSave.BackgroundImage = global::Clinica.Properties.Resources.Save;
            this.pnlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSave.Location = new System.Drawing.Point(140, 8);
            this.pnlSave.Name = "pnlSave";
            this.pnlSave.Size = new System.Drawing.Size(38, 39);
            this.pnlSave.TabIndex = 13;
            this.tltAyuda.SetToolTip(this.pnlSave, "Guardar");
            this.pnlSave.Click += new System.EventHandler(this.pnlSave_Click);
            this.pnlSave.MouseLeave += new System.EventHandler(this.pnlSave_MouseLeave);
            this.pnlSave.MouseHover += new System.EventHandler(this.pnlSave_MouseHover);
            // 
            // pnlEdit
            // 
            this.pnlEdit.BackgroundImage = global::Clinica.Properties.Resources.Edit_File;
            this.pnlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlEdit.Location = new System.Drawing.Point(52, 8);
            this.pnlEdit.Name = "pnlEdit";
            this.pnlEdit.Size = new System.Drawing.Size(38, 39);
            this.pnlEdit.TabIndex = 11;
            this.tltAyuda.SetToolTip(this.pnlEdit, "Editar recordatorio");
            this.pnlEdit.Click += new System.EventHandler(this.pnlEdit_Click);
            this.pnlEdit.MouseLeave += new System.EventHandler(this.pnlEdit_MouseLeave);
            this.pnlEdit.MouseHover += new System.EventHandler(this.pnlEdit_MouseHover);
            // 
            // pcbLogo
            // 
            this.pcbLogo.Image = global::Clinica.Properties.Resources.Hospital;
            this.pcbLogo.Location = new System.Drawing.Point(22, 54);
            this.pcbLogo.Name = "pcbLogo";
            this.pcbLogo.Size = new System.Drawing.Size(87, 83);
            this.pcbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbLogo.TabIndex = 14;
            this.pcbLogo.TabStop = false;
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // tltAyuda
            // 
            this.tltAyuda.AutomaticDelay = 400;
            this.tltAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.tltAyuda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tltAyuda.OwnerDraw = true;
            this.tltAyuda.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.tltAyuda_Draw);
            // 
            // FrmRecordatorios
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(849, 528);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbcRecordatorios);
            this.Controls.Add(this.pcbLogo);
            this.Controls.Add(this.lblTitulo2);
            this.Controls.Add(this.pnlTitulo);
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmRecordatorios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmRecordatorios";
            this.Load += new System.EventHandler(this.FrmRecordatorios_Load);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            this.tbcRecordatorios.ResumeLayout(false);
            this.tbpBuscar.ResumeLayout(false);
            this.tbpBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecordatorios)).EndInit();
            this.tbpCUD.ResumeLayout(false);
            this.tbpCUD.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pnlAdd.ResumeLayout(false);
            this.pnlDelete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pcbLogo;
        private System.Windows.Forms.Label lblTitulo2;
        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Panel pnlMinimize;
        private System.Windows.Forms.Panel pnlSalir;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.TabControl tbcRecordatorios;
        private System.Windows.Forms.TabPage tbpBuscar;
        private System.Windows.Forms.Label lblTotalRegistros;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.DataGridView dgvRecordatorios;
        private System.Windows.Forms.TabPage tbpCUD;
        private System.Windows.Forms.TextBox txtTituloRecordatorio;
        private System.Windows.Forms.Label lblTituloRecordatorio;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label lblDescripcion;
        private System.Windows.Forms.Label lblFeha;
        private System.Windows.Forms.MaskedTextBox txtFecha;
        private System.Windows.Forms.Panel pnlEstado;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbPendiente;
        private System.Windows.Forms.RadioButton rdbRealizado;
        private System.Windows.Forms.RadioButton rdbRetrasado;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlAdd;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel pnlSave;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.ErrorProvider epError;
        private System.Windows.Forms.ToolTip tltAyuda;
    }
}