﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmMain : Form
    {
        #region Variales / Instancias
        FrmLogin objLog = new FrmLogin();
        FrmUsuarios frmUsers = new FrmUsuarios();
        FrmConfirmDialog frmConf;
        FrmRecordatorios frmRec;
        Recordatorio objRec = new Recordatorio();
        String userActive;
        Int32 userType;
        int move, x, y, i = 0;
        #endregion

        #region Métodos
        void verify()
        {
            if (this.userType == 2)
            {
                lblExpedientes.Enabled = false;
                lblAnulados.Enabled = false;
                lblConsultas.Enabled = false;
                lblUsuarios.Enabled = false;
                lblEntregas.Enabled = false;
                lblEntegasPlanC.Enabled = false;
            }
        }
        #endregion

        public FrmMain()
        {
            InitializeComponent();
        }

        public FrmMain(String active, Int32 type)
        {
            InitializeComponent();
            this.userActive = active;
            this.userType = type;
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(25, 25, 25), new TransitionType_Linear(2500));
            Transition.run(lblCerrarSesion, "BackColor", Color.FromArgb(25, 25, 25), new TransitionType_Linear(2500));
            lblBienvenida.Text += userActive;
            tmrNotify.Start();
            verify();
        }

        private void lblExpedientes_MouseHover(object sender, EventArgs e)
        {
            Transition.run(lblExpedientes, "ForeColor", Color.SteelBlue, new TransitionType_Linear(10));
        }

        private void lblExpedientes_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(lblExpedientes, "ForeColor", Color.Silver, new TransitionType_Linear(400));
        }

        private void lblAnulados_MouseHover(object sender, EventArgs e)
        {
            Transition.run(lblAnulados, "ForeColor", Color.SteelBlue, new TransitionType_Linear(10));
        }

        private void lblAnulados_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(lblAnulados, "ForeColor", Color.Silver, new TransitionType_Linear(400));
        }

        private void lblExpedientes_Click(object sender, EventArgs e)
        {
            FrmExpediente objExp = new FrmExpediente(userActive, userType);
            objExp.Show();
            this.Close();
        }

        private void lblAnulados_Click(object sender, EventArgs e)
        {
            FrmExpedienteAnulado objAn = new FrmExpedienteAnulado(userActive, userType);
            objAn.Show();
            this.Close();
        }

        private void lblCerrarSesion_Click(object sender, EventArgs e)
        {
            FrmConfirmDialog frmConf = new FrmConfirmDialog("Cerrar sesión", "¿Seguro que desea cerrar la sesión?", "Si", "No", "question");
            if (frmConf.ShowDialog() == DialogResult.OK)
            {
                objLog.Show();
                this.Close();
            }
        }

        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            move = 1;
            x = e.X;
            y = e.Y;
        }

        private void FrmMain_MouseUp(object sender, MouseEventArgs e)
        {
            move = 0;
        }

        private void FrmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == 1)
                SetDesktopLocation(MousePosition.X - x, MousePosition.Y - y);
        }

        private void lblCerrarSesion_MouseHover(object sender, EventArgs e)
        {
            Transition.run(lblCerrarSesion, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(10));
        }

        private void lblCerrarSesion_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(lblCerrarSesion, "BackColor", Color.FromArgb(25, 25, 25), new TransitionType_Linear(400));
        }

        private void lblConsultas_MouseHover(object sender, EventArgs e)
        {
            Transition.run(lblConsultas, "ForeColor", Color.SteelBlue, new TransitionType_Linear(100));
        }

        private void lblConsultas_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(lblConsultas, "ForeColor", Color.Silver, new TransitionType_Linear(400));
        }

        private void lblConsultas_Click(object sender, EventArgs e)
        {
            FrmConsultas objCon = new FrmConsultas(userActive, userType);
            objCon.Show();
            this.Close();
        }

        private void tmrNotify_Tick(object sender, EventArgs e)
        {
            if (i == 50)
            {
                tmrNotify.Stop();
                if (objRec.late(objRec.getFechas()))
                {
                    frmConf = new FrmConfirmDialog("Mensaje", "¡Tiene actividades retrasadas!\nPara verificar las actividades ahora, haga click en el botón \"Ver\" o puede hacerlo luego en la opción Recordatorios.", "Ver", "Ahora no", "infoRec");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        frmRec = new FrmRecordatorios(this.userActive, userType);
                        frmRec.Show();
                        this.Close();
                    }
                }
                else if (objRec.pending(objRec.getFechas()))
                {
                    frmConf = new FrmConfirmDialog("Mensaje", "¡Tiene actividades pendientes!\nPara verificar las actividades ahora, haga click en el botón \"Ver\" o puede hacerlo luego en la opción Recordatorios.", "Ver", "Ahora no", "infoRec");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        frmRec = new FrmRecordatorios(this.userActive, userType);
                        frmRec.Show();
                        this.Close();
                    }
                }
            }
            else
                i++;
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

        private void lblRecordatorios_Click(object sender, EventArgs e)
        {
            frmRec = new FrmRecordatorios(this.userActive, userType);
            frmRec.Show();
            this.Close();
        }

        private void lblUsuarios_Click(object sender, EventArgs e)
        {
            frmUsers = new FrmUsuarios(this.userActive, userType);
            frmUsers.Show();
            this.Close();
        }

        private void lblRepConGeneral_Click(object sender, EventArgs e)
        {
            FrmReporteConsulta frmRepCon = new FrmReporteConsulta();
            frmRepCon.Show();
        }

        private void lblRepConActual_Click(object sender, EventArgs e)
        {
            FrmReporteConsultaMes frmRepConActual = new FrmReporteConsultaMes();
            frmRepConActual.Show();
        }

        private void lblRepConEspecifico_Click(object sender, EventArgs e)
        {
            FrmEstablecerFecha frmEstFecha = new FrmEstablecerFecha("ConsultasMesEsp");
            frmEstFecha.ShowDialog();
        }

        private void lblEntregas_Click(object sender, EventArgs e)
        {
            FrmPlanificacionA frmEntr = new FrmPlanificacionA(userActive, userType);
            frmEntr.Show();
            this.Close();
        }

        private void lblRepEntregasPlanA_Click(object sender, EventArgs e)
        {
            FrmEstablecerFecha frmEstFecha = new FrmEstablecerFecha("PlanA");
            frmEstFecha.ShowDialog();
        }

        private void lblEntegasPlanC_Click(object sender, EventArgs e)
        {
            FrmPlanificacionC frmEntr = new FrmPlanificacionC(userActive, userType);
            frmEntr.Show();
            this.Close();
        }

        private void lblRepEntregasPlanC_Click(object sender, EventArgs e)
        {
            FrmEstablecerFecha frmEstFecha = new FrmEstablecerFecha("PlanC");
            frmEstFecha.ShowDialog();
        }

    }
}
