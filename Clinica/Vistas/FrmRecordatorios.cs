﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmRecordatorios : Form
    {
        #region Variables / Instancias
        Recordatorio objRec = new Recordatorio();
        String userActive;
        Int32 userType;
        Boolean adding, editing;
        int move, x, y;
        #endregion

        #region Metodos del formulario

        void all()
        {
            String sql = "Exec spMostrarRecordatorios";
            dgvRecordatorios.DataSource = objRec.all(sql);
            // Ocultando columna
            dgvRecordatorios.Columns[0].Visible = false;
            lblTotalRegistros.Text = "Total de registros: " + dgvRecordatorios.Rows.Count;
        }

        // Método para buscar recordatorios
        void find(String value)
        {
            String sql = "Exec spBuscarRecordatorios '" + value + "'";
            dgvRecordatorios.DataSource = objRec.all(sql);
            // Ocultando columna ID
            dgvRecordatorios.Columns[0].Visible = false;
            lblTotalRegistros.Text = "Total de registros: " + dgvRecordatorios.Rows.Count;
        }

        void load()
        {
            objRec.id = Convert.ToInt32(dgvRecordatorios.SelectedRows[0].Cells[0].Value.ToString());
            objRec.titulo = dgvRecordatorios.SelectedRows[0].Cells[1].Value.ToString();
            objRec.descripcion = dgvRecordatorios.SelectedRows[0].Cells[2].Value.ToString();
            objRec.fecha = dgvRecordatorios.SelectedRows[0].Cells[3].Value.ToString();
            objRec.estado = dgvRecordatorios.SelectedRows[0].Cells[4].Value.ToString();
        }

        Boolean isValid()
        {
            Boolean estado = true;
            epError.Clear();
            if (String.IsNullOrEmpty(txtTituloRecordatorio.Text))
            {
                epError.SetError(txtTituloRecordatorio, "Ingrese el titulo del recordatorio");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtDescripcion.Text))
            {
                epError.SetError(txtDescripcion, "Ingrese una descripción");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtFecha.Text))
            {
                epError.SetError(txtFecha, "Ingrese la fecha");
                estado = false;
            }
            else
            {
                try
                {
                    String fecha = Convert.ToDateTime(txtFecha.Text).ToString("dd/MM/yyyy");
                    Convert.ToDateTime(fecha);
                }
                catch
                {
                    epError.SetError(txtFecha, "Ingrese una fecha correcta");
                    estado = false;
                }
            }
            return estado;
        }

        // Metodo para cambiar imagen a escala de grises
        void grayScale(Panel pnl, Image img)
        {
            Bitmap bmp = new Bitmap(img);
            // Obteniendo dimension de imagen
            int width = bmp.Width;
            int height = bmp.Height;

            // Color del pixel
            Color p;

            // Escala de grises
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Obteniendo valor del pixel
                    p = bmp.GetPixel(x, y);

                    // Extrayendo el componente ARGB del pixel
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    // Encontrando promedio
                    int avg = (r + g + b) / 3;

                    // Asigando nuevo valor al pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                }
            }

            //load grayscale image in picturebox2
            pnl.BackgroundImage = bmp;
        }

        // Método para habilitar y deshabilitar los botones
        void enableButtons()
        {
            // Verificando si hay o no registros
            if (dgvRecordatorios.RowCount == 0)
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_File1;
                grayScale(pnlEdit, Properties.Resources.Edit_File);
                grayScale(pnlDelete, Properties.Resources.Delete1);
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }
            else
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = true;
                pnlDelete.Enabled = true;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_File1;
                pnlEdit.BackgroundImage = Properties.Resources.Edit_File;
                pnlDelete.BackgroundImage = Properties.Resources.Delete1;
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }

            // Verificando si se está agregando
            if (adding == true || editing == true)
            {
                pnlAdd.Enabled = false;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = true;
                pnlCancel.Enabled = true;
                grayScale(pnlAdd, Properties.Resources.Add_File1);
                grayScale(pnlEdit, Properties.Resources.Edit_File);
                grayScale(pnlDelete, Properties.Resources.Delete1);
                pnlSave.BackgroundImage = Properties.Resources.Save;
                pnlCancel.BackgroundImage = Properties.Resources.Cancel;
            }
        }

        // Método para habilitar y deshabilitar los controles
        void enableControls(Boolean option)
        {
            txtTituloRecordatorio.Enabled = option;
            txtDescripcion.Enabled = option;
            txtFecha.Enabled = option;
            rdbPendiente.Enabled = option;
            rdbRealizado.Enabled = option;
            rdbRetrasado.Enabled = option;
            txtTituloRecordatorio.Focus();
        }

        // Método para resetear controles
        void reset()
        {
            txtTituloRecordatorio.Clear();
            txtDescripcion.Clear();
            txtFecha.Clear();
            rdbPendiente.Checked = true;
            rdbRetrasado.Checked = false;
            rdbRealizado.Checked = false;
            pnlEstado.BackColor = Color.Goldenrod;
        }

        // Método para verificar si existen registros
        Boolean existsRows()
        {
            Boolean estado = false;
            if (dgvRecordatorios.Rows.Count > 0)
                estado = true;
            return estado;
        }

        // Método para leer los atributos de la clase Recordatorios y mostrarlos en controles
        void read()
        {
            txtTituloRecordatorio.Text = objRec.titulo;
            txtDescripcion.Text = objRec.descripcion;
            txtFecha.Text = objRec.fecha;
            if (objRec.estado == "Realizado")
                rdbRealizado.Checked = true;
            else if (objRec.estado == "Pendiente")
                rdbPendiente.Checked = true;
            else if (objRec.estado == "Retrasado")
                rdbRetrasado.Checked = true;
        }

        void send()
        {
            objRec.titulo = txtTituloRecordatorio.Text.Replace("'", "''");
            objRec.descripcion = txtDescripcion.Text.Replace("'", "''");
            objRec.fecha = txtFecha.Text.Replace("'", "''");
            if (rdbRealizado.Checked == true)
                objRec.estado = "Realizado";
            else if (rdbPendiente.Checked == true)
                objRec.estado = "Pendiente";
            else if (rdbRetrasado.Checked == true)
                objRec.estado = "Retrasado";
        }
        #endregion

        #region CRUD
        void add()
        {
            adding = true;
            editing = false;
            reset();
            enableControls(true);
            enableButtons();
            dgvRecordatorios.Enabled = false;
            tbcRecordatorios.SelectedIndex = 1;
        }

        void RadioButtonsCheckedChange()
        {
            if (rdbPendiente.Checked == true)
                pnlEstado.BackColor = Color.Goldenrod;
            else if (rdbRealizado.Checked == true)
                pnlEstado.BackColor = Color.SeaGreen;
            else if (rdbRetrasado.Checked == true)
                pnlEstado.BackColor = Color.Firebrick;
        }

        void edit()
        {
            try
            {
                if (isValid())
                {
                    adding = false;
                    editing = true;
                    enableControls(true);
                    dgvRecordatorios.Enabled = false;
                    tbcRecordatorios.SelectedIndex = 1;
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Selecione un recordatorio para editar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcRecordatorios.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void delete()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea eliminar el recordatorio?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        if (objRec.crud("delete") > 0)
                        {
                            frmConf = new FrmConfirmDialog("Mensaje", "Recordatorio eliminado correctamente.", "Ok", "No", "info");
                            frmConf.ShowDialog();
                        }
                        else
                        {
                            frmConf = new FrmConfirmDialog("Error", "Error al eliminar el recordatorio.", "Ok", "No", "error");
                            frmConf.ShowDialog();
                        }
                        all();
                        reset();
                        tbcRecordatorios.SelectedIndex = 0;
                    }
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione un recordatorio para eliminar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcRecordatorios.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void save()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea guardar el recordatorio?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        send();
                        if (adding == true && editing == false) // Si se está Ingresando Datos
                        {
                            if (objRec.crud("save") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Recordatorio guardado correctamente.", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcRecordatorios.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al guardar el recordatorio.", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        else if (adding == false && editing == true) // Si se está Modificando Datos
                        {
                            if (objRec.crud("update") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Recordatorio editado correctamente", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcRecordatorios.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al editar el recordatorio", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        adding = false;
                        editing = false;
                        reset();
                        enableControls(false);
                        dgvRecordatorios.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void cancel()
        {
            adding = false;
            editing = false;
            reset();
            enableButtons();
            enableControls(false);
            epError.Clear();
            dgvRecordatorios.Enabled = true;
            tbcRecordatorios.SelectedIndex = 0;
        }
        #endregion

        public FrmRecordatorios()
        {
            InitializeComponent();
        }

        public FrmRecordatorios(String active, Int32 type)
        {
            InitializeComponent();
            this.userActive = active;
            this.userType = type;
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            FrmMain objMain = new FrmMain(userActive, userType);
            if (!adding && !editing)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana recordatorios y volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "Todos los datos no guardados se perderan.\n¿Seguro que desea volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pnlMinimize_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(35, 35, 35), new TransitionType_Linear(200));
        }

        private void pnlMinimize_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void FrmRecordatorios_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            all();
            reset();
            enableButtons();
            enableControls(false);
            if (!existsRows())
                dgvRecordatorios.Enabled = false;
        }

        private void pnlAdd_Click(object sender, EventArgs e)
        {
            add();
        }

        private void pnlAdd_MouseHover(object sender, EventArgs e)
        {
            pnlAdd.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlAdd_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlAdd, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlEdit_Click(object sender, EventArgs e)
        {
            edit();
        }

        private void pnlEdit_MouseHover(object sender, EventArgs e)
        {
            pnlEdit.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlEdit_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlEdit, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlDelete_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void pnlDelete_MouseHover(object sender, EventArgs e)
        {
            pnlDelete.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlDelete_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlDelete, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void pnlSave_MouseHover(object sender, EventArgs e)
        {
            pnlSave.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlSave_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSave, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlCancel_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void pnlCancel_MouseHover(object sender, EventArgs e)
        {
            pnlCancel.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlCancel_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlCancel, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void rdbRealizado_CheckedChanged(object sender, EventArgs e)
        {
            RadioButtonsCheckedChange();
        }

        private void rdbPendiente_CheckedChanged(object sender, EventArgs e)
        {
            RadioButtonsCheckedChange();
        }

        private void rdbRetrasado_CheckedChanged(object sender, EventArgs e)
        {
            RadioButtonsCheckedChange();
        }

        private void dgvRecordatorios_Click(object sender, EventArgs e)
        {
            if (existsRows())
            {
                reset();
                load();
                read();
                if (dgvRecordatorios.SelectedRows[0].Cells[4].Value.ToString() == "Realizado")
                    //dgvRecordatorios.RowsDefaultCellStyle.SelectionBackColor = Color.SeaGreen;
                    dgvRecordatorios.SelectedRows[0].DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
                else if (dgvRecordatorios.SelectedRows[0].Cells[4].Value.ToString() == "Pendiente")
                    //dgvRecordatorios.RowsDefaultCellStyle.SelectionBackColor = Color.Goldenrod;
                    dgvRecordatorios.SelectedRows[0].DefaultCellStyle.SelectionBackColor = Color.Goldenrod;
                else if (dgvRecordatorios.SelectedRows[0].Cells[4].Value.ToString() == "Retrasado")
                    //dgvRecordatorios.RowsDefaultCellStyle.SelectionBackColor = Color.Firebrick;
                    dgvRecordatorios.SelectedRows[0].DefaultCellStyle.SelectionBackColor = Color.Firebrick;
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "Debe haber al menos un reccordatorio para continuar.", "Ok", "No", "info");
                frmConf.ShowDialog();
            }
        }

        private void dgvRecordatorios_DoubleClick(object sender, EventArgs e)
        {
            tbcRecordatorios.SelectedIndex = 1;
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            find(txtBuscar.Text.Replace("'", "''"));
            enableButtons();
        }

        private void pnlTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            move = 1;
            x = e.X;
            y = e.Y;
        }

        private void pnlTitulo_MouseUp(object sender, MouseEventArgs e)
        {
            move = 0;
        }

        private void pnlTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == 1)
                SetDesktopLocation(MousePosition.X - x, MousePosition.Y - y);
        }

    }
}
