﻿namespace Clinica.Vistas
{
    partial class FrmExpediente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmExpediente));
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.pnlMinimize = new System.Windows.Forms.Panel();
            this.pnlSalir = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbcPacientes = new System.Windows.Forms.TabControl();
            this.tbpBuscar = new System.Windows.Forms.TabPage();
            this.lblAnulados = new System.Windows.Forms.Label();
            this.lblTotalRegistros = new System.Windows.Forms.Label();
            this.txtFiltrar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.dgvPacientes = new System.Windows.Forms.DataGridView();
            this.tbpCUD = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.txtNombreArch = new System.Windows.Forms.TextBox();
            this.grbFamilia = new System.Windows.Forms.GroupBox();
            this.txtTelEmergencia = new System.Windows.Forms.MaskedTextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtMadre = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtEmergencia = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtProporcionador = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtConyugue = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtPadre = new System.Windows.Forms.TextBox();
            this.grbActividad = new System.Windows.Forms.GroupBox();
            this.ckbAct9 = new System.Windows.Forms.CheckBox();
            this.ckbAct8 = new System.Windows.Forms.CheckBox();
            this.ckbAct7 = new System.Windows.Forms.CheckBox();
            this.ckbAct6 = new System.Windows.Forms.CheckBox();
            this.ckbAct5 = new System.Windows.Forms.CheckBox();
            this.ckbAct4 = new System.Windows.Forms.CheckBox();
            this.ckbAct3 = new System.Windows.Forms.CheckBox();
            this.ckbAct2 = new System.Windows.Forms.CheckBox();
            this.ckbAct1 = new System.Windows.Forms.CheckBox();
            this.txtFecha = new System.Windows.Forms.MaskedTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.grbDatos = new System.Windows.Forms.GroupBox();
            this.txtTelTrabajo = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDirTrabajo = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPatrono = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtOcupacion = new System.Windows.Forms.TextBox();
            this.txtTelDomicilio = new System.Windows.Forms.MaskedTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDirDomicilio = new System.Windows.Forms.TextBox();
            this.txtFechaNac = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtLugarNac = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbEstadoCivil = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbSexo = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbCalidad = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCentroAtencion = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDui = new System.Windows.Forms.MaskedTextBox();
            this.txtApellidos = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNombres = new System.Windows.Forms.TextBox();
            this.txtNoAfiliacion = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlPrint = new System.Windows.Forms.Panel();
            this.pnlAdd = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlCancel = new System.Windows.Forms.Panel();
            this.pnlDelete = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlSave = new System.Windows.Forms.Panel();
            this.pnlEdit = new System.Windows.Forms.Panel();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.tltAyuda = new System.Windows.Forms.ToolTip(this.components);
            this.pcbLogo = new System.Windows.Forms.PictureBox();
            this.pnlTitulo.SuspendLayout();
            this.tbcPacientes.SuspendLayout();
            this.tbpBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacientes)).BeginInit();
            this.tbpCUD.SuspendLayout();
            this.grbFamilia.SuspendLayout();
            this.grbActividad.SuspendLayout();
            this.grbDatos.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlAdd.SuspendLayout();
            this.pnlDelete.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.pnlTitulo.Controls.Add(this.pnlMinimize);
            this.pnlTitulo.Controls.Add(this.pnlSalir);
            this.pnlTitulo.Controls.Add(this.label1);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(1362, 39);
            this.pnlTitulo.TabIndex = 0;
            // 
            // pnlMinimize
            // 
            this.pnlMinimize.BackgroundImage = global::Clinica.Properties.Resources.Horizontal_Line_96px;
            this.pnlMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMinimize.Location = new System.Drawing.Point(1286, 0);
            this.pnlMinimize.Name = "pnlMinimize";
            this.pnlMinimize.Size = new System.Drawing.Size(38, 39);
            this.pnlMinimize.TabIndex = 2;
            this.tltAyuda.SetToolTip(this.pnlMinimize, "Minimizar");
            this.pnlMinimize.Click += new System.EventHandler(this.pnlMinimize_Click);
            this.pnlMinimize.MouseLeave += new System.EventHandler(this.pnlMinimize_MouseLeave);
            this.pnlMinimize.MouseHover += new System.EventHandler(this.pnlMinimize_MouseHover);
            // 
            // pnlSalir
            // 
            this.pnlSalir.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.pnlSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSalir.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSalir.Location = new System.Drawing.Point(1324, 0);
            this.pnlSalir.Name = "pnlSalir";
            this.pnlSalir.Size = new System.Drawing.Size(38, 39);
            this.pnlSalir.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlSalir, "Cerrar");
            this.pnlSalir.Click += new System.EventHandler(this.pnlSalir_Click);
            this.pnlSalir.MouseLeave += new System.EventHandler(this.pnlSalir_MouseLeave);
            this.pnlSalir.MouseHover += new System.EventHandler(this.pnlSalir_MouseHover);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semilight", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "EXPEDIENTES";
            // 
            // tbcPacientes
            // 
            this.tbcPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcPacientes.Controls.Add(this.tbpBuscar);
            this.tbcPacientes.Controls.Add(this.tbpCUD);
            this.tbcPacientes.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcPacientes.Location = new System.Drawing.Point(12, 149);
            this.tbcPacientes.Name = "tbcPacientes";
            this.tbcPacientes.SelectedIndex = 0;
            this.tbcPacientes.Size = new System.Drawing.Size(1342, 571);
            this.tbcPacientes.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcPacientes.TabIndex = 3;
            this.tltAyuda.SetToolTip(this.tbcPacientes, "Gestión de expedientes");
            // 
            // tbpBuscar
            // 
            this.tbpBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpBuscar.Controls.Add(this.lblAnulados);
            this.tbpBuscar.Controls.Add(this.lblTotalRegistros);
            this.tbpBuscar.Controls.Add(this.txtFiltrar);
            this.tbpBuscar.Controls.Add(this.lblBuscar);
            this.tbpBuscar.Controls.Add(this.dgvPacientes);
            this.tbpBuscar.Location = new System.Drawing.Point(4, 26);
            this.tbpBuscar.Name = "tbpBuscar";
            this.tbpBuscar.Padding = new System.Windows.Forms.Padding(3);
            this.tbpBuscar.Size = new System.Drawing.Size(1334, 541);
            this.tbpBuscar.TabIndex = 0;
            this.tbpBuscar.Text = "Pacientes";
            // 
            // lblAnulados
            // 
            this.lblAnulados.AutoSize = true;
            this.lblAnulados.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.lblAnulados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAnulados.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnulados.ForeColor = System.Drawing.Color.Tomato;
            this.lblAnulados.Location = new System.Drawing.Point(1240, 6);
            this.lblAnulados.Margin = new System.Windows.Forms.Padding(3);
            this.lblAnulados.Name = "lblAnulados";
            this.lblAnulados.Padding = new System.Windows.Forms.Padding(2);
            this.lblAnulados.Size = new System.Drawing.Size(88, 23);
            this.lblAnulados.TabIndex = 2;
            this.lblAnulados.Text = "Ver anulados";
            this.tltAyuda.SetToolTip(this.lblAnulados, "Ir a la ventana de expedientes anulados");
            this.lblAnulados.Click += new System.EventHandler(this.lblAnulados_Click);
            // 
            // lblTotalRegistros
            // 
            this.lblTotalRegistros.AutoSize = true;
            this.lblTotalRegistros.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalRegistros.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRegistros.ForeColor = System.Drawing.Color.Gray;
            this.lblTotalRegistros.Location = new System.Drawing.Point(1166, 62);
            this.lblTotalRegistros.Name = "lblTotalRegistros";
            this.lblTotalRegistros.Size = new System.Drawing.Size(116, 19);
            this.lblTotalRegistros.TabIndex = 3;
            this.lblTotalRegistros.Text = "Total de registros: ";
            // 
            // txtFiltrar
            // 
            this.txtFiltrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtFiltrar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFiltrar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFiltrar.ForeColor = System.Drawing.Color.LightGray;
            this.txtFiltrar.Location = new System.Drawing.Point(37, 43);
            this.txtFiltrar.Name = "txtFiltrar";
            this.txtFiltrar.Size = new System.Drawing.Size(338, 22);
            this.txtFiltrar.TabIndex = 1;
            this.txtFiltrar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtFiltrar, "Escriba el DUI del paciente");
            this.txtFiltrar.TextChanged += new System.EventHandler(this.txtFiltrar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.BackColor = System.Drawing.Color.Transparent;
            this.lblBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.Location = new System.Drawing.Point(33, 19);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(59, 21);
            this.lblBuscar.TabIndex = 0;
            this.lblBuscar.Text = "Buscar:";
            // 
            // dgvPacientes
            // 
            this.dgvPacientes.AllowUserToAddRows = false;
            this.dgvPacientes.AllowUserToDeleteRows = false;
            this.dgvPacientes.AllowUserToResizeColumns = false;
            this.dgvPacientes.AllowUserToResizeRows = false;
            this.dgvPacientes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvPacientes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPacientes.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvPacientes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvPacientes.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvPacientes.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPacientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPacientes.ColumnHeadersHeight = 40;
            this.dgvPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(71)))), ((int)(((byte)(79)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPacientes.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPacientes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvPacientes.EnableHeadersVisualStyles = false;
            this.dgvPacientes.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvPacientes.Location = new System.Drawing.Point(6, 84);
            this.dgvPacientes.MultiSelect = false;
            this.dgvPacientes.Name = "dgvPacientes";
            this.dgvPacientes.ReadOnly = true;
            this.dgvPacientes.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPacientes.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.LightGray;
            this.dgvPacientes.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPacientes.RowTemplate.Height = 35;
            this.dgvPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPacientes.Size = new System.Drawing.Size(1322, 451);
            this.dgvPacientes.TabIndex = 4;
            this.dgvPacientes.Click += new System.EventHandler(this.dgvPacientes_Click);
            this.dgvPacientes.DoubleClick += new System.EventHandler(this.dgvPacientes_DoubleClick);
            // 
            // tbpCUD
            // 
            this.tbpCUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpCUD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpCUD.Controls.Add(this.label27);
            this.tbpCUD.Controls.Add(this.txtNombreArch);
            this.tbpCUD.Controls.Add(this.grbFamilia);
            this.tbpCUD.Controls.Add(this.grbActividad);
            this.tbpCUD.Controls.Add(this.txtFecha);
            this.tbpCUD.Controls.Add(this.label8);
            this.tbpCUD.Controls.Add(this.label7);
            this.tbpCUD.Controls.Add(this.grbDatos);
            this.tbpCUD.Controls.Add(this.txtCentroAtencion);
            this.tbpCUD.Controls.Add(this.label3);
            this.tbpCUD.Controls.Add(this.label6);
            this.tbpCUD.Controls.Add(this.txtDui);
            this.tbpCUD.Controls.Add(this.txtApellidos);
            this.tbpCUD.Controls.Add(this.label4);
            this.tbpCUD.Controls.Add(this.label5);
            this.tbpCUD.Controls.Add(this.txtNombres);
            this.tbpCUD.Controls.Add(this.txtNoAfiliacion);
            this.tbpCUD.Location = new System.Drawing.Point(4, 26);
            this.tbpCUD.Name = "tbpCUD";
            this.tbpCUD.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCUD.Size = new System.Drawing.Size(1334, 541);
            this.tbpCUD.TabIndex = 1;
            this.tbpCUD.Text = "Gestionar";
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Silver;
            this.label27.Location = new System.Drawing.Point(49, 475);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(145, 20);
            this.label27.TabIndex = 15;
            this.label27.Text = "Nombre de archivista:";
            // 
            // txtNombreArch
            // 
            this.txtNombreArch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNombreArch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNombreArch.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombreArch.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreArch.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNombreArch.Location = new System.Drawing.Point(53, 498);
            this.txtNombreArch.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNombreArch.Name = "txtNombreArch";
            this.txtNombreArch.Size = new System.Drawing.Size(319, 22);
            this.txtNombreArch.TabIndex = 16;
            this.txtNombreArch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grbFamilia
            // 
            this.grbFamilia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbFamilia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.grbFamilia.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grbFamilia.Controls.Add(this.txtTelEmergencia);
            this.grbFamilia.Controls.Add(this.label26);
            this.grbFamilia.Controls.Add(this.txtMadre);
            this.grbFamilia.Controls.Add(this.label25);
            this.grbFamilia.Controls.Add(this.txtEmergencia);
            this.grbFamilia.Controls.Add(this.label24);
            this.grbFamilia.Controls.Add(this.txtProporcionador);
            this.grbFamilia.Controls.Add(this.label23);
            this.grbFamilia.Controls.Add(this.txtConyugue);
            this.grbFamilia.Controls.Add(this.label22);
            this.grbFamilia.Controls.Add(this.label21);
            this.grbFamilia.Controls.Add(this.txtPadre);
            this.grbFamilia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbFamilia.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbFamilia.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.grbFamilia.Location = new System.Drawing.Point(784, 254);
            this.grbFamilia.Name = "grbFamilia";
            this.grbFamilia.Size = new System.Drawing.Size(544, 281);
            this.grbFamilia.TabIndex = 14;
            this.grbFamilia.TabStop = false;
            this.grbFamilia.Text = "C - De la Familia";
            // 
            // txtTelEmergencia
            // 
            this.txtTelEmergencia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelEmergencia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtTelEmergencia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelEmergencia.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelEmergencia.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtTelEmergencia.Location = new System.Drawing.Point(290, 174);
            this.txtTelEmergencia.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtTelEmergencia.Mask = "####-####";
            this.txtTelEmergencia.Name = "txtTelEmergencia";
            this.txtTelEmergencia.PromptChar = '.';
            this.txtTelEmergencia.Size = new System.Drawing.Size(140, 22);
            this.txtTelEmergencia.TabIndex = 9;
            this.txtTelEmergencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtTelEmergencia, "Teléfono de emergencia");
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(286, 151);
            this.label26.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(65, 20);
            this.label26.TabIndex = 8;
            this.label26.Text = "Teléfono:";
            // 
            // txtMadre
            // 
            this.txtMadre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMadre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtMadre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMadre.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMadre.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtMadre.Location = new System.Drawing.Point(290, 58);
            this.txtMadre.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtMadre.Name = "txtMadre";
            this.txtMadre.Size = new System.Drawing.Size(240, 22);
            this.txtMadre.TabIndex = 3;
            this.txtMadre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(18, 151);
            this.label25.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(210, 20);
            this.label25.TabIndex = 6;
            this.label25.Text = "En caso de emergencia llamar a:";
            // 
            // txtEmergencia
            // 
            this.txtEmergencia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmergencia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtEmergencia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmergencia.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmergencia.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtEmergencia.Location = new System.Drawing.Point(22, 174);
            this.txtEmergencia.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtEmergencia.Name = "txtEmergencia";
            this.txtEmergencia.Size = new System.Drawing.Size(240, 22);
            this.txtEmergencia.TabIndex = 7;
            this.txtEmergencia.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(18, 209);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(146, 20);
            this.label24.TabIndex = 10;
            this.label24.Text = "Proporcionó los datos:";
            // 
            // txtProporcionador
            // 
            this.txtProporcionador.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProporcionador.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtProporcionador.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtProporcionador.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProporcionador.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtProporcionador.Location = new System.Drawing.Point(22, 232);
            this.txtProporcionador.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtProporcionador.Name = "txtProporcionador";
            this.txtProporcionador.Size = new System.Drawing.Size(240, 22);
            this.txtProporcionador.TabIndex = 11;
            this.txtProporcionador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(18, 93);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(153, 20);
            this.label23.TabIndex = 4;
            this.label23.Text = "Nombre del conyugue:";
            // 
            // txtConyugue
            // 
            this.txtConyugue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConyugue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtConyugue.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtConyugue.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConyugue.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtConyugue.Location = new System.Drawing.Point(22, 116);
            this.txtConyugue.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtConyugue.Name = "txtConyugue";
            this.txtConyugue.Size = new System.Drawing.Size(240, 22);
            this.txtConyugue.TabIndex = 5;
            this.txtConyugue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(286, 35);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(142, 20);
            this.label22.TabIndex = 2;
            this.label22.Text = "Nombre de la madre:";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(18, 35);
            this.label21.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(126, 20);
            this.label21.TabIndex = 0;
            this.label21.Text = "Nombre del Padre:";
            // 
            // txtPadre
            // 
            this.txtPadre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPadre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtPadre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPadre.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPadre.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtPadre.Location = new System.Drawing.Point(22, 58);
            this.txtPadre.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtPadre.Name = "txtPadre";
            this.txtPadre.Size = new System.Drawing.Size(240, 22);
            this.txtPadre.TabIndex = 1;
            this.txtPadre.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grbActividad
            // 
            this.grbActividad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.grbActividad.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grbActividad.Controls.Add(this.ckbAct9);
            this.grbActividad.Controls.Add(this.ckbAct8);
            this.grbActividad.Controls.Add(this.ckbAct7);
            this.grbActividad.Controls.Add(this.ckbAct6);
            this.grbActividad.Controls.Add(this.ckbAct5);
            this.grbActividad.Controls.Add(this.ckbAct4);
            this.grbActividad.Controls.Add(this.ckbAct3);
            this.grbActividad.Controls.Add(this.ckbAct2);
            this.grbActividad.Controls.Add(this.ckbAct1);
            this.grbActividad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbActividad.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbActividad.ForeColor = System.Drawing.Color.Silver;
            this.grbActividad.Location = new System.Drawing.Point(6, 254);
            this.grbActividad.Name = "grbActividad";
            this.grbActividad.Size = new System.Drawing.Size(772, 211);
            this.grbActividad.TabIndex = 13;
            this.grbActividad.TabStop = false;
            this.grbActividad.Text = "B - Actividad Económica";
            // 
            // ckbAct9
            // 
            this.ckbAct9.AutoSize = true;
            this.ckbAct9.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct9.Location = new System.Drawing.Point(352, 163);
            this.ckbAct9.Name = "ckbAct9";
            this.ckbAct9.Size = new System.Drawing.Size(337, 28);
            this.ckbAct9.TabIndex = 8;
            this.ckbAct9.Text = "9 - Servicios comunales, sociales y personales";
            this.ckbAct9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct9.UseCompatibleTextRendering = true;
            this.ckbAct9.UseVisualStyleBackColor = true;
            // 
            // ckbAct8
            // 
            this.ckbAct8.AutoSize = true;
            this.ckbAct8.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct8.Location = new System.Drawing.Point(354, 101);
            this.ckbAct8.Name = "ckbAct8";
            this.ckbAct8.Size = new System.Drawing.Size(373, 49);
            this.ckbAct8.TabIndex = 7;
            this.ckbAct8.Text = "8 - Establecimientos financieros, seguros, \r\nbienes inmuebles y servicios prestad" +
    "os a empresas";
            this.ckbAct8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct8.UseCompatibleTextRendering = true;
            this.ckbAct8.UseVisualStyleBackColor = true;
            // 
            // ckbAct7
            // 
            this.ckbAct7.AutoSize = true;
            this.ckbAct7.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct7.Location = new System.Drawing.Point(354, 70);
            this.ckbAct7.Name = "ckbAct7";
            this.ckbAct7.Size = new System.Drawing.Size(383, 28);
            this.ckbAct7.TabIndex = 6;
            this.ckbAct7.Text = "7 - Transportes, Almacenamiento y Comunicaciónes";
            this.ckbAct7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct7.UseCompatibleTextRendering = true;
            this.ckbAct7.UseVisualStyleBackColor = true;
            // 
            // ckbAct6
            // 
            this.ckbAct6.AutoSize = true;
            this.ckbAct6.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct6.Location = new System.Drawing.Point(354, 39);
            this.ckbAct6.Name = "ckbAct6";
            this.ckbAct6.Size = new System.Drawing.Size(395, 28);
            this.ckbAct6.TabIndex = 5;
            this.ckbAct6.Text = "6 - Comercio por mayor y menor, Restaurantes, Hotel";
            this.ckbAct6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct6.UseCompatibleTextRendering = true;
            this.ckbAct6.UseVisualStyleBackColor = true;
            // 
            // ckbAct5
            // 
            this.ckbAct5.AutoSize = true;
            this.ckbAct5.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct5.Location = new System.Drawing.Point(32, 163);
            this.ckbAct5.Name = "ckbAct5";
            this.ckbAct5.Size = new System.Drawing.Size(139, 28);
            this.ckbAct5.TabIndex = 4;
            this.ckbAct5.Text = "5 - Construcción";
            this.ckbAct5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct5.UseCompatibleTextRendering = true;
            this.ckbAct5.UseVisualStyleBackColor = true;
            // 
            // ckbAct4
            // 
            this.ckbAct4.AutoSize = true;
            this.ckbAct4.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct4.Location = new System.Drawing.Point(34, 132);
            this.ckbAct4.Name = "ckbAct4";
            this.ckbAct4.Size = new System.Drawing.Size(215, 28);
            this.ckbAct4.TabIndex = 3;
            this.ckbAct4.Text = "4 - Electricidad, Gaz y Agua";
            this.ckbAct4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct4.UseCompatibleTextRendering = true;
            this.ckbAct4.UseVisualStyleBackColor = true;
            // 
            // ckbAct3
            // 
            this.ckbAct3.AutoSize = true;
            this.ckbAct3.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct3.Location = new System.Drawing.Point(34, 101);
            this.ckbAct3.Name = "ckbAct3";
            this.ckbAct3.Size = new System.Drawing.Size(226, 28);
            this.ckbAct3.TabIndex = 2;
            this.ckbAct3.Text = "3 - Industrias Manufactureras";
            this.ckbAct3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct3.UseCompatibleTextRendering = true;
            this.ckbAct3.UseVisualStyleBackColor = true;
            // 
            // ckbAct2
            // 
            this.ckbAct2.AutoSize = true;
            this.ckbAct2.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct2.ForeColor = System.Drawing.Color.Silver;
            this.ckbAct2.Location = new System.Drawing.Point(34, 70);
            this.ckbAct2.Name = "ckbAct2";
            this.ckbAct2.Size = new System.Drawing.Size(268, 28);
            this.ckbAct2.TabIndex = 1;
            this.ckbAct2.Text = "2 - Explotacón de Minas y Canteras";
            this.ckbAct2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct2.UseCompatibleTextRendering = true;
            this.ckbAct2.UseVisualStyleBackColor = true;
            // 
            // ckbAct1
            // 
            this.ckbAct1.AutoSize = true;
            this.ckbAct1.FlatAppearance.BorderColor = System.Drawing.Color.WhiteSmoke;
            this.ckbAct1.Location = new System.Drawing.Point(34, 39);
            this.ckbAct1.Name = "ckbAct1";
            this.ckbAct1.Size = new System.Drawing.Size(302, 28);
            this.ckbAct1.TabIndex = 0;
            this.ckbAct1.Text = "1 - Agricultura, Caza, Silvicultura y Pezca";
            this.ckbAct1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckbAct1.UseCompatibleTextRendering = true;
            this.ckbAct1.UseVisualStyleBackColor = true;
            // 
            // txtFecha
            // 
            this.txtFecha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtFecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFecha.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtFecha.Location = new System.Drawing.Point(1188, 41);
            this.txtFecha.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtFecha.Mask = "00/00/0000";
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.PromptChar = '.';
            this.txtFecha.Size = new System.Drawing.Size(93, 22);
            this.txtFecha.TabIndex = 11;
            this.txtFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtFecha, "Fecha de expedición (dd/mm/aaaa)");
            this.txtFecha.ValidatingType = typeof(System.DateTime);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Silver;
            this.label8.Location = new System.Drawing.Point(1184, 18);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "Fecha:";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Silver;
            this.label7.Location = new System.Drawing.Point(830, 18);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "Centro de atención:";
            // 
            // grbDatos
            // 
            this.grbDatos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbDatos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.grbDatos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.grbDatos.Controls.Add(this.txtTelTrabajo);
            this.grbDatos.Controls.Add(this.label20);
            this.grbDatos.Controls.Add(this.label19);
            this.grbDatos.Controls.Add(this.txtDirTrabajo);
            this.grbDatos.Controls.Add(this.label18);
            this.grbDatos.Controls.Add(this.txtPatrono);
            this.grbDatos.Controls.Add(this.label17);
            this.grbDatos.Controls.Add(this.txtOcupacion);
            this.grbDatos.Controls.Add(this.txtTelDomicilio);
            this.grbDatos.Controls.Add(this.label16);
            this.grbDatos.Controls.Add(this.label15);
            this.grbDatos.Controls.Add(this.txtDirDomicilio);
            this.grbDatos.Controls.Add(this.txtFechaNac);
            this.grbDatos.Controls.Add(this.label14);
            this.grbDatos.Controls.Add(this.label13);
            this.grbDatos.Controls.Add(this.txtLugarNac);
            this.grbDatos.Controls.Add(this.txtEdad);
            this.grbDatos.Controls.Add(this.label12);
            this.grbDatos.Controls.Add(this.cmbEstadoCivil);
            this.grbDatos.Controls.Add(this.label11);
            this.grbDatos.Controls.Add(this.cmbSexo);
            this.grbDatos.Controls.Add(this.label10);
            this.grbDatos.Controls.Add(this.cmbCalidad);
            this.grbDatos.Controls.Add(this.label9);
            this.grbDatos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbDatos.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDatos.ForeColor = System.Drawing.Color.Silver;
            this.grbDatos.Location = new System.Drawing.Point(6, 83);
            this.grbDatos.Name = "grbDatos";
            this.grbDatos.Size = new System.Drawing.Size(1322, 165);
            this.grbDatos.TabIndex = 12;
            this.grbDatos.TabStop = false;
            this.grbDatos.Text = "A - Del Paciente";
            // 
            // txtTelTrabajo
            // 
            this.txtTelTrabajo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelTrabajo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtTelTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelTrabajo.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelTrabajo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtTelTrabajo.Location = new System.Drawing.Point(1145, 122);
            this.txtTelTrabajo.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtTelTrabajo.Mask = "####-####";
            this.txtTelTrabajo.Name = "txtTelTrabajo";
            this.txtTelTrabajo.PromptChar = '.';
            this.txtTelTrabajo.Size = new System.Drawing.Size(140, 22);
            this.txtTelTrabajo.TabIndex = 23;
            this.txtTelTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.label20.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1141, 99);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(131, 20);
            this.label20.TabIndex = 22;
            this.label20.Text = "Teléfono de trabajo:";
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.label19.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(796, 99);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(177, 20);
            this.label19.TabIndex = 20;
            this.label19.Text = "Dirección de trabajo actual:";
            // 
            // txtDirTrabajo
            // 
            this.txtDirTrabajo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDirTrabajo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtDirTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDirTrabajo.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirTrabajo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtDirTrabajo.Location = new System.Drawing.Point(800, 122);
            this.txtDirTrabajo.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtDirTrabajo.Name = "txtDirTrabajo";
            this.txtDirTrabajo.Size = new System.Drawing.Size(317, 22);
            this.txtDirTrabajo.TabIndex = 21;
            this.txtDirTrabajo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.label18.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(528, 99);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(99, 20);
            this.label18.TabIndex = 18;
            this.label18.Text = "Patrono actual:";
            // 
            // txtPatrono
            // 
            this.txtPatrono.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPatrono.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtPatrono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPatrono.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPatrono.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtPatrono.Location = new System.Drawing.Point(532, 122);
            this.txtPatrono.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtPatrono.Name = "txtPatrono";
            this.txtPatrono.Size = new System.Drawing.Size(240, 22);
            this.txtPatrono.TabIndex = 19;
            this.txtPatrono.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.label17.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(316, 99);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(188, 20);
            this.label17.TabIndex = 16;
            this.label17.Text = "Ocupación del trabajo actual:";
            // 
            // txtOcupacion
            // 
            this.txtOcupacion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOcupacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtOcupacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtOcupacion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOcupacion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtOcupacion.Location = new System.Drawing.Point(320, 122);
            this.txtOcupacion.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtOcupacion.Name = "txtOcupacion";
            this.txtOcupacion.Size = new System.Drawing.Size(184, 22);
            this.txtOcupacion.TabIndex = 17;
            this.txtOcupacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtTelDomicilio
            // 
            this.txtTelDomicilio.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTelDomicilio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtTelDomicilio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelDomicilio.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelDomicilio.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtTelDomicilio.Location = new System.Drawing.Point(1145, 60);
            this.txtTelDomicilio.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtTelDomicilio.Mask = "####-####";
            this.txtTelDomicilio.Name = "txtTelDomicilio";
            this.txtTelDomicilio.PromptChar = '.';
            this.txtTelDomicilio.Size = new System.Drawing.Size(140, 22);
            this.txtTelDomicilio.TabIndex = 13;
            this.txtTelDomicilio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1141, 37);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(144, 20);
            this.label16.TabIndex = 12;
            this.label16.Text = "Teléfono de domicilio:";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(796, 37);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(190, 20);
            this.label15.TabIndex = 10;
            this.label15.Text = "Dirección de domicilio actual:";
            // 
            // txtDirDomicilio
            // 
            this.txtDirDomicilio.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDirDomicilio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtDirDomicilio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDirDomicilio.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirDomicilio.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtDirDomicilio.Location = new System.Drawing.Point(800, 60);
            this.txtDirDomicilio.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtDirDomicilio.Name = "txtDirDomicilio";
            this.txtDirDomicilio.Size = new System.Drawing.Size(317, 22);
            this.txtDirDomicilio.TabIndex = 11;
            this.txtDirDomicilio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtFechaNac
            // 
            this.txtFechaNac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFechaNac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtFechaNac.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFechaNac.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFechaNac.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtFechaNac.Location = new System.Drawing.Point(655, 60);
            this.txtFechaNac.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtFechaNac.Mask = "00/00/0000";
            this.txtFechaNac.Name = "txtFechaNac";
            this.txtFechaNac.PromptChar = '.';
            this.txtFechaNac.Size = new System.Drawing.Size(117, 22);
            this.txtFechaNac.TabIndex = 9;
            this.txtFechaNac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtFechaNac, "Fecha de nacimiento (dd/mm/aaaa)");
            this.txtFechaNac.ValidatingType = typeof(System.DateTime);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(651, 37);
            this.label14.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(121, 20);
            this.label14.TabIndex = 8;
            this.label14.Text = "Fecha nacimiento:";
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.label13.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(30, 99);
            this.label13.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 20);
            this.label13.TabIndex = 14;
            this.label13.Text = "Lugar de nacimiento:";
            // 
            // txtLugarNac
            // 
            this.txtLugarNac.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLugarNac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtLugarNac.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLugarNac.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLugarNac.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtLugarNac.Location = new System.Drawing.Point(34, 122);
            this.txtLugarNac.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtLugarNac.Name = "txtLugarNac";
            this.txtLugarNac.Size = new System.Drawing.Size(258, 22);
            this.txtLugarNac.TabIndex = 15;
            this.txtLugarNac.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtEdad
            // 
            this.txtEdad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEdad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtEdad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEdad.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEdad.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtEdad.Location = new System.Drawing.Point(574, 60);
            this.txtEdad.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtEdad.Mask = "00";
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.PromptChar = '.';
            this.txtEdad.Size = new System.Drawing.Size(53, 22);
            this.txtEdad.TabIndex = 7;
            this.txtEdad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(570, 37);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 20);
            this.label12.TabIndex = 6;
            this.label12.Text = "Edad:";
            // 
            // cmbEstadoCivil
            // 
            this.cmbEstadoCivil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbEstadoCivil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbEstadoCivil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEstadoCivil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbEstadoCivil.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEstadoCivil.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbEstadoCivil.IntegralHeight = false;
            this.cmbEstadoCivil.Items.AddRange(new object[] {
            "-Seleccionar-",
            "Soltero",
            "Casado",
            "Divorciado",
            "Viudo"});
            this.cmbEstadoCivil.Location = new System.Drawing.Point(394, 58);
            this.cmbEstadoCivil.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbEstadoCivil.Name = "cmbEstadoCivil";
            this.cmbEstadoCivil.Size = new System.Drawing.Size(152, 28);
            this.cmbEstadoCivil.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(390, 35);
            this.label11.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 20);
            this.label11.TabIndex = 4;
            this.label11.Text = "Estado civil:";
            // 
            // cmbSexo
            // 
            this.cmbSexo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbSexo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSexo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbSexo.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSexo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbSexo.IntegralHeight = false;
            this.cmbSexo.Items.AddRange(new object[] {
            "-Seleccionar-",
            "Masculino\t",
            "Femenino"});
            this.cmbSexo.Location = new System.Drawing.Point(214, 58);
            this.cmbSexo.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbSexo.Name = "cmbSexo";
            this.cmbSexo.Size = new System.Drawing.Size(152, 28);
            this.cmbSexo.TabIndex = 3;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(210, 35);
            this.label10.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Sexo:";
            // 
            // cmbCalidad
            // 
            this.cmbCalidad.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCalidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbCalidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCalidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbCalidad.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCalidad.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbCalidad.IntegralHeight = false;
            this.cmbCalidad.Items.AddRange(new object[] {
            "-Seleccionar-",
            "Asegurado",
            "Beneficiario(a)",
            "Pensionado",
            "Hijo"});
            this.cmbCalidad.Location = new System.Drawing.Point(34, 58);
            this.cmbCalidad.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbCalidad.Name = "cmbCalidad";
            this.cmbCalidad.Size = new System.Drawing.Size(152, 28);
            this.cmbCalidad.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(30, 35);
            this.label9.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Calidad:";
            // 
            // txtCentroAtencion
            // 
            this.txtCentroAtencion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCentroAtencion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtCentroAtencion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCentroAtencion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCentroAtencion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtCentroAtencion.Location = new System.Drawing.Point(834, 41);
            this.txtCentroAtencion.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtCentroAtencion.Name = "txtCentroAtencion";
            this.txtCentroAtencion.Size = new System.Drawing.Size(326, 22);
            this.txtCentroAtencion.TabIndex = 9;
            this.txtCentroAtencion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Silver;
            this.label3.Location = new System.Drawing.Point(49, 18);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "DUI:";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Silver;
            this.label6.Location = new System.Drawing.Point(557, 18);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 6;
            this.label6.Text = "Apellidos:";
            // 
            // txtDui
            // 
            this.txtDui.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDui.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtDui.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDui.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDui.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtDui.Location = new System.Drawing.Point(53, 41);
            this.txtDui.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtDui.Mask = "00000000-0";
            this.txtDui.Name = "txtDui";
            this.txtDui.PromptChar = '.';
            this.txtDui.Size = new System.Drawing.Size(93, 22);
            this.txtDui.TabIndex = 1;
            this.txtDui.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtApellidos
            // 
            this.txtApellidos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApellidos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtApellidos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApellidos.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidos.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtApellidos.Location = new System.Drawing.Point(561, 41);
            this.txtApellidos.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtApellidos.Name = "txtApellidos";
            this.txtApellidos.Size = new System.Drawing.Size(245, 22);
            this.txtApellidos.TabIndex = 7;
            this.txtApellidos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtApellidos, "Apellidos del paciente");
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Silver;
            this.label4.Location = new System.Drawing.Point(170, 18);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 20);
            this.label4.TabIndex = 2;
            this.label4.Text = "No. Afiliacion";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Silver;
            this.label5.Location = new System.Drawing.Point(284, 18);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nombres:";
            // 
            // txtNombres
            // 
            this.txtNombres.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNombres.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNombres.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombres.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombres.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNombres.Location = new System.Drawing.Point(288, 41);
            this.txtNombres.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNombres.Name = "txtNombres";
            this.txtNombres.Size = new System.Drawing.Size(245, 22);
            this.txtNombres.TabIndex = 5;
            this.txtNombres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tltAyuda.SetToolTip(this.txtNombres, "Nombres del paciente");
            // 
            // txtNoAfiliacion
            // 
            this.txtNoAfiliacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoAfiliacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNoAfiliacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNoAfiliacion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoAfiliacion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNoAfiliacion.Location = new System.Drawing.Point(174, 41);
            this.txtNoAfiliacion.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNoAfiliacion.Mask = "000000000";
            this.txtNoAfiliacion.Name = "txtNoAfiliacion";
            this.txtNoAfiliacion.PromptChar = '.';
            this.txtNoAfiliacion.Size = new System.Drawing.Size(86, 22);
            this.txtNoAfiliacion.TabIndex = 3;
            this.txtNoAfiliacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(115, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(391, 47);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lista de pacientes activos";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pnlPrint);
            this.panel1.Controls.Add(this.pnlAdd);
            this.panel1.Controls.Add(this.pnlCancel);
            this.panel1.Controls.Add(this.pnlDelete);
            this.panel1.Controls.Add(this.pnlSave);
            this.panel1.Controls.Add(this.pnlEdit);
            this.panel1.Location = new System.Drawing.Point(1076, 76);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(274, 56);
            this.panel1.TabIndex = 2;
            // 
            // pnlPrint
            // 
            this.pnlPrint.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pnlPrint.BackgroundImage = global::Clinica.Properties.Resources.printer;
            this.pnlPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlPrint.Location = new System.Drawing.Point(228, 8);
            this.pnlPrint.Name = "pnlPrint";
            this.pnlPrint.Size = new System.Drawing.Size(38, 39);
            this.pnlPrint.TabIndex = 15;
            this.tltAyuda.SetToolTip(this.pnlPrint, "Imprimir expediente");
            this.pnlPrint.Click += new System.EventHandler(this.pnlPrint_Click);
            // 
            // pnlAdd
            // 
            this.pnlAdd.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pnlAdd.BackgroundImage = global::Clinica.Properties.Resources.Add_User_Male;
            this.pnlAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAdd.Controls.Add(this.panel2);
            this.pnlAdd.Location = new System.Drawing.Point(8, 8);
            this.pnlAdd.Name = "pnlAdd";
            this.pnlAdd.Size = new System.Drawing.Size(38, 39);
            this.pnlAdd.TabIndex = 10;
            this.tltAyuda.SetToolTip(this.pnlAdd, "Nuevo expediente");
            this.pnlAdd.Click += new System.EventHandler(this.pnlAdd_Click);
            this.pnlAdd.MouseLeave += new System.EventHandler(this.pnlAdd_MouseLeave);
            this.pnlAdd.MouseHover += new System.EventHandler(this.pnlAdd_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(44, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(38, 39);
            this.panel2.TabIndex = 11;
            // 
            // pnlCancel
            // 
            this.pnlCancel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pnlCancel.BackgroundImage = global::Clinica.Properties.Resources.Cancel;
            this.pnlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCancel.Location = new System.Drawing.Point(184, 8);
            this.pnlCancel.Name = "pnlCancel";
            this.pnlCancel.Size = new System.Drawing.Size(38, 39);
            this.pnlCancel.TabIndex = 14;
            this.tltAyuda.SetToolTip(this.pnlCancel, "Cancelar");
            this.pnlCancel.Click += new System.EventHandler(this.pnlCancel_Click);
            this.pnlCancel.MouseLeave += new System.EventHandler(this.pnlCancel_MouseLeave);
            this.pnlCancel.MouseHover += new System.EventHandler(this.pnlCancel_MouseHover);
            // 
            // pnlDelete
            // 
            this.pnlDelete.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pnlDelete.BackgroundImage = global::Clinica.Properties.Resources.Remove_User_Male;
            this.pnlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelete.Controls.Add(this.panel6);
            this.pnlDelete.Location = new System.Drawing.Point(96, 8);
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.Size = new System.Drawing.Size(38, 39);
            this.pnlDelete.TabIndex = 12;
            this.tltAyuda.SetToolTip(this.pnlDelete, "Eliminar expediente");
            this.pnlDelete.Click += new System.EventHandler(this.pnlDelete_Click);
            this.pnlDelete.MouseLeave += new System.EventHandler(this.pnlDelete_MouseLeave);
            this.pnlDelete.MouseHover += new System.EventHandler(this.pnlDelete_MouseHover);
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(44, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(38, 39);
            this.panel6.TabIndex = 11;
            // 
            // pnlSave
            // 
            this.pnlSave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pnlSave.BackgroundImage = global::Clinica.Properties.Resources.Save;
            this.pnlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSave.Location = new System.Drawing.Point(140, 8);
            this.pnlSave.Name = "pnlSave";
            this.pnlSave.Size = new System.Drawing.Size(38, 39);
            this.pnlSave.TabIndex = 13;
            this.tltAyuda.SetToolTip(this.pnlSave, "Guardar");
            this.pnlSave.Click += new System.EventHandler(this.pnlSave_Click);
            this.pnlSave.MouseLeave += new System.EventHandler(this.pnlSave_MouseLeave);
            this.pnlSave.MouseHover += new System.EventHandler(this.pnlSave_MouseHover);
            // 
            // pnlEdit
            // 
            this.pnlEdit.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pnlEdit.BackgroundImage = global::Clinica.Properties.Resources.Registration;
            this.pnlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlEdit.Location = new System.Drawing.Point(52, 8);
            this.pnlEdit.Name = "pnlEdit";
            this.pnlEdit.Size = new System.Drawing.Size(38, 39);
            this.pnlEdit.TabIndex = 11;
            this.tltAyuda.SetToolTip(this.pnlEdit, "Editar expediente");
            this.pnlEdit.Click += new System.EventHandler(this.pnlEdit_Click);
            this.pnlEdit.MouseLeave += new System.EventHandler(this.pnlEdit_MouseLeave);
            this.pnlEdit.MouseHover += new System.EventHandler(this.pnlEdit_MouseHover);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // tltAyuda
            // 
            this.tltAyuda.AutomaticDelay = 400;
            this.tltAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.tltAyuda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tltAyuda.OwnerDraw = true;
            this.tltAyuda.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.tltAyuda_Draw);
            // 
            // pcbLogo
            // 
            this.pcbLogo.Image = global::Clinica.Properties.Resources.Hospital;
            this.pcbLogo.Location = new System.Drawing.Point(22, 54);
            this.pcbLogo.Name = "pcbLogo";
            this.pcbLogo.Size = new System.Drawing.Size(87, 83);
            this.pcbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbLogo.TabIndex = 9;
            this.pcbLogo.TabStop = false;
            // 
            // FrmExpediente
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(1362, 733);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pcbLogo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbcPacientes);
            this.Controls.Add(this.pnlTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmExpediente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expedientes";
            this.Load += new System.EventHandler(this.FrmExpediente_Load);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            this.tbcPacientes.ResumeLayout(false);
            this.tbpBuscar.ResumeLayout(false);
            this.tbpBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPacientes)).EndInit();
            this.tbpCUD.ResumeLayout(false);
            this.tbpCUD.PerformLayout();
            this.grbFamilia.ResumeLayout(false);
            this.grbFamilia.PerformLayout();
            this.grbActividad.ResumeLayout(false);
            this.grbActividad.PerformLayout();
            this.grbDatos.ResumeLayout(false);
            this.grbDatos.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pnlAdd.ResumeLayout(false);
            this.pnlDelete.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlSalir;
        private System.Windows.Forms.TabControl tbcPacientes;
        private System.Windows.Forms.TabPage tbpBuscar;
        private System.Windows.Forms.TextBox txtFiltrar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.DataGridView dgvPacientes;
        private System.Windows.Forms.TabPage tbpCUD;
        private System.Windows.Forms.GroupBox grbDatos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pcbLogo;
        private System.Windows.Forms.MaskedTextBox txtDui;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox txtNoAfiliacion;
        private System.Windows.Forms.TextBox txtNombres;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtApellidos;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCentroAtencion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.MaskedTextBox txtFecha;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbCalidad;
        private System.Windows.Forms.ComboBox cmbSexo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbEstadoCivil;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtEdad;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtLugarNac;
        private System.Windows.Forms.MaskedTextBox txtFechaNac;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtDirDomicilio;
        private System.Windows.Forms.MaskedTextBox txtTelDomicilio;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtOcupacion;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPatrono;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDirTrabajo;
        private System.Windows.Forms.MaskedTextBox txtTelTrabajo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.GroupBox grbActividad;
        private System.Windows.Forms.CheckBox ckbAct1;
        private System.Windows.Forms.CheckBox ckbAct9;
        private System.Windows.Forms.CheckBox ckbAct8;
        private System.Windows.Forms.CheckBox ckbAct7;
        private System.Windows.Forms.CheckBox ckbAct6;
        private System.Windows.Forms.CheckBox ckbAct5;
        private System.Windows.Forms.CheckBox ckbAct4;
        private System.Windows.Forms.CheckBox ckbAct3;
        private System.Windows.Forms.CheckBox ckbAct2;
        private System.Windows.Forms.GroupBox grbFamilia;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtConyugue;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtPadre;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtProporcionador;
        private System.Windows.Forms.TextBox txtMadre;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtEmergencia;
        private System.Windows.Forms.MaskedTextBox txtTelEmergencia;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtNombreArch;
        private System.Windows.Forms.Label lblTotalRegistros;
        private System.Windows.Forms.Panel pnlAdd;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.Panel pnlSave;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlMinimize;
        private System.Windows.Forms.ErrorProvider epError;
        private System.Windows.Forms.Label lblAnulados;
        private System.Windows.Forms.ToolTip tltAyuda;
        private System.Windows.Forms.Panel pnlPrint;
    }
}