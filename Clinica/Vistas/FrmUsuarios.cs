﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmUsuarios : Form
    {
        #region Variables / Instancias
        private int move, x, y;
        private bool adding, editing;
        private string userActive;
        Usuario objUser = new Usuario();
        Int32 userType;
        #endregion

        #region Métodos del formulario
        void all()
        {
            String sql = "Exec spMostrarUsuarios '" + userActive + "'";
            dgvUsuarios.DataSource = objUser.all(sql);
            for (int i = 0; i < dgvUsuarios.Rows.Count; i++)
            {
                if (Convert.ToInt32(dgvUsuarios.Rows[i].Cells[4].Value.ToString()) == 0)
                    dgvUsuarios.Rows[i].Cells[4].Value = "Desactivado";
                else if (Convert.ToInt32(dgvUsuarios.Rows[i].Cells[4].Value.ToString()) == 1)
                    dgvUsuarios.Rows[i].Cells[4].Value = "Activado";
            }
            // Ocultando columna
            dgvUsuarios.Columns[0].Visible = false;
            dgvUsuarios.Columns[5].Visible = false;
            if (existsRows())
                lblTotalRegistros.Text = "Total de registros: " + dgvUsuarios.RowCount + " de " + dgvUsuarios.Rows[0].Cells[5].Value.ToString();
            else
                lblTotalRegistros.Text = "Total de registros: " + dgvUsuarios.RowCount + " de " + 0;
        }

        // Método para buscar Usuarios
        void find(String value)
        {
            String sql = "Exec spBuscarUsuarios '" + value + "', '" + userActive + "'";
            dgvUsuarios.DataSource = objUser.all(sql);
            for (int i = 0; i < dgvUsuarios.Rows.Count; i++)
            {
                if (Convert.ToInt32(dgvUsuarios.Rows[i].Cells[4].Value.ToString()) == 0)
                    dgvUsuarios.Rows[i].Cells[4].Value = "Desactivado";
                else if (Convert.ToInt32(dgvUsuarios.Rows[i].Cells[4].Value.ToString()) == 1)
                    dgvUsuarios.Rows[i].Cells[4].Value = "Activado";
            }
            // Ocultando columna
            dgvUsuarios.Columns[0].Visible = false;
            dgvUsuarios.Columns[5].Visible = false;
            if (existsRows())
                lblTotalRegistros.Text = "Total de registros: " + dgvUsuarios.RowCount + " de " + dgvUsuarios.Rows[0].Cells[5].Value.ToString();
            else
                lblTotalRegistros.Text = "Total de registros: " + dgvUsuarios.RowCount + " de " + 0;
        }

        void load()
        {
            objUser.id = Convert.ToInt32(dgvUsuarios.SelectedRows[0].Cells[0].Value.ToString());
            objUser.user = dgvUsuarios.SelectedRows[0].Cells[1].Value.ToString();
            objUser.pass = "-";
            if (dgvUsuarios.SelectedRows[0].Cells[4].Value.ToString() == "Desactivado")
                objUser.state = 0;
            else if (dgvUsuarios.SelectedRows[0].Cells[4].Value.ToString() == "Activado")
                objUser.state = 1;
            if (dgvUsuarios.SelectedRows[0].Cells[3].Value.ToString() == "Administrador")
                objUser.userType = 1;
            if (dgvUsuarios.SelectedRows[0].Cells[3].Value.ToString() == "Usuario")
                objUser.userType = 2;
        }

        void read()
        {
            if (objUser.userType == 1)
            {
                rdbAdmin.Checked = true;
                rdbUsuario.Checked = false;
            }
            else if (objUser.userType == 2)
            {
                rdbUsuario.Checked = true;
                rdbAdmin.Checked = false;
            }
            if (objUser.state == 1)
            {
                rdbActivado.Checked = true;
                rdbDesactivado.Checked = false;
            }
            else if (objUser.state == 0)
            {
                rdbDesactivado.Checked = true;
                rdbActivado.Checked = false;
            }
            txtNombreUsuario.Text = objUser.user;
            txtPass.Text = "-";
        }

        // Método para verificar si existen registros
        Boolean existsRows()
        {
            Boolean estado = false;
            if (dgvUsuarios.Rows.Count > 0)
                estado = true;
            return estado;
        }

        void reset()
        {
            txtNombreUsuario.Clear();
            txtPass.Clear();
            rdbAdmin.Checked = false;
            rdbUsuario.Checked = true;
            rdbActivado.Checked = true;
            rdbDesactivado.Checked = false;
        }

        void send()
        {
            int state = -1, type = -1;
            if (rdbAdmin.Checked == true && rdbUsuario.Checked == false)
                type = 1;
            else if (rdbUsuario.Checked == true && rdbAdmin.Checked == false)
                type = 2;
            if (rdbActivado.Checked == true && rdbDesactivado.Checked == false)
                state = 1;
            else if (rdbDesactivado.Checked == true && rdbActivado.Checked == false)
                state = 0;
            objUser.user = txtNombreUsuario.Text.Replace("'", "''");
            objUser.pass = txtPass.Text.Replace("'", "''");
            objUser.state = state;
            objUser.userType = type;
        }

        Boolean isValid()
        {
            Boolean estado = true;
            epError.Clear();
            if (String.IsNullOrEmpty(txtNombreUsuario.Text))
            {
                epError.SetError(txtNombreUsuario, "Ingrese un nombre de usuario.");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtPass.Text))
            {
                epError.SetError(txtPass, "Ingrese una contraseña");
                estado = false;
            }
            return estado;
        }

        // Metodo para cambiar imagen a escala de grises
        void grayScale(Panel pnl, Image img)
        {
            Bitmap bmp = new Bitmap(img);
            // Obteniendo dimension de imagen
            int width = bmp.Width;
            int height = bmp.Height;

            // Color del pixel
            Color p;

            // Escala de grises
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Obteniendo valor del pixel
                    p = bmp.GetPixel(x, y);

                    // Extrayendo el componente ARGB del pixel
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    // Encontrando promedio
                    int avg = (r + g + b) / 3;

                    // Asigando nuevo valor al pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                }
            }

            //load grayscale image in picturebox2
            pnl.BackgroundImage = bmp;
        }

        // Método para habilitar y deshabilitar los botones
        void enableButtons()
        {
            // Verificando si hay o no registros
            if (dgvUsuarios.RowCount == 0)
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_User_Male;
                grayScale(pnlEdit, Properties.Resources.Registration);
                grayScale(pnlDelete, Properties.Resources.Remove_User_Male);
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }
            else
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = true;
                pnlDelete.Enabled = true;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_User_Male;
                pnlEdit.BackgroundImage = Properties.Resources.Registration;
                pnlDelete.BackgroundImage = Properties.Resources.Remove_User_Male;
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }

            // Verificando si se está agregando
            if (adding == true || editing == true)
            {
                pnlAdd.Enabled = false;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = true;
                pnlCancel.Enabled = true;
                grayScale(pnlAdd, Properties.Resources.Add_User_Male);
                grayScale(pnlEdit, Properties.Resources.Registration);
                grayScale(pnlDelete, Properties.Resources.Remove_User_Male);
                pnlSave.BackgroundImage = Properties.Resources.Save;
                pnlCancel.BackgroundImage = Properties.Resources.Cancel;
            }
        }

        // Método para habilitar y deshabilitar los controles
        void enableControls(Boolean option)
        {
            txtNombreUsuario.Enabled = option;
            txtPass.Enabled = option;
            rdbAdmin.Enabled = option;
            rdbUsuario.Enabled = option;
            rdbActivado.Enabled = option;
            rdbDesactivado.Enabled = option;
            txtNombreUsuario.Focus();
        }
        #endregion

        #region CRUD
        void add()
        {
            adding = true;
            editing = false;
            reset();
            enableControls(true);
            enableButtons();
            dgvUsuarios.Enabled = false;
            tbcUsuarios.SelectedIndex = 1;
        }

        void edit()
        {
            try
            {
                if (isValid())
                {
                    adding = false;
                    editing = true;
                    enableControls(true);
                    dgvUsuarios.Enabled = false;
                    tbcUsuarios.SelectedIndex = 1;
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Selecione un usuario para editar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcUsuarios.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void delete()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea eliminar al usuario " + txtNombreUsuario.Text + "?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        if (objUser.crud("delete") > 0)
                        {
                            frmConf = new FrmConfirmDialog("Mensaje", "Usuario eliminado correctamente.", "Ok", "No", "info");
                            frmConf.ShowDialog();
                        }
                        else
                        {
                            frmConf = new FrmConfirmDialog("Error", "Error al eliminar el usuario.", "Ok", "No", "error");
                            frmConf.ShowDialog();
                        }
                        all();
                        reset();
                        tbcUsuarios.SelectedIndex = 0;
                    }
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione un usuario para eliminar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcUsuarios.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void save()
        {
            try
            {
                if (isValid() && txtPass.Text != "-")
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea guardar el usuario?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        send();
                        if (adding == true && editing == false) // Si se está Ingresando Datos
                        {
                            if (objUser.crud("save") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Usuario agregado correctamente.", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcUsuarios.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al agregar el usuario.", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        else if (adding == false && editing == true) // Si se está Modificando Datos
                        {
                            if (objUser.crud("update") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Usuario editado correctamente", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcUsuarios.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al editar el usuario", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        adding = false;
                        editing = false;
                        reset();
                        enableControls(false);
                        dgvUsuarios.Enabled = true;
                    }
                }
                else
                    epError.SetError(txtPass, "Ingrese una nueva contraseña");
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void cancel()
        {
            adding = false;
            editing = false;
            reset();
            enableButtons();
            enableControls(false);
            epError.Clear();
            dgvUsuarios.Enabled = true;
            tbcUsuarios.SelectedIndex = 0;
        }

        #endregion

        public FrmUsuarios()
        {
            InitializeComponent();
        }

        public FrmUsuarios(string active, Int32 type)
        {
            InitializeComponent();
            userActive = active;
            userType = type;
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            all();
            reset();
            enableButtons();
            enableControls(false);
            if (!existsRows())
                dgvUsuarios.Enabled = false;
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            FrmMain objMain = new FrmMain(userActive, userType);
            if (!adding && !editing)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana usuarios y volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "Todos los datos no guardados se perderan.\n¿Seguro que desea volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pnlMinimize_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(35, 35, 35), new TransitionType_Linear(200));
        }

        private void pnlMinimize_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            move = 1;
            x = e.X;
            y = e.Y;
        }

        private void pnlTitulo_MouseUp(object sender, MouseEventArgs e)
        {
            move = 0;
        }

        private void pnlTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == 1)
                SetDesktopLocation(MousePosition.X - x, MousePosition.Y - y);
        }

        private void pnlAdd_Click(object sender, EventArgs e)
        {
            add();
        }

        private void pnlAdd_MouseHover(object sender, EventArgs e)
        {
            pnlAdd.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlAdd_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlAdd, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlEdit_Click(object sender, EventArgs e)
        {
            edit();
        }

        private void pnlEdit_MouseHover(object sender, EventArgs e)
        {
            pnlEdit.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlEdit_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlEdit, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlDelete_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void pnlDelete_MouseHover(object sender, EventArgs e)
        {
            pnlDelete.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlDelete_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlDelete, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void pnlSave_MouseHover(object sender, EventArgs e)
        {
            pnlSave.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlSave_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSave, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlCancel_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void pnlCancel_MouseHover(object sender, EventArgs e)
        {
            pnlCancel.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlCancel_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlCancel, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void rdbActivado_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbActivado.Checked == true)
                pnlEstado.BackColor = Color.SeaGreen;
            else if (rdbDesactivado.Checked == true)
                pnlEstado.BackColor = Color.Firebrick;
        }

        private void rdbDesactivado_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbActivado.Checked == true)
                pnlEstado.BackColor = Color.SeaGreen;
            else if (rdbDesactivado.Checked == true)
                pnlEstado.BackColor = Color.Firebrick;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            find(txtBuscar.Text.Replace("'", "''"));
            enableButtons();
        }

        private void dgvUsuarios_Click(object sender, EventArgs e)
        {
            if (existsRows())
            {
                reset();
                load();
                read();
                if (dgvUsuarios.SelectedRows[0].Cells[4].Value.ToString() == "Activado")
                    dgvUsuarios.SelectedRows[0].DefaultCellStyle.SelectionBackColor = Color.SeaGreen;
                else if (dgvUsuarios.SelectedRows[0].Cells[4].Value.ToString() == "Desactivado")
                    dgvUsuarios.SelectedRows[0].DefaultCellStyle.SelectionBackColor = Color.Firebrick;
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "Debe haber al menos un usuario registrado para continuar.", "Ok", "No", "info");
                frmConf.ShowDialog();
            }
        }

        private void dgvUsuarios_DoubleClick(object sender, EventArgs e)
        {
            if (existsRows())
                tbcUsuarios.SelectedIndex = 1;
        }

    }
}
