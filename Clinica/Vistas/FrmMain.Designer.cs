﻿namespace Clinica.Vistas
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblConsultas = new System.Windows.Forms.Label();
            this.lblAnulados = new System.Windows.Forms.Label();
            this.lblCerrarSesion = new System.Windows.Forms.Label();
            this.lblBienvenida = new System.Windows.Forms.Label();
            this.grbPrincipal = new System.Windows.Forms.GroupBox();
            this.lblExpedientes = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblRepConActual = new System.Windows.Forms.Label();
            this.lblRepConEspecifico = new System.Windows.Forms.Label();
            this.lblRepConGeneral = new System.Windows.Forms.Label();
            this.lblRepEntregasPlanC = new System.Windows.Forms.Label();
            this.lblRepEntregasPlanA = new System.Windows.Forms.Label();
            this.tmrNotify = new System.Windows.Forms.Timer(this.components);
            this.tltAyuda = new System.Windows.Forms.ToolTip(this.components);
            this.lblRecordatorios = new System.Windows.Forms.Label();
            this.lblUsuarios = new System.Windows.Forms.Label();
            this.lblEmpleados = new System.Windows.Forms.Label();
            this.lblEntregas = new System.Windows.Forms.Label();
            this.lblEntegasPlanC = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlTitulo.SuspendLayout();
            this.grbPrincipal.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.pnlTitulo.Controls.Add(this.label1);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(843, 82);
            this.pnlTitulo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 25F);
            this.label1.ForeColor = System.Drawing.Color.Silver;
            this.label1.Location = new System.Drawing.Point(155, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(533, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "SISTEMA CLÍNICA GRUPO MIGUEL";
            // 
            // lblConsultas
            // 
            this.lblConsultas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblConsultas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblConsultas.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultas.ForeColor = System.Drawing.Color.Silver;
            this.lblConsultas.Location = new System.Drawing.Point(553, 33);
            this.lblConsultas.Name = "lblConsultas";
            this.lblConsultas.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.lblConsultas.Size = new System.Drawing.Size(249, 52);
            this.lblConsultas.TabIndex = 0;
            this.lblConsultas.Text = "Consultas";
            this.lblConsultas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblConsultas, "Gestión de consultas");
            this.lblConsultas.Click += new System.EventHandler(this.lblConsultas_Click);
            this.lblConsultas.MouseLeave += new System.EventHandler(this.lblConsultas_MouseLeave);
            this.lblConsultas.MouseHover += new System.EventHandler(this.lblConsultas_MouseHover);
            // 
            // lblAnulados
            // 
            this.lblAnulados.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblAnulados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAnulados.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnulados.ForeColor = System.Drawing.Color.Silver;
            this.lblAnulados.Location = new System.Drawing.Point(284, 33);
            this.lblAnulados.Name = "lblAnulados";
            this.lblAnulados.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.lblAnulados.Size = new System.Drawing.Size(249, 52);
            this.lblAnulados.TabIndex = 0;
            this.lblAnulados.Text = "Anulados";
            this.lblAnulados.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblAnulados, "Gestión Expedientes anulados");
            this.lblAnulados.Click += new System.EventHandler(this.lblAnulados_Click);
            this.lblAnulados.MouseLeave += new System.EventHandler(this.lblAnulados_MouseLeave);
            this.lblAnulados.MouseHover += new System.EventHandler(this.lblAnulados_MouseHover);
            // 
            // lblCerrarSesion
            // 
            this.lblCerrarSesion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCerrarSesion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.lblCerrarSesion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCerrarSesion.ForeColor = System.Drawing.Color.White;
            this.lblCerrarSesion.Location = new System.Drawing.Point(671, 642);
            this.lblCerrarSesion.Name = "lblCerrarSesion";
            this.lblCerrarSesion.Size = new System.Drawing.Size(144, 35);
            this.lblCerrarSesion.TabIndex = 5;
            this.lblCerrarSesion.Text = "CERRAR SESIÓN";
            this.lblCerrarSesion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblCerrarSesion, "Cerrar sesión actual");
            this.lblCerrarSesion.Click += new System.EventHandler(this.lblCerrarSesion_Click);
            this.lblCerrarSesion.MouseLeave += new System.EventHandler(this.lblCerrarSesion_MouseLeave);
            this.lblCerrarSesion.MouseHover += new System.EventHandler(this.lblCerrarSesion_MouseHover);
            // 
            // lblBienvenida
            // 
            this.lblBienvenida.AutoSize = true;
            this.lblBienvenida.Font = new System.Drawing.Font("Segoe UI Light", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBienvenida.ForeColor = System.Drawing.Color.Silver;
            this.lblBienvenida.Location = new System.Drawing.Point(203, 138);
            this.lblBienvenida.Name = "lblBienvenida";
            this.lblBienvenida.Size = new System.Drawing.Size(229, 46);
            this.lblBienvenida.TabIndex = 1;
            this.lblBienvenida.Text = "Bienvenido(a), ";
            // 
            // grbPrincipal
            // 
            this.grbPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.grbPrincipal.Controls.Add(this.lblConsultas);
            this.grbPrincipal.Controls.Add(this.lblExpedientes);
            this.grbPrincipal.Controls.Add(this.lblAnulados);
            this.grbPrincipal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grbPrincipal.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPrincipal.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.grbPrincipal.Location = new System.Drawing.Point(12, 229);
            this.grbPrincipal.Name = "grbPrincipal";
            this.grbPrincipal.Size = new System.Drawing.Size(819, 106);
            this.grbPrincipal.TabIndex = 2;
            this.grbPrincipal.TabStop = false;
            this.grbPrincipal.Text = "Principal";
            // 
            // lblExpedientes
            // 
            this.lblExpedientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblExpedientes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblExpedientes.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpedientes.ForeColor = System.Drawing.Color.Silver;
            this.lblExpedientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblExpedientes.Location = new System.Drawing.Point(15, 33);
            this.lblExpedientes.Name = "lblExpedientes";
            this.lblExpedientes.Padding = new System.Windows.Forms.Padding(25, 0, 20, 0);
            this.lblExpedientes.Size = new System.Drawing.Size(249, 52);
            this.lblExpedientes.TabIndex = 0;
            this.lblExpedientes.Text = "Expedientes";
            this.lblExpedientes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblExpedientes, "Gestión expedientes activos");
            this.lblExpedientes.Click += new System.EventHandler(this.lblExpedientes_Click);
            this.lblExpedientes.MouseLeave += new System.EventHandler(this.lblExpedientes_MouseLeave);
            this.lblExpedientes.MouseHover += new System.EventHandler(this.lblExpedientes_MouseHover);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.groupBox1.Controls.Add(this.lblRepConActual);
            this.groupBox1.Controls.Add(this.lblRepConEspecifico);
            this.groupBox1.Controls.Add(this.lblRepConGeneral);
            this.groupBox1.Controls.Add(this.lblRepEntregasPlanC);
            this.groupBox1.Controls.Add(this.lblRepEntregasPlanA);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Location = new System.Drawing.Point(12, 341);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(819, 179);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Reportes";
            // 
            // lblRepConActual
            // 
            this.lblRepConActual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblRepConActual.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRepConActual.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.lblRepConActual.ForeColor = System.Drawing.Color.Silver;
            this.lblRepConActual.Location = new System.Drawing.Point(285, 40);
            this.lblRepConActual.Name = "lblRepConActual";
            this.lblRepConActual.Size = new System.Drawing.Size(249, 52);
            this.lblRepConActual.TabIndex = 0;
            this.lblRepConActual.Text = "Reporte de consultas\r\n(mes actual)";
            this.lblRepConActual.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblRepConActual, "Generar reporte de consultas del mes actual");
            this.lblRepConActual.Click += new System.EventHandler(this.lblRepConActual_Click);
            // 
            // lblRepConEspecifico
            // 
            this.lblRepConEspecifico.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblRepConEspecifico.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRepConEspecifico.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.lblRepConEspecifico.ForeColor = System.Drawing.Color.Silver;
            this.lblRepConEspecifico.Location = new System.Drawing.Point(554, 40);
            this.lblRepConEspecifico.Name = "lblRepConEspecifico";
            this.lblRepConEspecifico.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.lblRepConEspecifico.Size = new System.Drawing.Size(249, 52);
            this.lblRepConEspecifico.TabIndex = 0;
            this.lblRepConEspecifico.Text = "Reporte de consultas\r\n(especificar mes y año)";
            this.lblRepConEspecifico.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblRepConEspecifico, "Generar reporte de consultas de mes especifico");
            this.lblRepConEspecifico.Click += new System.EventHandler(this.lblRepConEspecifico_Click);
            // 
            // lblRepConGeneral
            // 
            this.lblRepConGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblRepConGeneral.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRepConGeneral.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.lblRepConGeneral.ForeColor = System.Drawing.Color.Silver;
            this.lblRepConGeneral.Location = new System.Drawing.Point(16, 40);
            this.lblRepConGeneral.Name = "lblRepConGeneral";
            this.lblRepConGeneral.Size = new System.Drawing.Size(249, 52);
            this.lblRepConGeneral.TabIndex = 0;
            this.lblRepConGeneral.Text = "Reporte general de consultas\r\n(últimas 50)";
            this.lblRepConGeneral.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblRepConGeneral, "Generar reporte de ultimas 50 consultas");
            this.lblRepConGeneral.Click += new System.EventHandler(this.lblRepConGeneral_Click);
            // 
            // lblRepEntregasPlanC
            // 
            this.lblRepEntregasPlanC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblRepEntregasPlanC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRepEntregasPlanC.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.lblRepEntregasPlanC.ForeColor = System.Drawing.Color.Silver;
            this.lblRepEntregasPlanC.Location = new System.Drawing.Point(452, 112);
            this.lblRepEntregasPlanC.Name = "lblRepEntregasPlanC";
            this.lblRepEntregasPlanC.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.lblRepEntregasPlanC.Size = new System.Drawing.Size(249, 52);
            this.lblRepEntregasPlanC.TabIndex = 0;
            this.lblRepEntregasPlanC.Text = "Reporte de registro diario de consultas";
            this.lblRepEntregasPlanC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblRepEntregasPlanC, "Reporte consultas planificación familiar (Especificar mes y año)");
            this.lblRepEntregasPlanC.Click += new System.EventHandler(this.lblRepEntregasPlanC_Click);
            // 
            // lblRepEntregasPlanA
            // 
            this.lblRepEntregasPlanA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblRepEntregasPlanA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRepEntregasPlanA.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.lblRepEntregasPlanA.ForeColor = System.Drawing.Color.Silver;
            this.lblRepEntregasPlanA.Location = new System.Drawing.Point(121, 112);
            this.lblRepEntregasPlanA.Name = "lblRepEntregasPlanA";
            this.lblRepEntregasPlanA.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.lblRepEntregasPlanA.Size = new System.Drawing.Size(249, 52);
            this.lblRepEntregasPlanA.TabIndex = 0;
            this.lblRepEntregasPlanA.Text = "Reporte de entregas de anticonceptivos";
            this.lblRepEntregasPlanA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblRepEntregasPlanA, "Reporte de entregas (Especificar mes y año)");
            this.lblRepEntregasPlanA.Click += new System.EventHandler(this.lblRepEntregasPlanA_Click);
            // 
            // tmrNotify
            // 
            this.tmrNotify.Tick += new System.EventHandler(this.tmrNotify_Tick);
            // 
            // tltAyuda
            // 
            this.tltAyuda.AutomaticDelay = 400;
            this.tltAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.tltAyuda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tltAyuda.OwnerDraw = true;
            this.tltAyuda.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.tltAyuda_Draw);
            // 
            // lblRecordatorios
            // 
            this.lblRecordatorios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRecordatorios.AutoSize = true;
            this.lblRecordatorios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.lblRecordatorios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblRecordatorios.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecordatorios.ForeColor = System.Drawing.Color.DarkGray;
            this.lblRecordatorios.Location = new System.Drawing.Point(32, 647);
            this.lblRecordatorios.Name = "lblRecordatorios";
            this.lblRecordatorios.Padding = new System.Windows.Forms.Padding(9, 3, 9, 5);
            this.lblRecordatorios.Size = new System.Drawing.Size(120, 29);
            this.lblRecordatorios.TabIndex = 4;
            this.lblRecordatorios.Text = "Recordatorios";
            this.tltAyuda.SetToolTip(this.lblRecordatorios, "Gestionar recodatorios");
            this.lblRecordatorios.Click += new System.EventHandler(this.lblRecordatorios_Click);
            // 
            // lblUsuarios
            // 
            this.lblUsuarios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUsuarios.AutoSize = true;
            this.lblUsuarios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.lblUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblUsuarios.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuarios.ForeColor = System.Drawing.Color.DarkGray;
            this.lblUsuarios.Location = new System.Drawing.Point(167, 647);
            this.lblUsuarios.Name = "lblUsuarios";
            this.lblUsuarios.Padding = new System.Windows.Forms.Padding(9, 3, 9, 5);
            this.lblUsuarios.Size = new System.Drawing.Size(84, 29);
            this.lblUsuarios.TabIndex = 13;
            this.lblUsuarios.Text = "Usuarios";
            this.tltAyuda.SetToolTip(this.lblUsuarios, "Gestionar usuarios");
            this.lblUsuarios.Click += new System.EventHandler(this.lblUsuarios_Click);
            // 
            // lblEmpleados
            // 
            this.lblEmpleados.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmpleados.AutoSize = true;
            this.lblEmpleados.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.lblEmpleados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblEmpleados.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpleados.ForeColor = System.Drawing.Color.DarkGray;
            this.lblEmpleados.Location = new System.Drawing.Point(266, 647);
            this.lblEmpleados.Name = "lblEmpleados";
            this.lblEmpleados.Padding = new System.Windows.Forms.Padding(9, 3, 9, 5);
            this.lblEmpleados.Size = new System.Drawing.Size(101, 29);
            this.lblEmpleados.TabIndex = 14;
            this.lblEmpleados.Text = "Empleados";
            this.tltAyuda.SetToolTip(this.lblEmpleados, "Gestionar empleados");
            this.lblEmpleados.Visible = false;
            // 
            // lblEntregas
            // 
            this.lblEntregas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblEntregas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblEntregas.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntregas.ForeColor = System.Drawing.Color.Silver;
            this.lblEntregas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblEntregas.Location = new System.Drawing.Point(121, 33);
            this.lblEntregas.Name = "lblEntregas";
            this.lblEntregas.Padding = new System.Windows.Forms.Padding(25, 0, 20, 0);
            this.lblEntregas.Size = new System.Drawing.Size(249, 52);
            this.lblEntregas.TabIndex = 0;
            this.lblEntregas.Text = "Registro diario de entrega de anticonceptivos";
            this.lblEntregas.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblEntregas, "Gestión de entregas de anticonceptivos");
            this.lblEntregas.Click += new System.EventHandler(this.lblEntregas_Click);
            // 
            // lblEntegasPlanC
            // 
            this.lblEntegasPlanC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(45)))));
            this.lblEntegasPlanC.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblEntegasPlanC.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntegasPlanC.ForeColor = System.Drawing.Color.Silver;
            this.lblEntegasPlanC.Location = new System.Drawing.Point(452, 33);
            this.lblEntegasPlanC.Name = "lblEntegasPlanC";
            this.lblEntegasPlanC.Padding = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.lblEntegasPlanC.Size = new System.Drawing.Size(249, 52);
            this.lblEntegasPlanC.TabIndex = 0;
            this.lblEntegasPlanC.Text = "Registro diario de consultas";
            this.lblEntegasPlanC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tltAyuda.SetToolTip(this.lblEntegasPlanC, "Gestión consultas (Planificaciín familiar)");
            this.lblEntegasPlanC.Click += new System.EventHandler(this.lblEntegasPlanC_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.groupBox2.Controls.Add(this.lblEntregas);
            this.groupBox2.Controls.Add(this.lblEntegasPlanC);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Location = new System.Drawing.Point(12, 526);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(819, 106);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Programa planificación familiar";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Clinica.Properties.Resources.Hospital;
            this.pictureBox1.Location = new System.Drawing.Point(48, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(132, 127);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // FrmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(843, 690);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblEmpleados);
            this.Controls.Add(this.lblUsuarios);
            this.Controls.Add(this.lblCerrarSesion);
            this.Controls.Add(this.lblRecordatorios);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grbPrincipal);
            this.Controls.Add(this.lblBienvenida);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pnlTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseUp);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            this.grbPrincipal.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCerrarSesion;
        private System.Windows.Forms.Label lblAnulados;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblBienvenida;
        private System.Windows.Forms.GroupBox grbPrincipal;
        private System.Windows.Forms.Label lblConsultas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRepEntregasPlanA;
        private System.Windows.Forms.Label lblRepConGeneral;
        private System.Windows.Forms.Label lblRepConEspecifico;
        private System.Windows.Forms.Label lblRepEntregasPlanC;
        private System.Windows.Forms.Label lblRepConActual;
        private System.Windows.Forms.Timer tmrNotify;
        private System.Windows.Forms.ToolTip tltAyuda;
        private System.Windows.Forms.Label lblRecordatorios;
        private System.Windows.Forms.Label lblExpedientes;
        private System.Windows.Forms.Label lblUsuarios;
        private System.Windows.Forms.Label lblEmpleados;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblEntregas;
        private System.Windows.Forms.Label lblEntegasPlanC;
    }
}