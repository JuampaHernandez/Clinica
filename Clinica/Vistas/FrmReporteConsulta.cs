﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Vistas
{
    public partial class FrmReporteConsulta : Form
    {
        public FrmReporteConsulta()
        {
            InitializeComponent();
        }

        private void FrmReporteConsulta_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'dtsConsultas.spReporteConsultas' Puede moverla o quitarla según sea necesario.
            this.spReporteConsultasTableAdapter.Fill(this.dtsConsultas.spReporteConsultas);
            this.reportViewer1.RefreshReport();
        }
    }
}
