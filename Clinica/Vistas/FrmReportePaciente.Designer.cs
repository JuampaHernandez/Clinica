﻿namespace Clinica.Vistas
{
    partial class FrmReportePaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReportePaciente));
            this.spReportePacienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dtsPacientes = new Clinica.DataSets.dtsPacientes();
            this.spReportePacienteTableAdapter = new Clinica.DataSets.dtsPacientesTableAdapters.spReportePacienteTableAdapter();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.spReportePacienteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPacientes)).BeginInit();
            this.SuspendLayout();
            // 
            // spReportePacienteBindingSource
            // 
            this.spReportePacienteBindingSource.DataMember = "spReportePaciente";
            this.spReportePacienteBindingSource.DataSource = this.dtsPacientes;
            // 
            // dtsPacientes
            // 
            this.dtsPacientes.DataSetName = "dtsPacientes";
            this.dtsPacientes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // spReportePacienteTableAdapter
            // 
            this.spReportePacienteTableAdapter.ClearBeforeFill = true;
            // 
            // reportViewer1
            // 
            this.reportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportViewer1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.reportViewer1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            reportDataSource1.Name = "dtsPaciente";
            reportDataSource1.Value = this.spReportePacienteBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.EnableExternalImages = true;
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "Clinica.Reportes.rptPaciente.rdlc";
            this.reportViewer1.LocalReport.ReportPath = "../../Reportes/rptPaciente.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ShowBackButton = false;
            this.reportViewer1.ShowContextMenu = false;
            this.reportViewer1.ShowCredentialPrompts = false;
            this.reportViewer1.ShowDocumentMapButton = false;
            this.reportViewer1.ShowFindControls = false;
            this.reportViewer1.ShowPageNavigationControls = false;
            this.reportViewer1.ShowParameterPrompts = false;
            this.reportViewer1.ShowPromptAreaButton = false;
            this.reportViewer1.ShowStopButton = false;
            this.reportViewer1.Size = new System.Drawing.Size(932, 549);
            this.reportViewer1.TabIndex = 1;
            // 
            // FrmReportePaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 549);
            this.Controls.Add(this.reportViewer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(948, 588);
            this.Name = "FrmReportePaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expediente de paciente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmReportePaciente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.spReportePacienteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtsPacientes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource spReportePacienteBindingSource;
        private DataSets.dtsPacientes dtsPacientes;
        private DataSets.dtsPacientesTableAdapters.spReportePacienteTableAdapter spReportePacienteTableAdapter;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
    }
}