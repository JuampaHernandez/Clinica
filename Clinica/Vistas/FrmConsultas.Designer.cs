﻿namespace Clinica.Vistas
{
    partial class FrmConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConsultas));
            this.pnlTitulo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbcConsultas = new System.Windows.Forms.TabControl();
            this.tbpBuscar = new System.Windows.Forms.TabPage();
            this.lblTotalRegistros = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.lblBuscar = new System.Windows.Forms.Label();
            this.dgvConsultas = new System.Windows.Forms.DataGridView();
            this.tbpCUD = new System.Windows.Forms.TabPage();
            this.txtCodSec = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtCodPrincipal = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtDiagSecundario = new System.Windows.Forms.TextBox();
            this.cmbResultadoConsulta = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMedicamento = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtDiagPrincipal = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdbOtros = new System.Windows.Forms.RadioButton();
            this.rdbRX = new System.Windows.Forms.RadioButton();
            this.txtNoExaLab = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbNoGenera = new System.Windows.Forms.RadioButton();
            this.rdbGenera = new System.Windows.Forms.RadioButton();
            this.txtSerie = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbSubsecuente = new System.Windows.Forms.RadioButton();
            this.rdbPrimera = new System.Windows.Forms.RadioButton();
            this.cmbTipoConsulta = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cmbTipoAsegurado = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtFecha = new System.Windows.Forms.MaskedTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtEdad = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNoAfiliacion = new System.Windows.Forms.MaskedTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.tltAyuda = new System.Windows.Forms.ToolTip(this.components);
            this.pnlAdd = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlCancel = new System.Windows.Forms.Panel();
            this.pnlDelete = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pnlSave = new System.Windows.Forms.Panel();
            this.pnlEdit = new System.Windows.Forms.Panel();
            this.pnlActualizar = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pcbLogo = new System.Windows.Forms.PictureBox();
            this.pnlMinimize = new System.Windows.Forms.Panel();
            this.pnlSalir = new System.Windows.Forms.Panel();
            this.pnlTitulo.SuspendLayout();
            this.tbcConsultas.SuspendLayout();
            this.tbpBuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultas)).BeginInit();
            this.tbpCUD.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            this.pnlAdd.SuspendLayout();
            this.pnlDelete.SuspendLayout();
            this.pnlActualizar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTitulo
            // 
            this.pnlTitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.pnlTitulo.Controls.Add(this.pnlMinimize);
            this.pnlTitulo.Controls.Add(this.pnlSalir);
            this.pnlTitulo.Controls.Add(this.label1);
            this.pnlTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTitulo.Location = new System.Drawing.Point(0, 0);
            this.pnlTitulo.Name = "pnlTitulo";
            this.pnlTitulo.Size = new System.Drawing.Size(1362, 39);
            this.pnlTitulo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semilight", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "CONSULTAS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 26F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(120, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(472, 47);
            this.label2.TabIndex = 1;
            this.label2.Text = "Listado de consultas realizadas";
            // 
            // tbcConsultas
            // 
            this.tbcConsultas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcConsultas.Controls.Add(this.tbpBuscar);
            this.tbcConsultas.Controls.Add(this.tbpCUD);
            this.tbcConsultas.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcConsultas.Location = new System.Drawing.Point(12, 149);
            this.tbcConsultas.Name = "tbcConsultas";
            this.tbcConsultas.SelectedIndex = 0;
            this.tbcConsultas.Size = new System.Drawing.Size(1342, 571);
            this.tbcConsultas.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcConsultas.TabIndex = 3;
            // 
            // tbpBuscar
            // 
            this.tbpBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpBuscar.Controls.Add(this.pnlActualizar);
            this.tbpBuscar.Controls.Add(this.lblTotalRegistros);
            this.tbpBuscar.Controls.Add(this.txtBuscar);
            this.tbpBuscar.Controls.Add(this.lblBuscar);
            this.tbpBuscar.Controls.Add(this.dgvConsultas);
            this.tbpBuscar.Location = new System.Drawing.Point(4, 26);
            this.tbpBuscar.Name = "tbpBuscar";
            this.tbpBuscar.Padding = new System.Windows.Forms.Padding(3);
            this.tbpBuscar.Size = new System.Drawing.Size(1334, 541);
            this.tbpBuscar.TabIndex = 0;
            this.tbpBuscar.Text = "Consultas";
            // 
            // lblTotalRegistros
            // 
            this.lblTotalRegistros.AutoSize = true;
            this.lblTotalRegistros.BackColor = System.Drawing.Color.Transparent;
            this.lblTotalRegistros.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRegistros.ForeColor = System.Drawing.Color.Gray;
            this.lblTotalRegistros.Location = new System.Drawing.Point(1166, 62);
            this.lblTotalRegistros.Name = "lblTotalRegistros";
            this.lblTotalRegistros.Size = new System.Drawing.Size(116, 19);
            this.lblTotalRegistros.TabIndex = 2;
            this.lblTotalRegistros.Text = "Total de registros: ";
            // 
            // txtBuscar
            // 
            this.txtBuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtBuscar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBuscar.ForeColor = System.Drawing.Color.LightGray;
            this.txtBuscar.Location = new System.Drawing.Point(37, 43);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(338, 22);
            this.txtBuscar.TabIndex = 1;
            this.txtBuscar.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtBuscar.TextChanged += new System.EventHandler(this.txtBuscar_TextChanged);
            // 
            // lblBuscar
            // 
            this.lblBuscar.AutoSize = true;
            this.lblBuscar.BackColor = System.Drawing.Color.Transparent;
            this.lblBuscar.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscar.Location = new System.Drawing.Point(33, 19);
            this.lblBuscar.Name = "lblBuscar";
            this.lblBuscar.Size = new System.Drawing.Size(59, 21);
            this.lblBuscar.TabIndex = 0;
            this.lblBuscar.Text = "Buscar:";
            // 
            // dgvConsultas
            // 
            this.dgvConsultas.AllowUserToAddRows = false;
            this.dgvConsultas.AllowUserToDeleteRows = false;
            this.dgvConsultas.AllowUserToResizeColumns = false;
            this.dgvConsultas.AllowUserToResizeRows = false;
            this.dgvConsultas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvConsultas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvConsultas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvConsultas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvConsultas.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvConsultas.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvConsultas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvConsultas.ColumnHeadersHeight = 40;
            this.dgvConsultas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvConsultas.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvConsultas.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvConsultas.EnableHeadersVisualStyles = false;
            this.dgvConsultas.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.dgvConsultas.Location = new System.Drawing.Point(6, 84);
            this.dgvConsultas.MultiSelect = false;
            this.dgvConsultas.Name = "dgvConsultas";
            this.dgvConsultas.ReadOnly = true;
            this.dgvConsultas.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvConsultas.RowHeadersVisible = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.LightGray;
            this.dgvConsultas.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvConsultas.RowTemplate.Height = 35;
            this.dgvConsultas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvConsultas.Size = new System.Drawing.Size(1322, 451);
            this.dgvConsultas.TabIndex = 3;
            this.dgvConsultas.Click += new System.EventHandler(this.dgvConsultas_Click);
            this.dgvConsultas.DoubleClick += new System.EventHandler(this.dgvConsultas_DoubleClick);
            // 
            // tbpCUD
            // 
            this.tbpCUD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.tbpCUD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpCUD.Controls.Add(this.txtCodSec);
            this.tbpCUD.Controls.Add(this.label24);
            this.tbpCUD.Controls.Add(this.txtCodPrincipal);
            this.tbpCUD.Controls.Add(this.label23);
            this.tbpCUD.Controls.Add(this.label22);
            this.tbpCUD.Controls.Add(this.txtDiagSecundario);
            this.tbpCUD.Controls.Add(this.cmbResultadoConsulta);
            this.tbpCUD.Controls.Add(this.label21);
            this.tbpCUD.Controls.Add(this.txtMedicamento);
            this.tbpCUD.Controls.Add(this.label20);
            this.tbpCUD.Controls.Add(this.label18);
            this.tbpCUD.Controls.Add(this.txtDiagPrincipal);
            this.tbpCUD.Controls.Add(this.groupBox3);
            this.tbpCUD.Controls.Add(this.txtNoExaLab);
            this.tbpCUD.Controls.Add(this.label17);
            this.tbpCUD.Controls.Add(this.groupBox2);
            this.tbpCUD.Controls.Add(this.txtSerie);
            this.tbpCUD.Controls.Add(this.label19);
            this.tbpCUD.Controls.Add(this.groupBox1);
            this.tbpCUD.Controls.Add(this.cmbTipoConsulta);
            this.tbpCUD.Controls.Add(this.label16);
            this.tbpCUD.Controls.Add(this.cmbTipoAsegurado);
            this.tbpCUD.Controls.Add(this.label15);
            this.tbpCUD.Controls.Add(this.txtFecha);
            this.tbpCUD.Controls.Add(this.label12);
            this.tbpCUD.Controls.Add(this.txtEdad);
            this.tbpCUD.Controls.Add(this.label6);
            this.tbpCUD.Controls.Add(this.txtSexo);
            this.tbpCUD.Controls.Add(this.label4);
            this.tbpCUD.Controls.Add(this.label5);
            this.tbpCUD.Controls.Add(this.txtNoAfiliacion);
            this.tbpCUD.Location = new System.Drawing.Point(4, 26);
            this.tbpCUD.Name = "tbpCUD";
            this.tbpCUD.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCUD.Size = new System.Drawing.Size(1334, 541);
            this.tbpCUD.TabIndex = 1;
            this.tbpCUD.Text = "Gestionar";
            // 
            // txtCodSec
            // 
            this.txtCodSec.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodSec.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtCodSec.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodSec.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodSec.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtCodSec.Location = new System.Drawing.Point(800, 439);
            this.txtCodSec.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtCodSec.Name = "txtCodSec";
            this.txtCodSec.Size = new System.Drawing.Size(105, 22);
            this.txtCodSec.TabIndex = 44;
            this.txtCodSec.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(799, 416);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 20);
            this.label24.TabIndex = 43;
            this.label24.Text = "Código (C.I.E)";
            // 
            // txtCodPrincipal
            // 
            this.txtCodPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCodPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtCodPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodPrincipal.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodPrincipal.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtCodPrincipal.Location = new System.Drawing.Point(945, 340);
            this.txtCodPrincipal.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtCodPrincipal.Name = "txtCodPrincipal";
            this.txtCodPrincipal.Size = new System.Drawing.Size(105, 22);
            this.txtCodPrincipal.TabIndex = 40;
            this.txtCodPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(941, 317);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(91, 20);
            this.label23.TabIndex = 39;
            this.label23.Text = "Código (C.I.E)";
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(144, 416);
            this.label22.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(148, 20);
            this.label22.TabIndex = 41;
            this.label22.Text = "Dignóstico secundatio:";
            // 
            // txtDiagSecundario
            // 
            this.txtDiagSecundario.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiagSecundario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtDiagSecundario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDiagSecundario.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiagSecundario.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtDiagSecundario.Location = new System.Drawing.Point(148, 439);
            this.txtDiagSecundario.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtDiagSecundario.Name = "txtDiagSecundario";
            this.txtDiagSecundario.Size = new System.Drawing.Size(624, 22);
            this.txtDiagSecundario.TabIndex = 42;
            this.txtDiagSecundario.TextChanged += new System.EventHandler(this.txtDiagSecundario_TextChanged);
            // 
            // cmbResultadoConsulta
            // 
            this.cmbResultadoConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbResultadoConsulta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbResultadoConsulta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbResultadoConsulta.DropDownWidth = 153;
            this.cmbResultadoConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbResultadoConsulta.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbResultadoConsulta.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbResultadoConsulta.IntegralHeight = false;
            this.cmbResultadoConsulta.Items.AddRange(new object[] {
            "--Seleccionar--",
            "1. Alta",
            "2. Referencia",
            "3. Retorno",
            "4. Cita",
            "5. Ingreso",
            "6. Observación"});
            this.cmbResultadoConsulta.Location = new System.Drawing.Point(933, 437);
            this.cmbResultadoConsulta.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbResultadoConsulta.Name = "cmbResultadoConsulta";
            this.cmbResultadoConsulta.Size = new System.Drawing.Size(256, 28);
            this.cmbResultadoConsulta.TabIndex = 46;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(929, 414);
            this.label21.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(161, 20);
            this.label21.TabIndex = 45;
            this.label21.Text = "Resultado de la consulta:";
            // 
            // txtMedicamento
            // 
            this.txtMedicamento.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMedicamento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtMedicamento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMedicamento.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMedicamento.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtMedicamento.Location = new System.Drawing.Point(1111, 229);
            this.txtMedicamento.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtMedicamento.Name = "txtMedicamento";
            this.txtMedicamento.Size = new System.Drawing.Size(151, 22);
            this.txtMedicamento.TabIndex = 36;
            this.txtMedicamento.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(1107, 206);
            this.label20.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(155, 20);
            this.label20.TabIndex = 35;
            this.label20.Text = "Medicamento prescrito:";
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(289, 317);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(133, 20);
            this.label18.TabIndex = 37;
            this.label18.Text = "Dignóstico principal:";
            // 
            // txtDiagPrincipal
            // 
            this.txtDiagPrincipal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiagPrincipal.AutoCompleteCustomSource.AddRange(new string[] {
            "ASDFD",
            "Col"});
            this.txtDiagPrincipal.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtDiagPrincipal.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtDiagPrincipal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtDiagPrincipal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDiagPrincipal.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiagPrincipal.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtDiagPrincipal.Location = new System.Drawing.Point(293, 340);
            this.txtDiagPrincipal.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtDiagPrincipal.Name = "txtDiagPrincipal";
            this.txtDiagPrincipal.Size = new System.Drawing.Size(624, 22);
            this.txtDiagPrincipal.TabIndex = 38;
            this.txtDiagPrincipal.TextChanged += new System.EventHandler(this.txtDiagPrincipal_TextChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.groupBox3.Controls.Add(this.rdbOtros);
            this.groupBox3.Controls.Add(this.rdbRX);
            this.groupBox3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox3.Location = new System.Drawing.Point(935, 195);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(163, 63);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Exámen de gabinete";
            // 
            // rdbOtros
            // 
            this.rdbOtros.AutoSize = true;
            this.rdbOtros.Location = new System.Drawing.Point(78, 25);
            this.rdbOtros.Name = "rdbOtros";
            this.rdbOtros.Size = new System.Drawing.Size(60, 23);
            this.rdbOtros.TabIndex = 1;
            this.rdbOtros.Text = "Otros";
            this.rdbOtros.UseVisualStyleBackColor = true;
            // 
            // rdbRX
            // 
            this.rdbRX.AutoSize = true;
            this.rdbRX.Checked = true;
            this.rdbRX.Location = new System.Drawing.Point(29, 25);
            this.rdbRX.Name = "rdbRX";
            this.rdbRX.Size = new System.Drawing.Size(43, 23);
            this.rdbRX.TabIndex = 0;
            this.rdbRX.TabStop = true;
            this.rdbRX.Text = "RX";
            this.rdbRX.UseVisualStyleBackColor = true;
            // 
            // txtNoExaLab
            // 
            this.txtNoExaLab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoExaLab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNoExaLab.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNoExaLab.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoExaLab.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNoExaLab.Location = new System.Drawing.Point(712, 229);
            this.txtNoExaLab.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNoExaLab.Name = "txtNoExaLab";
            this.txtNoExaLab.Size = new System.Drawing.Size(195, 22);
            this.txtNoExaLab.TabIndex = 33;
            this.txtNoExaLab.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(708, 206);
            this.label17.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(192, 20);
            this.label17.TabIndex = 32;
            this.label17.Text = "N° de exámen de laboratorio:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.groupBox2.Controls.Add(this.rdbNoGenera);
            this.groupBox2.Controls.Add(this.rdbGenera);
            this.groupBox2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Location = new System.Drawing.Point(473, 195);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(227, 63);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Subsidio";
            // 
            // rdbNoGenera
            // 
            this.rdbNoGenera.AutoSize = true;
            this.rdbNoGenera.Location = new System.Drawing.Point(109, 25);
            this.rdbNoGenera.Name = "rdbNoGenera";
            this.rdbNoGenera.Size = new System.Drawing.Size(90, 23);
            this.rdbNoGenera.TabIndex = 1;
            this.rdbNoGenera.Text = "No genera";
            this.rdbNoGenera.UseVisualStyleBackColor = true;
            // 
            // rdbGenera
            // 
            this.rdbGenera.AutoSize = true;
            this.rdbGenera.Checked = true;
            this.rdbGenera.Location = new System.Drawing.Point(29, 25);
            this.rdbGenera.Name = "rdbGenera";
            this.rdbGenera.Size = new System.Drawing.Size(69, 23);
            this.rdbGenera.TabIndex = 0;
            this.rdbGenera.TabStop = true;
            this.rdbGenera.Text = "Genera";
            this.rdbGenera.UseVisualStyleBackColor = true;
            // 
            // txtSerie
            // 
            this.txtSerie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSerie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSerie.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSerie.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtSerie.Location = new System.Drawing.Point(315, 229);
            this.txtSerie.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtSerie.Name = "txtSerie";
            this.txtSerie.Size = new System.Drawing.Size(130, 22);
            this.txtSerie.TabIndex = 30;
            this.txtSerie.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(311, 206);
            this.label19.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(127, 20);
            this.label19.TabIndex = 28;
            this.label19.Text = "Incapacidad (serie):";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.groupBox1.Controls.Add(this.rdbSubsecuente);
            this.groupBox1.Controls.Add(this.rdbPrimera);
            this.groupBox1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Location = new System.Drawing.Point(76, 195);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(227, 63);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo";
            // 
            // rdbSubsecuente
            // 
            this.rdbSubsecuente.AutoSize = true;
            this.rdbSubsecuente.Location = new System.Drawing.Point(104, 24);
            this.rdbSubsecuente.Name = "rdbSubsecuente";
            this.rdbSubsecuente.Size = new System.Drawing.Size(99, 23);
            this.rdbSubsecuente.TabIndex = 1;
            this.rdbSubsecuente.Text = "Subsecuente";
            this.rdbSubsecuente.UseVisualStyleBackColor = true;
            // 
            // rdbPrimera
            // 
            this.rdbPrimera.AutoSize = true;
            this.rdbPrimera.Checked = true;
            this.rdbPrimera.Location = new System.Drawing.Point(24, 24);
            this.rdbPrimera.Name = "rdbPrimera";
            this.rdbPrimera.Size = new System.Drawing.Size(74, 23);
            this.rdbPrimera.TabIndex = 0;
            this.rdbPrimera.TabStop = true;
            this.rdbPrimera.Text = "Primera";
            this.rdbPrimera.UseVisualStyleBackColor = true;
            // 
            // cmbTipoConsulta
            // 
            this.cmbTipoConsulta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTipoConsulta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbTipoConsulta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoConsulta.DropDownWidth = 152;
            this.cmbTipoConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTipoConsulta.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoConsulta.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbTipoConsulta.IntegralHeight = false;
            this.cmbTipoConsulta.ItemHeight = 20;
            this.cmbTipoConsulta.Items.AddRange(new object[] {
            "--Seleccionar--",
            "1. Enfermedad común",
            "2. Accidente común",
            "3. Accidente de trabajo",
            "4. Enfermeddad profesional",
            "5. Maternidad"});
            this.cmbTipoConsulta.Location = new System.Drawing.Point(992, 96);
            this.cmbTipoConsulta.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbTipoConsulta.Name = "cmbTipoConsulta";
            this.cmbTipoConsulta.Size = new System.Drawing.Size(199, 28);
            this.cmbTipoConsulta.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(988, 73);
            this.label16.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 20);
            this.label16.TabIndex = 25;
            this.label16.Text = "Tipo consulta:";
            // 
            // cmbTipoAsegurado
            // 
            this.cmbTipoAsegurado.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTipoAsegurado.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.cmbTipoAsegurado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTipoAsegurado.DropDownWidth = 153;
            this.cmbTipoAsegurado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbTipoAsegurado.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTipoAsegurado.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.cmbTipoAsegurado.IntegralHeight = false;
            this.cmbTipoAsegurado.Items.AddRange(new object[] {
            "--Seleccionar--",
            "1. Cotizantes",
            "2. Pensionados",
            "3. Conyugues o compañeros de vida",
            "4. Hijos de empleados del ISSS",
            "5. Padres de empleados del ISSS",
            "6. No asegurados",
            "7. Cesantes"});
            this.cmbTipoAsegurado.Location = new System.Drawing.Point(709, 96);
            this.cmbTipoAsegurado.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.cmbTipoAsegurado.Name = "cmbTipoAsegurado";
            this.cmbTipoAsegurado.Size = new System.Drawing.Size(255, 28);
            this.cmbTipoAsegurado.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(705, 73);
            this.label15.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(108, 20);
            this.label15.TabIndex = 23;
            this.label15.Text = "Tipo asegurado:";
            // 
            // txtFecha
            // 
            this.txtFecha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtFecha.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFecha.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtFecha.Location = new System.Drawing.Point(146, 98);
            this.txtFecha.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtFecha.Mask = "00/00/0000";
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.PromptChar = '.';
            this.txtFecha.Size = new System.Drawing.Size(131, 22);
            this.txtFecha.TabIndex = 10;
            this.txtFecha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFecha.ValidatingType = typeof(System.DateTime);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(142, 75);
            this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 20);
            this.label12.TabIndex = 9;
            this.label12.Text = "Fecha:";
            // 
            // txtEdad
            // 
            this.txtEdad.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEdad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtEdad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEdad.Enabled = false;
            this.txtEdad.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEdad.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtEdad.Location = new System.Drawing.Point(419, 98);
            this.txtEdad.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtEdad.Mask = "00";
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.PromptChar = '.';
            this.txtEdad.Size = new System.Drawing.Size(86, 22);
            this.txtEdad.TabIndex = 18;
            this.txtEdad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(529, 75);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 20);
            this.label6.TabIndex = 19;
            this.label6.Text = "Sexo:";
            // 
            // txtSexo
            // 
            this.txtSexo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSexo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtSexo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSexo.Enabled = false;
            this.txtSexo.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSexo.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtSexo.Location = new System.Drawing.Point(533, 98);
            this.txtSexo.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.Size = new System.Drawing.Size(148, 22);
            this.txtSexo.TabIndex = 20;
            this.txtSexo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(301, 75);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 20);
            this.label4.TabIndex = 15;
            this.label4.Text = "N° Afiliacion:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(415, 75);
            this.label5.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 20);
            this.label5.TabIndex = 17;
            this.label5.Text = "Edad:";
            // 
            // txtNoAfiliacion
            // 
            this.txtNoAfiliacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoAfiliacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.txtNoAfiliacion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNoAfiliacion.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoAfiliacion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.txtNoAfiliacion.Location = new System.Drawing.Point(305, 98);
            this.txtNoAfiliacion.Margin = new System.Windows.Forms.Padding(3, 3, 25, 3);
            this.txtNoAfiliacion.Mask = "000000000";
            this.txtNoAfiliacion.Name = "txtNoAfiliacion";
            this.txtNoAfiliacion.PromptChar = '.';
            this.txtNoAfiliacion.Size = new System.Drawing.Size(86, 22);
            this.txtNoAfiliacion.TabIndex = 16;
            this.txtNoAfiliacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNoAfiliacion.TextChanged += new System.EventHandler(this.txtNoAfiliacion_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.Controls.Add(this.pnlAdd);
            this.panel1.Controls.Add(this.pnlCancel);
            this.panel1.Controls.Add(this.pnlDelete);
            this.panel1.Controls.Add(this.pnlSave);
            this.panel1.Controls.Add(this.pnlEdit);
            this.panel1.Location = new System.Drawing.Point(1120, 73);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(5);
            this.panel1.Size = new System.Drawing.Size(230, 56);
            this.panel1.TabIndex = 2;
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            this.epError.Icon = ((System.Drawing.Icon)(resources.GetObject("epError.Icon")));
            // 
            // tltAyuda
            // 
            this.tltAyuda.AutomaticDelay = 400;
            this.tltAyuda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(21)))));
            this.tltAyuda.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tltAyuda.OwnerDraw = true;
            this.tltAyuda.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.tltAyuda_Draw);
            // 
            // pnlAdd
            // 
            this.pnlAdd.BackgroundImage = global::Clinica.Properties.Resources.Add_File1;
            this.pnlAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlAdd.Controls.Add(this.panel2);
            this.pnlAdd.Location = new System.Drawing.Point(8, 8);
            this.pnlAdd.Name = "pnlAdd";
            this.pnlAdd.Size = new System.Drawing.Size(38, 39);
            this.pnlAdd.TabIndex = 10;
            this.tltAyuda.SetToolTip(this.pnlAdd, "Nueva consulta");
            this.pnlAdd.Click += new System.EventHandler(this.pnlAdd_Click);
            this.pnlAdd.MouseLeave += new System.EventHandler(this.pnlAdd_MouseLeave);
            this.pnlAdd.MouseHover += new System.EventHandler(this.pnlAdd_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Location = new System.Drawing.Point(44, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(38, 39);
            this.panel2.TabIndex = 11;
            // 
            // pnlCancel
            // 
            this.pnlCancel.BackgroundImage = global::Clinica.Properties.Resources.Cancel;
            this.pnlCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCancel.Location = new System.Drawing.Point(184, 8);
            this.pnlCancel.Name = "pnlCancel";
            this.pnlCancel.Size = new System.Drawing.Size(38, 39);
            this.pnlCancel.TabIndex = 14;
            this.tltAyuda.SetToolTip(this.pnlCancel, "Cancelar");
            this.pnlCancel.Click += new System.EventHandler(this.pnlCancel_Click);
            this.pnlCancel.MouseLeave += new System.EventHandler(this.pnlCancel_MouseLeave);
            this.pnlCancel.MouseHover += new System.EventHandler(this.pnlCancel_MouseHover);
            // 
            // pnlDelete
            // 
            this.pnlDelete.BackgroundImage = global::Clinica.Properties.Resources.Delete1;
            this.pnlDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlDelete.Controls.Add(this.panel6);
            this.pnlDelete.Location = new System.Drawing.Point(96, 8);
            this.pnlDelete.Name = "pnlDelete";
            this.pnlDelete.Size = new System.Drawing.Size(38, 39);
            this.pnlDelete.TabIndex = 12;
            this.tltAyuda.SetToolTip(this.pnlDelete, "Eliminar Consulta");
            this.pnlDelete.Click += new System.EventHandler(this.pnlDelete_Click);
            this.pnlDelete.MouseLeave += new System.EventHandler(this.pnlDelete_MouseLeave);
            this.pnlDelete.MouseHover += new System.EventHandler(this.pnlDelete_MouseHover);
            // 
            // panel6
            // 
            this.panel6.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel6.Location = new System.Drawing.Point(44, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(38, 39);
            this.panel6.TabIndex = 11;
            // 
            // pnlSave
            // 
            this.pnlSave.BackgroundImage = global::Clinica.Properties.Resources.Save;
            this.pnlSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSave.Location = new System.Drawing.Point(140, 8);
            this.pnlSave.Name = "pnlSave";
            this.pnlSave.Size = new System.Drawing.Size(38, 39);
            this.pnlSave.TabIndex = 13;
            this.tltAyuda.SetToolTip(this.pnlSave, "Guardar");
            this.pnlSave.Click += new System.EventHandler(this.pnlSave_Click);
            this.pnlSave.MouseLeave += new System.EventHandler(this.pnlSave_MouseLeave);
            this.pnlSave.MouseHover += new System.EventHandler(this.pnlSave_MouseHover);
            // 
            // pnlEdit
            // 
            this.pnlEdit.BackgroundImage = global::Clinica.Properties.Resources.Edit_File;
            this.pnlEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlEdit.Location = new System.Drawing.Point(52, 8);
            this.pnlEdit.Name = "pnlEdit";
            this.pnlEdit.Size = new System.Drawing.Size(38, 39);
            this.pnlEdit.TabIndex = 11;
            this.tltAyuda.SetToolTip(this.pnlEdit, "Editar Consulta");
            this.pnlEdit.Click += new System.EventHandler(this.pnlEdit_Click);
            this.pnlEdit.MouseLeave += new System.EventHandler(this.pnlEdit_MouseLeave);
            this.pnlEdit.MouseHover += new System.EventHandler(this.pnlEdit_MouseHover);
            // 
            // pnlActualizar
            // 
            this.pnlActualizar.BackgroundImage = global::Clinica.Properties.Resources.repeat_1;
            this.pnlActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlActualizar.Controls.Add(this.panel4);
            this.pnlActualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlActualizar.Location = new System.Drawing.Point(1290, 6);
            this.pnlActualizar.Name = "pnlActualizar";
            this.pnlActualizar.Size = new System.Drawing.Size(38, 39);
            this.pnlActualizar.TabIndex = 11;
            this.tltAyuda.SetToolTip(this.pnlActualizar, "Actualizar tabla");
            this.pnlActualizar.Click += new System.EventHandler(this.pnlActualizar_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(44, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(38, 39);
            this.panel4.TabIndex = 11;
            // 
            // pcbLogo
            // 
            this.pcbLogo.Image = global::Clinica.Properties.Resources.Hospital;
            this.pcbLogo.Location = new System.Drawing.Point(22, 54);
            this.pcbLogo.Name = "pcbLogo";
            this.pcbLogo.Size = new System.Drawing.Size(87, 83);
            this.pcbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbLogo.TabIndex = 11;
            this.pcbLogo.TabStop = false;
            // 
            // pnlMinimize
            // 
            this.pnlMinimize.BackgroundImage = global::Clinica.Properties.Resources.Horizontal_Line_96px;
            this.pnlMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlMinimize.Location = new System.Drawing.Point(1286, 0);
            this.pnlMinimize.Name = "pnlMinimize";
            this.pnlMinimize.Size = new System.Drawing.Size(38, 39);
            this.pnlMinimize.TabIndex = 2;
            this.tltAyuda.SetToolTip(this.pnlMinimize, "Minimizar");
            this.pnlMinimize.Click += new System.EventHandler(this.pnlMinimize_Click);
            this.pnlMinimize.MouseLeave += new System.EventHandler(this.pnlMinimize_MouseLeave);
            this.pnlMinimize.MouseHover += new System.EventHandler(this.pnlMinimize_MouseHover);
            // 
            // pnlSalir
            // 
            this.pnlSalir.BackgroundImage = global::Clinica.Properties.Resources.Delete_100px;
            this.pnlSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlSalir.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSalir.Location = new System.Drawing.Point(1324, 0);
            this.pnlSalir.Name = "pnlSalir";
            this.pnlSalir.Size = new System.Drawing.Size(38, 39);
            this.pnlSalir.TabIndex = 1;
            this.tltAyuda.SetToolTip(this.pnlSalir, "Cerrar");
            this.pnlSalir.Click += new System.EventHandler(this.pnlSalir_Click);
            this.pnlSalir.MouseLeave += new System.EventHandler(this.pnlSalir_MouseLeave);
            this.pnlSalir.MouseHover += new System.EventHandler(this.pnlSalir_MouseHover);
            // 
            // FrmConsultas
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(1362, 733);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbcConsultas);
            this.Controls.Add(this.pcbLogo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pnlTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmConsultas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmConsultas";
            this.Load += new System.EventHandler(this.FrmConsultas_Load);
            this.pnlTitulo.ResumeLayout(false);
            this.pnlTitulo.PerformLayout();
            this.tbcConsultas.ResumeLayout(false);
            this.tbpBuscar.ResumeLayout(false);
            this.tbpBuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConsultas)).EndInit();
            this.tbpCUD.ResumeLayout(false);
            this.tbpCUD.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            this.pnlAdd.ResumeLayout(false);
            this.pnlDelete.ResumeLayout(false);
            this.pnlActualizar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTitulo;
        private System.Windows.Forms.Panel pnlMinimize;
        private System.Windows.Forms.Panel pnlSalir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pcbLogo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tbcConsultas;
        private System.Windows.Forms.TabPage tbpBuscar;
        private System.Windows.Forms.Label lblTotalRegistros;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.Label lblBuscar;
        private System.Windows.Forms.DataGridView dgvConsultas;
        private System.Windows.Forms.TabPage tbpCUD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox txtNoAfiliacion;
        private System.Windows.Forms.MaskedTextBox txtEdad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MaskedTextBox txtFecha;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbTipoAsegurado;
        private System.Windows.Forms.ComboBox cmbTipoConsulta;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbSubsecuente;
        private System.Windows.Forms.RadioButton rdbPrimera;
        private System.Windows.Forms.TextBox txtSerie;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbNoGenera;
        private System.Windows.Forms.RadioButton rdbGenera;
        private System.Windows.Forms.TextBox txtNoExaLab;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdbOtros;
        private System.Windows.Forms.RadioButton rdbRX;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtDiagPrincipal;
        private System.Windows.Forms.TextBox txtMedicamento;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cmbResultadoConsulta;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtDiagSecundario;
        private System.Windows.Forms.TextBox txtCodSec;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtCodPrincipal;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlAdd;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Panel pnlDelete;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel pnlSave;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.ErrorProvider epError;
        private System.Windows.Forms.ToolTip tltAyuda;
        private System.Windows.Forms.Panel pnlActualizar;
        private System.Windows.Forms.Panel panel4;
    }
}