﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmConsultas : Form
    {
        #region Variales / Instancias
        Boolean adding = false, editing = false;
        String userActive;
        Int32 userType;
        Consulta objCon = new Consulta();
        FrmMain objMain = new FrmMain();
        #endregion

        #region Metodos del formulario

        void all()
        {
            String sql = "EXEC spMostrarConsultas";
            dgvConsultas.DataSource = objCon.allC(sql);
            // Ocultando todas las columnas
            for (int i = 0; i <= 18; i++)
                dgvConsultas.Columns[i].Visible = false;
            dgvConsultas.Columns[1].Visible = true;
            dgvConsultas.Columns[2].Visible = true;
            dgvConsultas.Columns[6].Visible = true;
            dgvConsultas.Columns[7].Visible = true;
            dgvConsultas.Columns[8].Visible = true;
            dgvConsultas.Columns[9].Visible = true;
            dgvConsultas.Columns[10].Visible = true;
            dgvConsultas.Columns[12].Visible = true;
            dgvConsultas.Columns[13].Visible = true;
            lblTotalRegistros.Text = "Total de registros: " + dgvConsultas.Rows.Count;
        }

        // Método para buscar consultas
        void find(String value)
        {
            String sql = "EXEC spBuscarConsultas '" + value + "'";
            dgvConsultas.DataSource = objCon.allC(sql);
            // Ocultando todas las columnas
            for (int i = 0; i <= 18; i++)
                dgvConsultas.Columns[i].Visible = false;
            dgvConsultas.Columns[1].Visible = true;
            dgvConsultas.Columns[2].Visible = true;
            dgvConsultas.Columns[6].Visible = true;
            dgvConsultas.Columns[7].Visible = true;
            dgvConsultas.Columns[8].Visible = true;
            dgvConsultas.Columns[9].Visible = true;
            dgvConsultas.Columns[10].Visible = true;
            dgvConsultas.Columns[12].Visible = true;
            dgvConsultas.Columns[13].Visible = true;
            lblTotalRegistros.Text = "Total de registros: " + dgvConsultas.Rows.Count;
        }

        void data()
        {
            txtDiagPrincipal.AutoCompleteCustomSource = objCon.data();
            txtDiagPrincipal.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtDiagPrincipal.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtDiagSecundario.AutoCompleteCustomSource = objCon.data();
            txtDiagSecundario.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtDiagSecundario.AutoCompleteSource = AutoCompleteSource.CustomSource;
        }

        Boolean isValid()
        {
            Boolean estado = true;
            epError.Clear();
            if (cmbTipoAsegurado.SelectedIndex == 0)
            {
                epError.SetError(cmbTipoAsegurado, "Seleccione el tipo de asegurado");
                estado = false;
            }
            if (cmbTipoConsulta.SelectedIndex == 0)
            {
                epError.SetError(cmbTipoConsulta, "Seleccione el tipo de consulta");
                estado = false;
            }
            if (cmbResultadoConsulta.SelectedIndex == 0)
            {
                epError.SetError(cmbResultadoConsulta, "Seleccione el resultado de la consulta");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtFecha.Text))
            {
                epError.SetError(txtFecha, "Ingrese la fecha de la consulta");
                estado = false;
            }
            else
            {
                try
                {
                    String fecha = Convert.ToDateTime(txtFecha.Text).ToString("dd/MM/yyyy");
                    Convert.ToDateTime(fecha);
                }
                catch
                {
                    epError.SetError(txtFecha, "Ingrese una fecha correcta");
                    estado = false;
                }
            }
            if (String.IsNullOrEmpty(txtNoAfiliacion.Text))
            {
                epError.SetError(txtNoAfiliacion, "Ingrese el N° de afiliación del paciente");
                estado = false;
            }
            else if (String.IsNullOrEmpty(txtEdad.Text) || String.IsNullOrWhiteSpace(txtSexo.Text))
            {
                epError.SetError(txtNoAfiliacion, "Ingrese un N° de afiliación valido");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtSerie.Text))
            {
                epError.SetError(txtSerie, "Ingrese la serie de la incapacidad");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtNoExaLab.Text))
            {
                epError.SetError(txtNoExaLab, "Ingrese el N° de exámen de laboratorio");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtMedicamento.Text))
            {
                epError.SetError(txtMedicamento, "Ingrese el medicamento prescrito");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtDiagPrincipal.Text))
            {
                epError.SetError(txtDiagPrincipal, "Ingrese el diagnostico principal");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtCodPrincipal.Text))
            {
                epError.SetError(txtCodPrincipal, "Ingrese el código (C.I.E.)");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtDiagSecundario.Text))
            {
                txtDiagSecundario.Text = "N/A";
                txtCodSec.Text = "N/A";
            }
            if (String.IsNullOrWhiteSpace(txtCodSec.Text))
            {
                txtCodSec.Text = "N/A";
            }
            return estado;
        }

        // Metodo para cambiar imagen a escala de grises
        void grayScale(Panel pnl, Image img)
        {
            Bitmap bmp = new Bitmap(img);
            // Obteniendo dimension de imagen
            int width = bmp.Width;
            int height = bmp.Height;

            // Color del pixel
            Color p;

            // Escala de grises
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Obteniendo valor del pixel
                    p = bmp.GetPixel(x, y);

                    // Extrayendo el componente ARGB del pixel
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    // Encontrando promedio
                    int avg = (r + g + b) / 3;

                    // Asigando nuevo valor al pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                }
            }

            //load grayscale image in picturebox2
            pnl.BackgroundImage = bmp;
        }

        // Método para habilitar y deshabilitar los botones
        void enableButtons()
        {
            // Verificando si hay o no registros
            if (dgvConsultas.RowCount == 0)
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_File1;
                grayScale(pnlEdit, Properties.Resources.Edit_File);
                grayScale(pnlDelete, Properties.Resources.Delete1);
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }
            else
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = true;
                pnlDelete.Enabled = true;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_File1;
                pnlEdit.BackgroundImage = Properties.Resources.Edit_File;
                pnlDelete.BackgroundImage = Properties.Resources.Delete1;
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }

            // Verificando si se está agregando
            if (adding == true || editing == true)
            {
                pnlAdd.Enabled = false;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = true;
                pnlCancel.Enabled = true;
                grayScale(pnlAdd, Properties.Resources.Add_File1);
                grayScale(pnlEdit, Properties.Resources.Edit_File);
                grayScale(pnlDelete, Properties.Resources.Delete1);
                pnlSave.BackgroundImage = Properties.Resources.Save;
                pnlCancel.BackgroundImage = Properties.Resources.Cancel;
            }
        }

        // Método para verificar si existen registros
        Boolean existsRows()
        {
            Boolean estado = false;
            if (dgvConsultas.Rows.Count > 0)
                estado = true;
            return estado;
        }

        // Método para resetear controles
        void reset()
        {
            cmbTipoAsegurado.SelectedIndex = 0;
            cmbTipoConsulta.SelectedIndex = 0;
            cmbResultadoConsulta.SelectedIndex = 0;
            txtFecha.Clear();
            txtNoAfiliacion.Clear();
            txtEdad.Clear();
            txtSexo.Clear();
            rdbPrimera.Checked = true;
            rdbSubsecuente.Checked = false;
            txtSerie.Clear();
            rdbGenera.Checked = true;
            rdbNoGenera.Checked = false;
            txtNoExaLab.Clear();
            rdbRX.Checked = true;
            rdbOtros.Checked = false;
            txtMedicamento.Clear();
            txtDiagPrincipal.Clear();
            txtCodPrincipal.Clear();
            txtDiagSecundario.Clear();
            txtCodSec.Clear();
        }

        void send()
        {
            objCon.fecha = txtFecha.Text.Replace("'", "''");
            objCon.noAfiliacion = txtNoAfiliacion.Text;
            objCon.edad = Convert.ToInt32(txtEdad.Text);
            objCon.sexo = txtSexo.Text.Replace("'", "''");
            objCon.centroAtencion = "Centro";
            // ============= TIPO ASEGURADO ==============
            objCon.tipoAsegurado = cmbTipoAsegurado.SelectedIndex;
            // ============= TIPO CONSULTA ==============
            objCon.tipoConsulta = cmbTipoConsulta.SelectedIndex;
            // ============= TIPO =============
            if (rdbPrimera.Checked == true)
                objCon.tipo = "Primera";
            else if (rdbSubsecuente.Checked == true)
                objCon.tipo = "Subsecuente";
            objCon.serie = txtSerie.Text.Replace("'", "''");
            // ============= Subsidio =============
            if (rdbGenera.Checked == true)
                objCon.subsidio = "Genera";
            else if (rdbNoGenera.Checked == true)
                objCon.subsidio = "No genera";
            // ============= Examen Gabinete =============
            if (rdbRX.Checked == true)
                objCon.examGabinete = "RX";
            else if (rdbOtros.Checked == true)
                objCon.examGabinete = "Otros";
            objCon.noExamLab = txtNoExaLab.Text.Replace("'", "''");
            objCon.medicamentoPrescrito = txtMedicamento.Text.Replace("'", "''");
            // ============= Resultado =============
            objCon.resultadoConsulta = cmbResultadoConsulta.SelectedIndex;
            objCon.diagPrincipal = txtDiagPrincipal.Text.Replace("'", "''");
            objCon.codPrincipal = txtCodPrincipal.Text.Replace("'", "''");
            objCon.diagSecundario= txtDiagSecundario.Text.Replace("'", "''");
            objCon.codSecundario = txtCodSec.Text.Replace("'", "''");
        }

        // Método para cargar los datos a la clase Consulta
        void load()
        {
            objCon.idCon = Convert.ToInt32(dgvConsultas.SelectedRows[0].Cells[0].Value.ToString());
            objCon.fecha = dgvConsultas.SelectedRows[0].Cells[1].Value.ToString();
            objCon.noAfiliacion = dgvConsultas.SelectedRows[0].Cells[2].Value.ToString();
            objCon.edad = Convert.ToInt32(dgvConsultas.SelectedRows[0].Cells[3].Value.ToString());
            objCon.sexo = dgvConsultas.SelectedRows[0].Cells[4].Value.ToString();
            objCon.centroAtencion = dgvConsultas.SelectedRows[0].Cells[5].Value.ToString();
            // ============= TIPO CONSULTA ==============
            objCon.tipoConsulta = Convert.ToInt32(dgvConsultas.SelectedRows[0].Cells[6].Value.ToString());
            // ============= TIPO ASEGURADO ==============
            objCon.tipoAsegurado = Convert.ToInt32(dgvConsultas.SelectedRows[0].Cells[7].Value.ToString());
            // ============= TIPO =============
            if (dgvConsultas.SelectedRows[0].Cells[8].Value.ToString() == "Primera")
                objCon.tipo = "Primera";
            else if (dgvConsultas.SelectedRows[0].Cells[8].Value.ToString() == "Subsecuente")
                objCon.tipo = "Subsecuente";
            objCon.serie = dgvConsultas.SelectedRows[0].Cells[9].Value.ToString();
            // ============= Subsidio =============
            if (dgvConsultas.SelectedRows[0].Cells[10].Value.ToString() == "Genera")
                objCon.subsidio = "Genera";
            else if (dgvConsultas.SelectedRows[0].Cells[10].Value.ToString() == "No genera")
                objCon.subsidio = "No genera";
            objCon.noExamLab = dgvConsultas.SelectedRows[0].Cells[11].Value.ToString();
            // ============= Examen Gabinete =============
            if (dgvConsultas.SelectedRows[0].Cells[12].Value.ToString() == "RX")
                objCon.examGabinete = "RX";
            else if (dgvConsultas.SelectedRows[0].Cells[12].Value.ToString() == "Otros")
                objCon.examGabinete = "Otros";
            objCon.medicamentoPrescrito = dgvConsultas.SelectedRows[0].Cells[13].Value.ToString();
            // ============= Resultado =============
            objCon.resultadoConsulta = Convert.ToInt32(dgvConsultas.SelectedRows[0].Cells[14].Value.ToString());
            objCon.diagPrincipal = dgvConsultas.SelectedRows[0].Cells[15].Value.ToString();
            objCon.codPrincipal = dgvConsultas.SelectedRows[0].Cells[16].Value.ToString();
            objCon.diagSecundario = dgvConsultas.SelectedRows[0].Cells[17].Value.ToString();
            objCon.codSecundario = dgvConsultas.SelectedRows[0].Cells[18].Value.ToString();
        }

        // Método para habilitar y deshabilitar los controles
        void enableControls(Boolean option)
        {
            cmbTipoAsegurado.Enabled = option;
            cmbTipoConsulta.Enabled = option;
            cmbResultadoConsulta.Enabled = option;
            txtFecha.Enabled = option;
            txtNoAfiliacion.Enabled = option;
            txtEdad.Enabled = false;
            txtSexo.Enabled = false;
            rdbPrimera.Enabled = option;
            rdbSubsecuente.Enabled = option;
            txtSerie.Enabled = option;
            rdbGenera.Enabled = option;
            rdbNoGenera.Enabled = option;
            txtNoExaLab.Enabled = option;
            rdbRX.Enabled = option;
            rdbOtros.Enabled = option;
            txtMedicamento.Enabled = option;
            txtDiagPrincipal.Enabled = option;
            txtCodPrincipal.Enabled = option;
            txtDiagSecundario.Enabled = option;
            txtCodSec.Enabled = option;
            txtFecha.Focus();
        }

        // Método para leer los atributos de la clase Consulta y mostrarlos en controles
        void read()
        {
            txtFecha.Text = objCon.fecha;
            txtNoAfiliacion.Text = objCon.noAfiliacion;
            txtEdad.Text = objCon.edad.ToString();
            txtSexo.Text = objCon.sexo;
            // ============= TIPO ASEGURADO ==============
            cmbTipoAsegurado.SelectedIndex = objCon.tipoAsegurado;
            // ============= TIPO CONSULTA ==============
            cmbTipoConsulta.SelectedIndex = objCon.tipoConsulta;
            // ============= TIPO =============
            if (objCon.tipo == "Primera")
            {
                rdbPrimera.Checked = true;
                rdbSubsecuente.Checked = false;
            }
            else if (objCon.tipo == "Subsecuente")
            {
                rdbSubsecuente.Checked = true;
                rdbPrimera.Checked = false;
            }
            txtSerie.Text = objCon.serie;
            // ============= Subsidio =============
            if (objCon.subsidio == "Genera")
            {
                rdbGenera.Checked = true;
                rdbNoGenera.Checked = false;
            }
            else if (objCon.subsidio == "No genera")
            {
                rdbNoGenera.Checked = true;
                rdbGenera.Checked = false;
            }
            // ============= Examen Gabinete =============
            if (objCon.examGabinete == "RX")
            {
                rdbRX.Checked = true;
                rdbOtros.Checked = false;
            }
            else if (objCon.examGabinete == "Otros")
            {
                rdbOtros.Checked = true;
                rdbRX.Checked = false;
            }
            txtNoExaLab.Text = objCon.noExamLab;
            txtMedicamento.Text = objCon.medicamentoPrescrito;
            // ======================== Resultado ==========================
            cmbResultadoConsulta.SelectedIndex = objCon.resultadoConsulta;
            txtDiagPrincipal.Text = objCon.diagPrincipal;
            txtCodPrincipal.Text = objCon.codPrincipal;
            txtDiagSecundario.Text = objCon.diagSecundario;
            txtCodSec.Text = objCon.codSecundario;
        }

        #endregion

        #region CRUD
        void add()
        {
            adding = true;
            editing = false;
            reset();
            enableControls(true);
            enableButtons();
            dgvConsultas.Enabled = false;
            tbcConsultas.SelectedIndex = 1;
            String fecha = DateTime.Today.ToString("dd/MM/yyyy");
            txtFecha.Text = fecha;
        }

        void edit()
        {
            try
            {
                if (isValid())
                {
                    adding = false;
                    editing = true;
                    enableControls(true);
                    dgvConsultas.Enabled = false;
                    tbcConsultas.SelectedIndex = 1;
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Selecione un registro para editar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcConsultas.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void delete()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea eliminar el registro de la consulta?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        if (objCon.crudC("delete") > 0)
                        {
                            frmConf = new FrmConfirmDialog("Mensaje", "Registro de consulta eliminado correctamente.", "Ok", "No", "info");
                            frmConf.ShowDialog();
                        }
                        else
                        {
                            frmConf = new FrmConfirmDialog("Error", "Error al eliminar el registro de la consulta.", "Ok", "No", "error");
                            frmConf.ShowDialog();
                        }
                        all();
                        reset();
                        tbcConsultas.SelectedIndex = 0;
                    }
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione un registro para eliminar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcConsultas.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void save()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea guardar el registro de la consulta?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        send();
                        if (adding == true && editing == false) // Si se está Ingresando Datos
                        {
                            if (objCon.crudC("save") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Registro de consulta guardado correctamente.", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcConsultas.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al guardar el registro de la consulta.", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        else if (adding == false && editing == true) // Si se está Modificando Datos
                        {
                            if (objCon.crudC("update") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Registro de consulta editado correctamente", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcConsultas.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al editar el registro de la consulta", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        adding = false;
                        editing = false;
                        reset();
                        enableControls(false);
                        dgvConsultas.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación. ", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void cancel()
        {
            adding = false;
            editing = false;
            reset();
            enableButtons();
            enableControls(false);
            epError.Clear();
            dgvConsultas.Enabled = true;
            tbcConsultas.SelectedIndex = 0;
        }

        #endregion

        public FrmConsultas()
        {
            InitializeComponent();
        }

        public FrmConsultas(String active, Int32 type)
        {
            InitializeComponent();
            this.userActive = active;
            this.userType = type;
        }

        private void FrmConsultas_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            all();
            reset();
            enableControls(false);
            enableButtons();
            if (!existsRows())
                dgvConsultas.Enabled = false;
            data();
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            objMain = new FrmMain(userActive, userType);
            if (!adding && !editing)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana consultas y volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Salir", "Todos los datos no guardados se perderan.\n¿Seguro que desea volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    objMain.Show();
                    this.Close();
                }
            }
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(100));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pnlMinimize_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(35, 35, 35), new TransitionType_Linear(100));
        }

        private void pnlMinimize_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlAdd_Click(object sender, EventArgs e)
        {
            add();
        }

        private void pnlAdd_MouseHover(object sender, EventArgs e)
        {
            pnlAdd.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlAdd_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlAdd, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlEdit_Click(object sender, EventArgs e)
        {
            edit();
        }

        private void pnlEdit_MouseHover(object sender, EventArgs e)
        {
            pnlEdit.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlEdit_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlEdit, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlDelete_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void pnlDelete_MouseHover(object sender, EventArgs e)
        {
            pnlDelete.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlDelete_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlDelete, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void pnlSave_MouseHover(object sender, EventArgs e)
        {
            pnlSave.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlSave_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSave, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlCancel_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void pnlCancel_MouseHover(object sender, EventArgs e)
        {
            pnlCancel.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlCancel_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlCancel, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void txtNoAfiliacion_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNoAfiliacion.Text))
            {
                objCon.selectPersona(txtNoAfiliacion.Text.Replace("'", "''"));
                this.txtEdad.Text = objCon.edad.ToString();
                this.txtSexo.Text = objCon.sexo;
            }
            else
            {
                this.txtEdad.Text = "";
                this.txtSexo.Text = "";
            }
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            // e.DrawBorder();
            e.DrawText();
        }

        private void dgvConsultas_Click(object sender, EventArgs e)
        {
            if (dgvConsultas.Rows.Count > 0)
            {
                reset();
                load();
                read();
            }
            else
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "Debe haber al menos un registro para continuar.", "Ok", "No", "info");
                frmConf.ShowDialog();
            }
        }

        private void dgvConsultas_DoubleClick(object sender, EventArgs e)
        {
            tbcConsultas.SelectedIndex = 1;
        }

        private void txtDiagPrincipal_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDiagPrincipal.Text))
            {
                objCon.autoCompletePrinc(txtDiagPrincipal.Text.Replace("'", "''"));
                this.txtCodPrincipal.Text = objCon.codPrincipal;
            }
            else
            {
                this.txtCodPrincipal.Text = "";
            }
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            find(txtBuscar.Text.Replace("'", "''"));
            enableButtons();
        }

        private void txtDiagSecundario_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDiagSecundario.Text))
            {
                objCon.autoCompleteSec(txtDiagSecundario.Text.Replace("'", "''"));
                this.txtCodSec.Text = objCon.codSecundario;
            }
            else
            {
                this.txtCodSec.Text = "";
            }
        }

        private void pnlActualizar_Click(object sender, EventArgs e)
        {
            all();
            enableButtons();
        }

    }
}
