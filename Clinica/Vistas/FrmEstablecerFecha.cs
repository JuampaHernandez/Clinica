﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmEstablecerFecha : Form
    {
        #region Variables
        String reporte;
        #endregion

        #region Métodos de formulario
        Boolean isValid()
        {
            Boolean estado = true;
            String fecha = "17/";
            epError.Clear();
            try
            {
                if (String.IsNullOrEmpty(txtMes.Text))
                {
                    epError.SetError(txtMes, "Ingrese el mes del que desea generar el reporte");
                    estado = false;
                }
                if (String.IsNullOrEmpty(txtYear.Text))
                {
                    epError.SetError(txtYear, "Ingrese el año del que desea generar el reporte");
                    estado = false;
                }
                if (Convert.ToInt32(txtMes.Text) <= 0 || Convert.ToInt32(txtMes.Text) > 12)
                {
                    epError.SetError(txtMes, "Ingrese un mes valido (entre 01 y 12)");
                    estado = false;
                }
                else if (Convert.ToInt32(txtYear.Text) <= 2000 || Convert.ToInt32(txtYear.Text) > 2050)
                {
                    epError.SetError(txtYear, "Ingrese un año valido (entre 2000 y 2050)");
                    estado = false;
                }
                else
                {
                    fecha += txtMes.Text + "/" + txtYear.Text;
                    DateTime.Parse(fecha);
                }
            }
            catch (Exception)
            {
                estado = false;
            }
            return estado;
        }
        #endregion

        public FrmEstablecerFecha()
        {
            InitializeComponent();
        }

        public FrmEstablecerFecha(String rep)
        {
            InitializeComponent();
            this.reporte = rep;
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                if (reporte == "ConsultasMesEsp")
                {
                    FrmReporteConsultaMesEspecifico frmRepConEsp = new FrmReporteConsultaMesEspecifico(txtMes.Text, txtYear.Text);
                    this.Close();
                    frmRepConEsp.Show();
                }
                else if (reporte == "PlanC")
                {
                    FrmReportePlanC frmPlanC = new FrmReportePlanC(Convert.ToInt32(txtMes.Text), Convert.ToInt32(txtYear.Text));
                    this.Close();
                    frmPlanC.Show();
                }
                else if (reporte == "PlanA")
                {
                    FrmReportePlanA frmPlanA = new FrmReportePlanA(Convert.ToInt32(txtMes.Text), Convert.ToInt32(txtYear.Text));
                    this.Close();
                    frmPlanA.Show();
                }
            }
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            pnlSalir.BackColor = Color.FromArgb(183, 28, 28);
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void FrmEstablecerFecha_Load(object sender, EventArgs e)
        {

        }
    }
}
