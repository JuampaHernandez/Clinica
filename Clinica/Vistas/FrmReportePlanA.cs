﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Vistas
{
    public partial class FrmReportePlanA : Form
    {
        #region Variables
        int month, year;
        #endregion

        public FrmReportePlanA()
        {
            InitializeComponent();
        }

        public FrmReportePlanA(int mes, int an)
        {
            InitializeComponent();
            this.month = mes;
            this.year = an;
        }

        private void FrmReportePlanA_Load(object sender, EventArgs e)
        {
            this.spReporteEntregasPlanAMesTableAdapter.Fill(this.dtsEntregasPlanA.spReporteEntregasPlanAMes, this.month, this.year);
            this.reportViewer1.RefreshReport();
        }
    }
}
