﻿using Clinica.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Transitions;

namespace Clinica.Vistas
{
    public partial class FrmPlanificacionC : Form
    {
        #region Variables / Instancias
        private int move, x, y;
        FrmMain frmMain;
        FrmConfirmDialog frmConf;
        EntregasPlanC objEntr = new EntregasPlanC();
        Boolean adding = false, editing = false;
        String userActive;
        Int32 userType;
        #endregion

        #region Métodos de formulario

        void all()
        {
            String sql = "EXEC spMostrarEntregas";
            dgvEntregas.DataSource = objEntr.all(sql);
            // Ocultando columnas
            dgvEntregas.Columns[0].Visible = false;
            dgvEntregas.Columns[3].Visible = false;
            dgvEntregas.Columns[4].Visible = false;
            lblTotalRegistros.Text = "Total de registros: " + dgvEntregas.Rows.Count;
        }

        // Método para buscar entregas
        void find(String value)
        {
            String sql = "EXEC spBuscarEntregas '" + value + "'";
            dgvEntregas.DataSource = objEntr.all(sql);
            // Ocultando columnas
            dgvEntregas.Columns[0].Visible = false;
            dgvEntregas.Columns[3].Visible = false;
            dgvEntregas.Columns[4].Visible = false;
            lblTotalRegistros.Text = "Total de registros: " + dgvEntregas.Rows.Count;
        }

        void radioButtonsState()
        {
            if (rdbInscripcion.Checked && (adding || editing))
                cmbTipo.Enabled = false;
            else if (rdbSubsecuente.Checked && (adding || editing))
                cmbTipo.Enabled = true;
        }

        // Metodo para cambiar imagen a escala de grises
        void grayScale(Panel pnl, Image img)
        {
            Bitmap bmp = new Bitmap(img);
            // Obteniendo dimension de imagen
            int width = bmp.Width;
            int height = bmp.Height;

            // Color del pixel
            Color p;

            // Escala de grises
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    // Obteniendo valor del pixel
                    p = bmp.GetPixel(x, y);

                    // Extrayendo el componente ARGB del pixel
                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    // Encontrando promedio
                    int avg = (r + g + b) / 3;

                    // Asigando nuevo valor al pixel
                    bmp.SetPixel(x, y, Color.FromArgb(a, avg, avg, avg));
                }
            }

            //load grayscale image in picturebox2
            pnl.BackgroundImage = bmp;
        }

        void enableButtons()
        {
            // Verificando si hay o no registros
            if (dgvEntregas.RowCount == 0)
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_File1;
                grayScale(pnlEdit, Properties.Resources.Edit_File);
                grayScale(pnlDelete, Properties.Resources.Delete1);
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }
            else
            {
                pnlAdd.Enabled = true;
                pnlEdit.Enabled = true;
                pnlDelete.Enabled = true;
                pnlSave.Enabled = false;
                pnlCancel.Enabled = false;
                pnlAdd.BackgroundImage = Properties.Resources.Add_File1;
                pnlEdit.BackgroundImage = Properties.Resources.Edit_File;
                pnlDelete.BackgroundImage = Properties.Resources.Delete1;
                grayScale(pnlSave, Properties.Resources.Save);
                grayScale(pnlCancel, Properties.Resources.Cancel);
            }

            // Verificando si se está agregando
            if (adding == true || editing == true)
            {
                pnlAdd.Enabled = false;
                pnlEdit.Enabled = false;
                pnlDelete.Enabled = false;
                pnlSave.Enabled = true;
                pnlCancel.Enabled = true;
                grayScale(pnlAdd, Properties.Resources.Add_File1);
                grayScale(pnlEdit, Properties.Resources.Edit_File);
                grayScale(pnlDelete, Properties.Resources.Delete1);
                pnlSave.BackgroundImage = Properties.Resources.Save;
                pnlCancel.BackgroundImage = Properties.Resources.Cancel;
            }
        }

        // Método para verificar si existen registros
        Boolean existsRows()
        {
            Boolean estado = false;
            if (dgvEntregas.Rows.Count > 0)
                estado = true;
            return estado;
        }

        Boolean isValid()
        {
            Boolean estado = true;
            epError.Clear();
            if (cmbTipo.SelectedIndex == 0)
            {
                if (rdbSubsecuente.Checked)
                {
                    epError.SetError(cmbTipo, "Seleccione el tipo");
                    estado = false;
                }
            }
            if (cmbMetodo.SelectedIndex == 0)
            {
                epError.SetError(cmbMetodo, "Seleccione un método");
                estado = false;
            }
            if (String.IsNullOrEmpty(txtFecha.Text))
            {
                epError.SetError(txtFecha, "Ingrese la fecha de la entrega");
                estado = false;
            }
            else
            {
                try
                {
                    String fecha = Convert.ToDateTime(txtFecha.Text).ToString("dd/MM/yyyy");
                    Convert.ToDateTime(fecha);
                }
                catch
                {
                    epError.SetError(txtFecha, "Ingrese una fecha correcta");
                    estado = false;
                }
            }
            if (String.IsNullOrEmpty(txtNoAfiliacion.Text))
            {
                epError.SetError(txtNoAfiliacion, "Ingrese el N° de afiliación del paciente");
                estado = false;
            }
            else if (String.IsNullOrEmpty(txtNombres.Text) || String.IsNullOrWhiteSpace(txtApellidos.Text))
            {
                epError.SetError(txtNoAfiliacion, "Ingrese un N° de afiliación valido");
                estado = false;
            }
            if (String.IsNullOrWhiteSpace(txtResp.Text))
            {
                epError.SetError(txtResp, "Ingrese el nombres del responsable");
                estado = false;
            }
            return estado;
        }

        // Método para resetear controles
        void reset()
        {
            cmbMetodo.SelectedIndex = 0;
            cmbTipo.SelectedIndex = 0;
            txtFecha.Clear();
            txtNoAfiliacion.Clear();
            txtNombres.Clear();
            txtApellidos.Clear();
            rdbInscripcion.Checked = true;
            rdbSubsecuente.Checked = false;
            txtResp.Clear();
            txtFecha.Clear();
        }

        void send()
        {
            objEntr.fecha = txtFecha.Text.Replace("'", "''");
            objEntr.noAfiliacion = txtNoAfiliacion.Text;
            objEntr.nombres = txtNombres.Text.Replace("'", "''");
            objEntr.apellidos = txtApellidos.Text.Replace("'", "''");
            if (rdbInscripcion.Checked == true)
            {
                objEntr.tipoConsulta = "Inscripcion";
                objEntr.tipo = "N/A";
            }
            else if (rdbSubsecuente.Checked == true)
            {
                objEntr.tipoConsulta = "Subsecuente";
                objEntr.tipo = cmbTipo.Text;
            }
            objEntr.metodo = cmbMetodo.Text.Replace("'", "''");
            objEntr.responsable = txtResp.Text.Replace("'", "''");
        }

        // Método para cargar los datos a la clase Entregas
        void load()
        {
            objEntr.id = Convert.ToInt32(dgvEntregas.SelectedRows[0].Cells[0].Value.ToString());
            objEntr.fecha = dgvEntregas.SelectedRows[0].Cells[1].Value.ToString();
            objEntr.noAfiliacion = dgvEntregas.SelectedRows[0].Cells[2].Value.ToString();
            objEntr.nombres = dgvEntregas.SelectedRows[0].Cells[3].Value.ToString();
            objEntr.apellidos = dgvEntregas.SelectedRows[0].Cells[4].Value.ToString();
            if (dgvEntregas.SelectedRows[0].Cells[5].Value.ToString() == "Inscripcion")
                objEntr.tipoConsulta = "Inscripcion";
            else if (dgvEntregas.SelectedRows[0].Cells[5].Value.ToString() == "Subsecuente")
                objEntr.tipoConsulta = "Subsecuente";
            objEntr.tipo = dgvEntregas.SelectedRows[0].Cells[6].Value.ToString();
            objEntr.metodo = dgvEntregas.SelectedRows[0].Cells[7].Value.ToString();
            objEntr.responsable = dgvEntregas.SelectedRows[0].Cells[8].Value.ToString();
        }

        // Método para habilitar y deshabilitar los controles
        void enableControls(Boolean option)
        {
            cmbTipo.Enabled = option;
            cmbMetodo.Enabled = option;
            txtFecha.Enabled = option;
            txtNoAfiliacion.Enabled = option;
            txtNombres.Enabled = false;
            txtApellidos.Enabled = false;
            rdbInscripcion.Enabled = option;
            rdbSubsecuente.Enabled = option;
            txtResp.Enabled = option;
            txtFecha.Focus();
        }

        // Método para leer los atributos de la clase entrega y mostrarlos en controles
        void read()
        {
            txtFecha.Text = objEntr.fecha;
            txtNoAfiliacion.Text = objEntr.noAfiliacion;
            txtNombres.Text = objEntr.nombres;
            txtApellidos.Text = objEntr.apellidos;
            if (objEntr.tipoConsulta == "Inscripcion")
            {
                rdbInscripcion.Checked = true;
                rdbSubsecuente.Checked = false;
            }
            else if (objEntr.tipoConsulta == "Subsecuente")
            {
                rdbInscripcion.Checked = false;
                rdbSubsecuente.Checked = true;
            }
            // --------------------------
            if (objEntr.tipo == "Normal")
                cmbTipo.SelectedIndex = 1;
            else if (objEntr.tipo == "Por morbilidad")
                cmbTipo.SelectedIndex = 2;
            else if (objEntr.tipo == "Por falla")
                cmbTipo.SelectedIndex = 3;
            else if (objEntr.tipo == "N/A")
                cmbTipo.SelectedIndex = 0;
            // --------------------------
            if (objEntr.metodo == "Oral alta")
                cmbMetodo.SelectedIndex = 1;
            else if (objEntr.metodo == "Oral baja")
                cmbMetodo.SelectedIndex = 2;
            else if (objEntr.metodo == "DIUS")
                cmbMetodo.SelectedIndex = 3;
            else if (objEntr.metodo == "Inyect. Mensual")
                cmbMetodo.SelectedIndex = 4;
            else if (objEntr.metodo == "Inyect. Trimestral")
                cmbMetodo.SelectedIndex = 5;
            else if (objEntr.metodo == "Esteril")
                cmbMetodo.SelectedIndex = 6;
            else if (objEntr.metodo == "Ninguno")
                cmbMetodo.SelectedIndex = 7;

            txtResp.Text = objEntr.responsable;
        }

        #endregion

        #region CRUD
        void add()
        {
            adding = true;
            editing = false;
            reset();
            enableControls(true);
            radioButtonsState();
            enableButtons();
            dgvEntregas.Enabled = false;
            tbcEntregas.SelectedIndex = 1;
            String fecha = DateTime.Today.ToString("dd/MM/yyyy");
            txtFecha.Text = fecha;
        }

        void edit()
        {
            try
            {
                if (isValid())
                {
                    adding = false;
                    editing = true;
                    enableControls(true);
                    dgvEntregas.Enabled = false;
                    radioButtonsState();
                    tbcEntregas.SelectedIndex = 1;
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Selecione un registro para editar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcEntregas.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void delete()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea eliminar el registro de la entrega?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        if (objEntr.crud("delete") > 0)
                        {
                            frmConf = new FrmConfirmDialog("Mensaje", "Registro de entrega eliminado correctamente.", "Ok", "No", "info");
                            frmConf.ShowDialog();
                        }
                        else
                        {
                            frmConf = new FrmConfirmDialog("Error", "Error al eliminar el registro de la entrega.", "Ok", "No", "error");
                            frmConf.ShowDialog();
                        }
                        all();
                        reset();
                        tbcEntregas.SelectedIndex = 0;
                    }
                }
                else
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Alerta", "Seleccione un registro para eliminar.", "Ok", "No", "alert");
                    frmConf.ShowDialog();
                    epError.Clear();
                    tbcEntregas.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void save()
        {
            try
            {
                if (isValid())
                {
                    FrmConfirmDialog frmConf = new FrmConfirmDialog("Mensaje", "¿Seguro que desea guardar el registro de la entrega?", "Si", "No", "question");
                    if (frmConf.ShowDialog() == DialogResult.OK)
                    {
                        send();
                        if (adding == true && editing == false) // Si se está Ingresando Datos
                        {
                            if (objEntr.crud("save") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Registro de entrega guardado correctamente.", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcEntregas.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al guardar el registro de la entrega.", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        else if (adding == false && editing == true) // Si se está Modificando Datos
                        {
                            if (objEntr.crud("update") > 0)
                            {
                                frmConf = new FrmConfirmDialog("Mensaje", "Registro de entrega editado correctamente", "Ok", "No", "info");
                                frmConf.ShowDialog();
                                tbcEntregas.SelectedIndex = 0;
                            }
                            else
                            {
                                frmConf = new FrmConfirmDialog("Error", "Error al editar el registro de la entrega", "Ok", "No", "error");
                                frmConf.ShowDialog();
                            }
                            all();
                        }
                        adding = false;
                        editing = false;
                        reset();
                        enableControls(false);
                        dgvEntregas.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Error al realizar la operación.", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                epError.Clear();
            }
            finally
            {
                enableButtons();
            }
        }

        void cancel()
        {
            adding = false;
            editing = false;
            reset();
            enableButtons();
            enableControls(false);
            epError.Clear();
            dgvEntregas.Enabled = true;
            tbcEntregas.SelectedIndex = 0;
        }

        #endregion

        public FrmPlanificacionC()
        {
            InitializeComponent();
        }

        public FrmPlanificacionC(String active, Int32 type)
        {
            InitializeComponent();
            this.userActive = active;
            this.userType = type;
        }

        private void tltAyuda_Draw(object sender, DrawToolTipEventArgs e)
        {
            e.DrawBackground();
            e.DrawText();
        }

        private void FrmPlanificacionA_Load(object sender, EventArgs e)
        {
            Transition.run(pnlTitulo, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(2500));
            all();
            reset();
            enableControls(false);
            enableButtons();
            if (!existsRows())
                dgvEntregas.Enabled = false;
        }

        private void pnlSalir_Click(object sender, EventArgs e)
        {
            frmMain = new FrmMain(userActive, userType);
            if (!adding && !editing)
            {
                frmConf = new FrmConfirmDialog("Salir", "¿Desea salir de la ventana entregas y volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    frmMain.Show();
                    this.Close();
                }
            }
            else
            {
                frmConf = new FrmConfirmDialog("Salir", "Todos los datos no guardados se perderan.\n¿Seguro que desea volver al menú?", "Si", "No", "question");
                if (frmConf.ShowDialog() == DialogResult.OK)
                {
                    frmMain.Show();
                    this.Close();
                }
            }
        }

        private void pnlSalir_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(183, 28, 28), new TransitionType_Linear(100));
        }

        private void pnlSalir_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSalir, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void pnlMinimize_MouseHover(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(35, 35, 35), new TransitionType_Linear(100));
        }

        private void pnlMinimize_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlMinimize, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(200));
        }

        private void pnlAdd_Click(object sender, EventArgs e)
        {
            add();
        }

        private void pnlEdit_Click(object sender, EventArgs e)
        {
            edit();
        }

        private void pnlDelete_Click(object sender, EventArgs e)
        {
            delete();
        }

        private void pnlSave_Click(object sender, EventArgs e)
        {
            save();
        }

        private void pnlCancel_Click(object sender, EventArgs e)
        {
            cancel();
        }

        private void pnlAdd_MouseHover(object sender, EventArgs e)
        {
            pnlAdd.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlAdd_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlAdd, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlEdit_MouseHover(object sender, EventArgs e)
        {
            pnlEdit.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlEdit_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlEdit, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlDelete_MouseHover(object sender, EventArgs e)
        {
            pnlDelete.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlDelete_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlDelete, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlSave_MouseHover(object sender, EventArgs e)
        {
            pnlSave.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlSave_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlSave, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void pnlCancel_MouseHover(object sender, EventArgs e)
        {
            pnlCancel.BackColor = Color.FromArgb(35, 35, 35);
        }

        private void pnlCancel_MouseLeave(object sender, EventArgs e)
        {
            Transition.run(pnlCancel, "BackColor", Color.FromArgb(28, 28, 28), new TransitionType_Linear(300));
        }

        private void dgvEntregas_Click(object sender, EventArgs e)
        {
            if (dgvEntregas.Rows.Count > 0)
            {
                reset();
                load();
                read();
            }
            else
            {
                frmConf = new FrmConfirmDialog("Mensaje", "Debe haber al menos un registro para continuar.", "Ok", "No", "info");
                frmConf.ShowDialog();
            }
        }

        private void rdbSubsecuente_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonsState();
        }

        private void rdbInscripcion_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonsState();
        }

        private void txtNoAfiliacion_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNoAfiliacion.Text))
            {
                objEntr.selectPersona(txtNoAfiliacion.Text.Replace("'", "''"));
                this.txtNombres.Text = objEntr.nombres;
                this.txtApellidos.Text = objEntr.apellidos;
            }
            else
            {
                this.txtNombres.Text = "";
                this.txtApellidos.Text = "";
            }
        }

        private void dgvEntregas_DoubleClick(object sender, EventArgs e)
        {
            tbcEntregas.SelectedIndex = 1;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            find(txtBuscar.Text.Replace("'", "''"));
            enableButtons();
        }

        private void pnlTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            move = 1;
            x = e.X;
            y = e.Y;
        }

        private void pnlTitulo_MouseUp(object sender, MouseEventArgs e)
        {
            move = 0;
        }

        private void pnlTitulo_MouseMove(object sender, MouseEventArgs e)
        {
            if (move == 1)
                SetDesktopLocation(MousePosition.X - x, MousePosition.Y - y);
        }

    }
}
