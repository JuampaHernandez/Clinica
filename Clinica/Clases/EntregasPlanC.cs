﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Clases
{
    class EntregasPlanC
    {
        public int id { get; set; }
        public int idPaciente { get; set; }
        public String noAfiliacion { get; set; }
        public String nombres { get; set; }
        public String apellidos { get; set; }
        public String fecha { get; set; }
        public String tipoConsulta { get; set; }
        public String tipo { get; set; }
        public String metodo { get; set; }
        public String responsable { get; set; }

        public DataTable all(String sql)
        {
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            DataTable datos = new DataTable();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                adapter.Fill(datos);
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return datos;
        }

        public int crud(String action)
        {
            int estado = 0;
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "Exec ";
                if (action == "save")
                {
                    sql += "spAgregarEntrega '" + fecha + "', " + idPaciente + ", '" + tipoConsulta + "', '" + tipo;
                    sql += "', '" + metodo + "', '" + responsable + "';";
                }
                else if (action == "update")
                {
                    sql += "spModificarEntrega " + id + ", '" + fecha + "', " + idPaciente + ", '" + tipoConsulta + "', '" + tipo;
                    sql += "', '" + metodo + "', '" + responsable + "';";
                }
                else if (action == "delete")
                    sql += "spEliminarEntrega " + id;
                SqlCommand cmd = new SqlCommand(sql, con);
                estado = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return estado;
        } // Fin crud

        public void selectPersona(String numA)
        {
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "EXEC spSelectPacienteDataEntrega '" + numA + "'";
                SqlCommand Comando = new SqlCommand(sql, con);
                SqlDataReader reader = Comando.ExecuteReader();
                if (reader.Read())
                {
                    this.idPaciente = Convert.ToInt32(reader[0]);
                    this.nombres = Convert.ToString(reader[1]);
                    this.apellidos = Convert.ToString(reader[2]);
                }
                else
                {
                    this.idPaciente = 0;
                    this.nombres = "";
                    this.apellidos = "";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
