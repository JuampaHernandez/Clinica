﻿using Clinica.Vistas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Clases
{
    class Recordatorio
    {
        public int id { get; set; }
        public String titulo { get; set; }
        public String descripcion { get; set; }
        public String fecha { get; set; }
        public String estado { get; set; }

        public Recordatorio()
        {
            this.id = 0;
            this.titulo = "Titulo";
            this.descripcion = "Descripcion";
            this.fecha = "31/12/2999";
            this.estado = "Pendiente";
        }

        // Verifica si hay actividades pendientes
        public Boolean pending(ArrayList fechas)
        {
            Boolean estado = false;
            CultureInfo provider = CultureInfo.InvariantCulture;
            foreach (var fecha in fechas)
            {
                if (DateTime.Compare(DateTime.Parse(fecha.ToString()), DateTime.Parse(Convert.ToString(DateTime.Today))) > 0)
                    estado = true;
            }
            return estado;
        }

        // Verifica si hay actividades retrasadas
        public Boolean late(ArrayList fechas)
        {
            Boolean estado = false;
            CultureInfo provider = CultureInfo.InvariantCulture;
            foreach (var fecha in fechas)
            {
                if (DateTime.Compare(DateTime.Parse(fecha.ToString()), DateTime.Parse(DateTime.Today.ToShortDateString())) < 0)
                    estado = true;
            }
            return estado;
        }

        public int crud(String action)
        {
            int state = 0;
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "EXEC ";
                if (action == "save")
                    sql += "spAgregarRecordatorio '" + titulo + "', '" + descripcion + "', '" + fecha + "', '" + estado + "';";
                else if (action == "update")
                    sql += "spModificarRecordatorio " + id + ", '" + titulo + "', '" + descripcion + "', '" + fecha + "', '" + estado + "';";
                else if (action == "delete")
                    sql += "spEliminarRecordatorio " + id;
                SqlCommand cmd = new SqlCommand(sql, con);
                state = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Ocurrió un error al realizar la operación.", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
            return state;
        } // Fin Gestionar

        public ArrayList getFechas()
        {
            ArrayList fechas = new ArrayList();
            Conexion objCon = new Conexion();
            try
            {
                SqlConnection con = objCon.conectar();
                String sql = "Exec spGetFechas;";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())
                        fechas.Add(rdr[0]);
                }
                con.Close();
            }
            catch (Exception ex)
            {
                FrmConfirmDialog frmConf = new FrmConfirmDialog("Error", "Ocurrió un error al realizar la operación.", "Ok", "No", "error");
                frmConf.ShowDialog();
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
            return fechas;
        }

        public DataTable all(String sql)
        {
            verify();
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            DataTable datos = new DataTable();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                adapter.Fill(datos);
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return datos;
        }

        // Función para actualizar automaticamente el estado de los recordatorios
        public void verify()
        {
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            String sql = "EXEC spMostrarRecordatorios;";
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (DateTime.Compare(DateTime.Parse(Convert.ToString(reader[3])), DateTime.Parse(Convert.ToString(DateTime.Today))) > 0 && reader[4].ToString() != "Realizado")
                    {
                        id = Convert.ToInt32(reader[0]);
                        titulo = reader[1].ToString();
                        descripcion = reader[2].ToString();
                        fecha = reader[3].ToString();
                        estado = "Pendiente";
                        crud("update");
                    }
                    else if(DateTime.Compare(DateTime.Parse(Convert.ToString(reader[3])), DateTime.Parse(Convert.ToString(DateTime.Today))) < 0 && reader[4].ToString() != "Realizado")
                    {
                        id = Convert.ToInt32(reader[0]);
                        titulo = reader[1].ToString();
                        descripcion = reader[2].ToString();
                        fecha = reader[3].ToString();
                        estado = "Retrasado";
                        crud("update");
                    }
                }
            }
        }

    }
}
