﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Clases
{
    class Consulta : Paciente
    {
        public int idCon { get; set; }
        public int tipoAsegurado { get; set; }
        public int tipoConsulta { get; set; }
        public String tipo { get; set; }
        public String serie { get; set; }
        public String subsidio { get; set; }
        public String noExamLab { get; set; }
        public String examGabinete { get; set; }
        public String medicamentoPrescrito { get; set; }
        public int resultadoConsulta { get; set; }
        public String diagPrincipal { get; set; }
        public String codPrincipal { get; set; }
        public String diagSecundario { get; set; }
        public String codSecundario { get; set; }

        public Consulta()
        {
            // Nothing to do
        }

        public DataTable allC(String sql)
        {
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            DataTable datos = new DataTable();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                adapter.Fill(datos);
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return datos;
        }

        public AutoCompleteStringCollection data()
        {
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            AutoCompleteStringCollection datos = new AutoCompleteStringCollection();
            try
            {
                String sql = "EXECUTE spDataEnfermedades;";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                    datos.Add(Convert.ToString(reader[0]));
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return datos;
        }

        public void autoCompletePrinc(String val)
        {
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "EXECUTE spAutoComplete '" + val + "';";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    this.codPrincipal = Convert.ToString(reader[0]);
                }
                else
                {
                    this.codPrincipal = "";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void autoCompleteSec(String val)
        {
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "EXECUTE spAutoComplete '" + val + "';";
                SqlCommand cmd = new SqlCommand(sql, con);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    this.codSecundario = Convert.ToString(reader[0]);
                }
                else
                {
                    this.codSecundario = "";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Gestionar
        public int crudC(String action)
        {
            int estado = 0;
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "Exec ";
                if (action == "save")
                {
                    sql += "spAgregarConsulta '" + fecha + "', " + this.id + ", " + tipoConsulta + ", " + tipoAsegurado;
                    sql += ", '" + tipo + "', '" + serie + "', '" + subsidio;
                    sql += "', '" + noExamLab + "', '" + examGabinete + "', '" + medicamentoPrescrito + "', " + resultadoConsulta;
                    sql += ", '" + diagPrincipal + "', '" + codPrincipal + "', '" + diagSecundario + "', '" + codSecundario + "';";
                }
                else if (action == "update")
                {
                    sql += "spModificarConsulta " + idCon + ", '" + fecha + "', " + this.id + ", " + tipoConsulta + ", " + tipoAsegurado;
                    sql += ", '" + tipo + "', '" + serie + "', '" + subsidio;
                    sql += "', '" + noExamLab + "', '" + examGabinete + "', '" + medicamentoPrescrito + "', " + resultadoConsulta;
                    sql += ", '" + diagPrincipal + "', '" + codPrincipal + "', '" + diagSecundario + "', '" + codSecundario + "';";
                }
                else if (action == "delete")
                    sql += "spEliminarConsulta " + idCon;
                SqlCommand cmd = new SqlCommand(sql, con);
                estado = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
            return estado;
        } // Fin Gestionar

        public void selectPersona(String numA)
        {
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "EXEC spSelectPacienteData " + numA;
                SqlCommand Comando = new SqlCommand(sql, con);
                SqlDataReader reader = Comando.ExecuteReader();
                if (reader.Read())
                {
                    this.id = Convert.ToInt32(reader[0]);
                    this.edad = Convert.ToInt32(reader[1]);
                    this.sexo = Convert.ToString(reader[2]);
                    this.centroAtencion = Convert.ToString(reader[3]);
                }
                else
                {
                    this.id = 0;
                    this.edad = 00;
                    this.sexo = "";
                    this.centroAtencion = "";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
