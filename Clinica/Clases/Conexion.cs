﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Clinica.Clases
{
    class Conexion
    {
        public SqlConnection conectar()
        {
            SqlConnection conn = new SqlConnection(Properties.Settings.Default.Conexion);
            conn.Open();
            return conn;
        }
    }
}
