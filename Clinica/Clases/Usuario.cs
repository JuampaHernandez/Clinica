﻿using Clinica.Vistas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Clinica.Clases
{
    class Usuario
    {
        public int id { get; set; }
        public String user { get; set; }
        public String pass { get; set; }
        public int userType { get; set; }
        public int state { get; set; }
        public int idActive { get; set; }
        public String userActive { get; set; }
        public String passActive { get; set; }
        public int userTypeActive { get; set; }
        public int stateActive { get; set; }

        // Verificando que el usuario exista
        public Boolean verificarUsuario(String user, String pass)
        {
            Boolean estado = false;
            Conexion objCon = new Conexion();
            try
            {
                SqlConnection conn = objCon.conectar();
                SqlCommand Comando = new SqlCommand(string.Format("Exec spVerificarUsuario '{0}', '{1}';", user, pass), conn);
                SqlDataReader reader = Comando.ExecuteReader();
                if (reader.Read())
                {
                    this.idActive = Convert.ToInt32(reader[0]);
                    this.userActive = Convert.ToString(reader[1]);
                    this.passActive = Convert.ToString(reader[2]);
                    this.userTypeActive = Convert.ToInt32(reader[3]);
                    this.stateActive = Convert.ToInt32(reader[4]);
                    estado = true;
                }
                conn.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Error al ejecutar la consulta: " + e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return estado;
        } // Fin de verificarUsuario()

        // Verificando el tipo de usuario
        public Boolean verificarTipoUsuario(String user, String pass) // Verificar Tipo Usuario
        {
            Boolean estado = false;
            Conexion objCon = new Conexion();
            try
            {
                SqlConnection conn = objCon.conectar();
                SqlCommand cmd = new SqlCommand(string.Format("Exec spVerificarTipoUsuario '{0}', '{1}'", user, pass), conn);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    estado = true;
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al ejecutar la consulta: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return estado;
        } // Fin de verificarTipoUsuario()

        // Verificando que el usuario este activo
        public Boolean verificarEstadoUsuario(String user, String pass) // Verificar Tipo Usuario
        {
            Boolean estado = false;
            Conexion objCon = new Conexion();
            try
            {
                SqlConnection conn = objCon.conectar();
                SqlCommand cmd = new SqlCommand(string.Format("Exec spVerificarEstadoUsuario '{0}', '{1}'", user, pass), conn);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    estado = true;
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al ejecutar la consulta: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return estado;
        } // Fin de verificarEstadoUsuario()

        public DataTable all(String sql)
        {
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            DataTable datos = new DataTable();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                adapter.Fill(datos);
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return datos;
        }

        public int crud(String action)
        {
            int state = 0;
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            String sql = "EXECUTE ";
            if (action == "save")
                sql += "spAgregarUsuario '" + user + "', '" + pass + "', " + userType + ", " + this.state + ";";
            else if (action == "update")
                sql += "spModificarUsuario " + id + ",  '" + user + "', '" + pass + "', " + userType + ", " + this.state + ";";
            else if (action == "delete")
                sql += "spEliminarUsuario " + id + ";";
            SqlCommand cmd = new SqlCommand(sql, con);
            state = cmd.ExecuteNonQuery();
            con.Close();
            return state;
        } // Fin CRUD
    }
}
