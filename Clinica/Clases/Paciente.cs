﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clinica.Clases
{
    class Paciente
    {
        public int id { get; set; }
        public String dui { get; set; }
		public String noAfiliacion { get; set; }
		public String nombres { get; set; }
		public String apellidos { get; set; }
		public String centroAtencion { get; set; }
		public String fecha { get; set; }
		public String calidad { get; set; }
		public String sexo { get; set; }
		public String estadoCivil { get; set; }
        public int edad { get; set; }
		public String fechaNacimiento { get; set; }
		public String lugarNacimiento { get; set; }
		public String direccionDomicilio { get; set; }
		public String telDomicilio { get; set; }
		public String direccionTrabajo { get; set; }
		public String telTrabajo { get; set; }
		public String ocupacion { get; set; }
		public String patrono { get; set; }
		public String actividadEconomica { get; set; }
		public String nombrePadre { get; set; }
		public String nombreMadre { get; set; }
		public String nombreConyugue { get; set; }
		public String nombreEmergencia { get; set; }
		public String telEmergencia { get; set; }
		public String proporcionadorDatos { get; set; }
		public String archivista { get; set; }
        public int estado { get; set; }

        public DataTable all(String sql)
        {
            Conexion objCon = new Conexion();
            SqlConnection con = objCon.conectar();
            DataTable datos = new DataTable();
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, con);
                adapter.Fill(datos);
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió el siguiente error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return datos;
        }

        // Gestionar
        public int crud(String action)
        {
            int estado = 0;
            try
            {
                Conexion objCon = new Conexion();
                SqlConnection con = objCon.conectar();
                String sql = "Exec ";
                if (action == "save")
                {
                    sql += "spAgregarPaciente '" + dui + "', '" + noAfiliacion + "', '" + nombres + "', '" + apellidos;
                    sql += "', '" + centroAtencion + "', '" + fecha + "', '" + calidad + "', '" + sexo;
                    sql += "', '" + estadoCivil + "', " + edad + ", '" + fechaNacimiento + "', '" + lugarNacimiento;
                    sql += "', '" + direccionDomicilio + "', '" + telDomicilio + "', '" + direccionTrabajo + "', '" + telTrabajo;
                    sql += "', '" + ocupacion + "', '" + patrono + "', '" + actividadEconomica + "', '" + nombrePadre;
                    sql += "', '" + nombreMadre + "', '" + nombreConyugue + "', '" + nombreEmergencia + "', '" + telEmergencia;
                    sql += "', '" + proporcionadorDatos + "', '" + archivista + "';";
                }
                else if (action == "update")
                {
                    sql += "spModificarPaciente " + id + ", '" + dui + "', '" + noAfiliacion + "', '" + nombres + "', '" + apellidos;
                    sql += "', '" + centroAtencion + "', '" + fecha + "', '" + calidad + "', '" + sexo;
                    sql += "', '" + estadoCivil + "', " + edad + ", '" + fechaNacimiento + "', '" + lugarNacimiento;
                    sql += "', '" + direccionDomicilio + "', '" + telDomicilio + "', '" + direccionTrabajo + "', '" + telTrabajo;
                    sql += "', '" + ocupacion + "', '" + patrono + "', '" + actividadEconomica + "', '" + nombrePadre;
                    sql += "', '" + nombreMadre + "', '" + nombreConyugue + "', '" + nombreEmergencia + "', '" + telEmergencia;
                    sql += "', '" + proporcionadorDatos + "', '" + archivista + "';";
                }
                else if (action == "null")
                    sql += "spAnularPaciente " + id;
                else if (action == "delete")
                    sql += "spEliminarPaciente " + id;
                else if (action == "reactivate")
                    sql += "spReactivarPaciente " + id;
                SqlCommand cmd = new SqlCommand(sql, con);
                estado = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al ejecutar la consulta: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
            return estado;
        } // Fin Gestionar
    }
}
