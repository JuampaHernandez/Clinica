# Sistema para gestion de consultas

## Clinica Grupo Miguel

- [ScreenShots](#screenshots)
- [Descripción](#descripción)
- [Configuración](#configuración)
- [Ejecutar](#ejecutar)
- [About](#about)

---

**Pendientes**
- [ ] Screenshots
- [ ] Instrucciones

## ScreenShots

<!-- ![SS01](/img/ "SplashScreen") -->
<!-- ![SS01](/img/ "SplashScreen") -->
<!-- ![SS01](/img/ "SplashScreen") -->
<!-- ![SS01](/img/ "SplashScreen") -->

## Descripción

Sistema para clinica empresarial de Grupo Miguel.

Desarrollado en Visual Studio 2013 con el lenguaje de programación C# y base de datos en SQL Server 2014.
El sistema hace uso de la dependecia _Transitions.dll_ para efectos fade en colores y botones del formulario.

Diseño _material_ para formularios, formulario individual para el menú y exportación de reportes a PDF, Word y Excel.

Incluye volcado de +14,400 registros con el Código Internacional de Enfermedades (C.I.E.), descripción y grupo al que pertenecen.

## Configuración

Antes de empezar autilizar el sistema, debe asegurarse de estableccer la configuración correcta.

A continuación se presenta la lista de pasos a seguir para su correcto funcionamiento:

1. Buscar y abrir el archivo _`Clinica.exe.config`_ ubicado en el directorio [Clinica/bin/Release](Clinica/bin/Release/).

2. Editar el contenido de la etiqueta `<add>` en la linea [`6`](Clinica/bin/Release/Clinica.exe.config#L6) que se encuetra dentro de la etiqueta `<connectionStrings>`.

    Contenido por defecto:
    
    ``` xml
        <connectionStrings>
            <add name="Clinica.Properties.Settings.Conexion" connectionString="Data Source=LOCALHOST\DB;Initial Catalog=Clinica;User ID=sa;Password=12345"
            providerName="System.Data.SqlClient" />
        </connectionStrings>
    ```

3. En la opción `Data Source` escribir la IP o nombre del servidor SQL Server al que desea conectarse.

4. (Opcional) En la opción `Initial Catalog` escribir el nombre de la base de datos, si no desea cambiarlo omita este paso y el paso `8`.

5. En la opción `User ID` escriba el nombre de usuario con el que de sea iniciar sesión (Usuario del servidor SQL Server).

6. En la opción `Password` escriba la contraseña del usuario (Contraseña de usuario del servidor SQL Server).

    >Nota: Si utiliza la autenticación de windows para conectarse al servidor de SQL Server, omita el paso 5 y 6.  La cadena de conexión quedaría de la siguiente manera:
    
    ``` xml
        <connectionStrings>
            <add name="Clinica.Properties.Settings.Conexion" connectionString="Data Source=DireccionServidor;Initial Catalog=NombreDeBaseDeDatos;Integrated Security=true"
            providerName="System.Data.SqlClient" />
        </connectionStrings>
    ```

7. Guarde y cierre el archivo _`Clinica.exe.config`_.

8. Si cambio el nombre de la base de datos en el paso `4` realice lo siguiente:
    1. Buscar y abrir el archivo [Clinica.sql](Clinica.sql) ubicado en el directorio principal.
    2. Editar y reemplazar el nombre "**Clinica**" en la línea [`1`](Clinica.sql#L1) y [`6`](Clinica.sql#L6), por el nombre especificado en el paso `4`. Guardar y cerrar el archivo.
    3. Buscar y abrir el archivo [CIE.sql](CIE.sql) ubicado en el directorio principal.
    4. Editar y reemplazar el nombre "**Clinica**" en la línea [`2`](CIE.sql#L2) por el nombre especificado en el paso `4`. Guardar y cerrar el archivo.
9.  Ejecutar el archivo _`Clinica.sql`_ (*Contiene las tablas y procedimientos almacenados de la base de datos*) en SQL Server Management Studio y luego ejecutar el archivo _`CIE.sql`_ (*Contiene los códigos de enfermedades CIE*) ambos ubicados en la carpeta principal del proyecto.

## Ejecutar

Para ejecutar el sistema dirijase a la carpeta [Clinica/bin/Release/](Clinica/bin/Release/) y doble clic al archivo _`Clinica.exe`_.

Para ingresar como administrador digite "**Admin**" en el campo nombre de usuario y "**12345**" en el campo contraseña.

Para ingresar como usuario digite "**User**" en el campo nombre y "**12345**" en el campo contraseña.

## About

Version: 1.0.0

Author: [Juan Pablo Elias Hernández](https://gitlab.com/JuampaHernandez)

Contact: juanpahdz@hotmail.com
